var validate = {
    sname:false,
    stel:false,
    smsg:false,
    semail:false
}
// 一下是留言信息的js部分
var leav_msg = {
    lname : false,
    lemail : false,
    lmsg : false
}
var msg = '';

$(function(){
  var leavemsg = $('form[name=postmsg]');
  leavemsg.submit(function (){
    var ok = validate.sname && validate.stel && validate.smsg && validate.semail;
    if(ok)
    {
        return true;
    } 
    //点击提交按钮依次出发拾取焦点
    $('input[name=name]').trigger('blur');
    $('input[name=tel]').trigger('blur');
    $("textarea[name=msg]").trigger('blur');
    $("input[name=email]").trigger('blur');
    return false; 
   })

  var l_msg = $('form[name=leavemsg]');
  l_msg.submit(function (){
    var ok = leav_msg.lname && leav_msg.lmsg && leav_msg.lemail;
    if(ok)
    {
        return true;
    } 
    //点击提交按钮依次出发拾取焦点
    $('input[name=lname]').trigger('blur');
    $("textarea[name=lmsg]").trigger('blur');
    $("input[name=lemail]").trigger('blur');
    return false; 
   })
$("input[name=lname]").blur(function (){
   var username = $.trim($(this).val());
   var span = $(this).next();
       //不能为空
   if(username == '' || username=='您的姓名' || username == 'Your Name'){
       msg = '用户名不能为空';
       span.html(msg).removeClass('reg_right').addClass('reg_error');
       $(this).addClass('error');
       leav_msg.lname = false;
       return;
   }
 //正则判断
    len = username.replace(/[^\x00-\xff]/g,"**").length;
    if(len > 20 || len < 4){
       msg = '用户名的长度应在2-10位之间';
       span.html(msg).removeClass('reg_right').addClass('reg_error');
       $(this).addClass('error');
       leav_msg.lname = false;
       return;
    } 

   msg = '';
   leav_msg.lname = true;
   $(this).removeClass('error');
   span.html(msg).removeClass('reg_error').addClass('reg_right');
}) 
$("input[name=lemail]").blur(function (){
   var email = $.trim($(this).val());
   var span = $(this).next();
       //不能为空
   if(email == '' || email=='您的邮箱地址' || email == 'Your e-mail'){
       msg = '邮箱不能为空';
       span.html(msg).removeClass('reg_right').addClass('reg_error');
       $(this).addClass('error');
       leav_msg.lemail = false;
       return;
   }
 //正则判断
    stat = /^([a-zA-Z0-9_\-\.])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/i.test(email);
    if(! stat){
       msg = '输入正确的邮箱格式';
       span.html(msg).removeClass('reg_right').addClass('reg_error');
       $(this).addClass('error');
       leav_msg.lemail = false;
       return;
    } 

   msg = '';
   leav_msg.lemail = true;
   $(this).removeClass('error');
   span.html(msg).removeClass('reg_error').addClass('reg_right');
})

$("textarea[name=lmsg]").blur(function (){
   var message = $.trim($(this).val());
   var span = $(this).parent('p').next();
       //不能为空
   if(message == '' || message=='您的留言' || message == 'Your message'){
       msg = '内容不能为空';
       span.html(msg).removeClass('reg_right').addClass('reg_error');
       $(this).parent('p').removeClass('message').addClass('error');
       leav_msg.lmsg = false;
       return;
   }
   lens = message.replace(/[^\x00-\xff]/g,"**").length;
   if(lens > 1000){
      msg = '信息不能超过500字:(';
      span.html(msg).removeClass('reg_right').addClass('reg_error');
      $(this).parent('p').removeClass('message').addClass('error');
      leav_msg.lmsg = false;
      return;
   } 
   msg = '';
   leav_msg.lmsg = true;
   $(this).parent('p').removeClass('error').addClass('message');
   span.html(msg).removeClass('reg_error').addClass('reg_right');
})









 //验证用户名
 $("input[name=name]").blur(function (){
    var username = $.trim($(this).val());
    var span = $(this).next();
        //不能为空
    if(username == '' || username=='您的姓名' || username == 'Your Name'){
        msg = '用户名不能为空';
        span.html(msg).removeClass('reg_right').addClass('reg_error');
        $(this).addClass('error');
        validate.sname = false;
        return;
    }
  //正则判断
     len = username.replace(/[^\x00-\xff]/g,"**").length;
     if(len > 20 || len < 4){
        msg = '用户名的长度应在2-10位之间';
        span.html(msg).removeClass('reg_right').addClass('reg_error');
        $(this).addClass('error');
        validate.sname = false;
        return;
     } 

    msg = '';
    validate.sname = true;
    $(this).removeClass('error');
    span.html(msg).removeClass('reg_error').addClass('reg_right');
 })  

 $("input[name=tel]").blur(function (){
    var tel = $.trim($(this).val());
    var span = $(this).next();
        //不能为空
    if(tel == '' || tel=='您的联系方式' || tel == 'Your contact information'){
        msg = '联系方式不能为空';
        $(this).addClass('error');
        span.html(msg).removeClass('reg_right').addClass('reg_error');
        validate.stel = false;
        return;
    }
    stat1 = /^\d{11}$/.test(tel);
    stat2 = /(?:\(\d{3,4}\)|\d{3,4}-?)\d{8}/.test(tel);
    if(!stat1 || !stat2)
    {
        msg = '输入正确的联系方式,手机或固话';
        $(this).addClass('error');
        span.html(msg).removeClass('reg_right').addClass('reg_error');
        validate.stel = false;
        return;    
    }
    msg = '';
    validate.stel = true;
    $(this).removeClass('error');
    span.html(msg).removeClass('reg_error').addClass('reg_right');
 })  
 //  //邮箱
  $("input[name=email]").blur(function (){
     var email = $.trim($(this).val());
     var span = $(this).next();
         //不能为空
     if(email == '' || email=='您的邮箱地址' || email == 'Your e-mail'){
         msg = '邮箱不能为空';
         span.html(msg).removeClass('reg_right').addClass('reg_error');
         $(this).addClass('error');
         validate.semail = false;
         return;
     }
   //正则判断
      stat = /^([a-zA-Z0-9_\-\.])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/i.test(email);
      if(! stat){
         msg = '输入正确的邮箱格式';
         span.html(msg).removeClass('reg_right').addClass('reg_error');
         $(this).addClass('error');
         validate.semail = false;
         return;
      } 

     msg = '';
     validate.semail = true;
     $(this).removeClass('error');
     span.html(msg).removeClass('reg_error').addClass('reg_right');
  })
//  //textarea的解释 
 $("textarea[name=msg]").blur(function (){
    var message = $.trim($(this).val());
    var span = $(this).next();
        //不能为空
    if(message == '' || message=='您的留言' || message == 'Your message'){
        msg = '内容不能为空';
        span.html(msg).removeClass('reg_right').addClass('reg_error');
        $(this).addClass('error');
        validate.smsg = false;
        return;
    }
    lens = message.replace(/[^\x00-\xff]/g,"**").length;
    if(lens > 1000){
       msg = '信息不能超过500字:(';
       span.html(msg).removeClass('reg_right').addClass('reg_error');
       $(this).addClass('error');
       validate.smsg = false;
       return;
    } 
    msg = '';
    validate.smsg = true;
    $(this).removeClass('error');
    span.html(msg).removeClass('reg_error').addClass('reg_right');
 })
})