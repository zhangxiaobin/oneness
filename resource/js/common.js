/*
 * Copyright 2014-2015, gemground
 * The iDangero.us
 *
 * oneness（合一大学官方网站） common javascript
 *
 * Updated on: January 30, 2015
*/

var oWinTop;
var pcFlag = true;
function browserRedirect() {
	var sUserAgent = navigator.userAgent.toLowerCase();
	var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
	var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
	var bIsMidp = sUserAgent.match(/midp/i) == "midp";
	var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
	var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
	var bIsAndroid = sUserAgent.match(/android/i) == "android";
	var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
	var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";	
	if (/*bIsIpad || */bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
		pcFlag = false;
	} else {
		console.log("pc");
		pcFlag = true;
	}
}
browserRedirect();


// navigation js
$(function(){
	var  markMenuStatus = 1;
	// Menu control
	var menuthread = 120,
		logoH = $(".logo_box img").height(),
		_marginTop =$(".header .top_box").css("margin-top");
	
	$('.header_bg').css('opacity', 0.85);
	
	$("a.mobile-nav-list").click(function(){
		var _height = $(".nav").height(),
			leng = $(".nav li").length,
			h = $(".nav li").outerHeight();
		//alert(leng)	
		if(_height == 0){
			$(".nav").animate({height:(h+1)*leng})
		}else{
			$(".nav").animate({height:0})
		}
	});	
	
	$(window).scroll(function(){
		var win = $(window);
			oWinTop = win.scrollTop();
			
		_navMove = function(){
			//console.log(h); 
			if ( oWinTop > menuthread && markMenuStatus == 1) {
			  markMenuStatus = 2;
		
			  $('.header .top_box').animate({'marginTop':10, 'marginBottom':12});
			  $('.header .top_box .logo_box img').animate({'height':40});
			  $('.header .nav').animate({'lineHeight':40});
			  //$('.header .header_bg').animate({'height':60, 'opacity':0.8});
			  $('.header .header_bg').animate({'top':0});
			  
			}else if( oWinTop < menuthread && markMenuStatus == 2){
			  markMenuStatus = 1;
		
			  $('.header .top_box').animate({'marginTop':_marginTop, 'marginBottom':_marginTop});
			  $('.header .top_box .logo_box img').animate({'height':logoH});
			  $('.header .nav').animate({'lineHeight':logoH});
			  //$('.header .header_bg').animate({'height':120, 'opacity':0});
			  $('.header .header_bg').animate({'top':-120});
			}			
		}			
			
		if(pcFlag){
			_navMove();
		}
		
	});
	
	//
	// return top
	//
	$(".return_top a").click(function(){
		$("body,html").animate({
			scrollTop:$("#cont_1").offset().top
		},2000);
		return false;
	});
})























