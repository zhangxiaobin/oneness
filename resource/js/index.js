/*
 * Copyright 2014-2015, gemground
 * The iDangero.us
 *
 * oneness（合一大学官方网站） index javascript
 *
 * Updated on: February 3, 2015
*/

// 图片切换
$(function(){
	var _slidePerViewNum = 3;
	
	function _resize(){
		var v_w = $(window).width();
		//console.log(v_w);
		if(v_w<240){
			_slidePerViewNum = 1
		}else if(v_w<768){
			_slidePerViewNum = 2
		}
	}
	$(window).resize(_resize);
	_resize();
	
	var mySwiper1 = new Swiper('.swiper_box1',{
		//pagination: '.point_nav1',
		loop:true,
		speed:600,
		autoplay: 5000,	
		calculateHeight: true,
		cssWidthAndHeight: true,
		autoplayDisableOninteraction: false,
		grabCursor: true,
		paginationClickable: true
	})
	
	$('.prev1').on('click', function(e){
		e.preventDefault()
		mySwiper1.swipePrev()
		})
		$('.next1').on('click', function(e){
		e.preventDefault()
		mySwiper1.swipeNext()
	})
	
	var swiperParent = new Swiper('.swiper-parent',{
		pagination: '.pagination-parent',
		paginationClickable: true,
		calculateHeight: true,
		cssWidthAndHeight: true,
		loop: true,
		slidesPerView: _slidePerViewNum
	});
	
	var mySwiper2 = new Swiper('.swiper_box2',{
		//pagination: '.point_nav1',
		loop:true,
		speed:600,
		autoplay: 5000,	
		calculateHeight: true,
		cssWidthAndHeight: true,
		autoplayDisableOninteraction: false,
		grabCursor: true,
		paginationClickable: true
	})
	
	$('.world_prev').on('click', function(e){
		e.preventDefault()
		mySwiper2.swipePrev()
		})
		$('.world_next').on('click', function(e){
		e.preventDefault()
		mySwiper2.swipeNext()
	})
})


function format_num(num)
{
	str = num + '';
	len = Math.ceil(str.length / 3) * 3;
	while(str.length < len)
	{
	str = '0' + str;
	}
	str = str.replace(/(\d{3})/g, '$1,');
	str = str.replace(/^0+|,$/g, '');
	return str;
}

$(function(){
	//console.log(_marginTop);
		
	var markRollingDigital = false,markMenuAnimate = false,
		markAboutScroll = false, timerAboutScroll = 0, markAboutBoxActive = false,
		markCourse1 = markCourse2 = markCourse3 = true;
		
	$('.about_box .content').mouseleave(function (){markAboutBoxActive = false}).mouseenter(function (){markAboutBoxActive = true});
	
	// scroll 事件 start
	$(window).scroll(function(){
		var win = $(window);
			oWinTop = win.scrollTop();
			
		_pcMove = function(){
			
			// About content auto acroll control
			var aboutBox = $('.about_box'), spd = 60;
			if ( oWinTop > (aboutBox.offset().top - win.height())
				&& oWinTop < (aboutBox.offset().top + win.height())
			  ) {
		
			  if (!markAboutScroll) {
				markAboutScroll = true;
				var iid = Math.random();
				timerAboutScroll = iid;
				$('.content', aboutBox).scrollTop(0);
				var top = 0, _scroll = function (){
				  if (markAboutBoxActive) return setTimeout(_scroll, spd);
				  if (iid != timerAboutScroll) return;
		
				  var oldtop = $('.content', aboutBox).scrollTop();
				  top ++;
				  $('.content', aboutBox).scrollTop(top);
				  if (oldtop == $('.content', aboutBox).scrollTop()) return;
		
				  // interval control scroll speed
				  setTimeout(_scroll, spd);
				};
				_scroll();
			  }
			}else{
			  if ( markAboutScroll ) {
				markAboutScroll = false;
				timerAboutScroll = 0;
				$('.content', aboutBox).scrollTop(0);
			  }
			}
			
			// news & Announcement start
			var news = $('#cont_5'),
			pos4 = news.offset().top - win.height();
			if ( (markCourse1 || markCourse2 || markCourse3) && oWinTop < pos4) {
				markCourse1 = markCourse2 = markCourse3 = false;
				$('.fl h3').css({'marginLeft': -180, 'opacity':0, 'zoom':1});
				$('.fl ul').css({'marginLeft': -180, 'opacity':0, 'zoom':1});
				$('.fl a.more_btn').css({'marginLeft': -180, 'opacity':0, 'zoom':1});
				$('.fr h3').css({'marginRight': -180, 'opacity':0, 'zoom':1});
				$('.fr ul').css({'marginRight': -180, 'opacity':0, 'zoom':1});
				$('.fr a.more_btn').css({'marginRight': -180, 'opacity':0, 'zoom':1});
			}
		
			if ( !markCourse1 && oWinTop > (pos4 + 100)) {
				markCourse1 = true;
				$('.fl h3').animate({'marginLeft': 0, 'opacity':1}, {duration:'slow'});
				$('.fr h3').animate({'marginRight': 0, 'opacity':1}, {duration:'slow'});
			}
	
			if ( !markCourse2 && oWinTop > (pos4 + 360)) {
				
				markCourse2 = true;	
				$('.fl ul').animate({'marginLeft': 0, 'opacity':1}, {duration:'slow'});
				$('.fr ul').animate({'marginRight': 0, 'opacity':1}, {duration:'slow'});
				/*setTimeout(function (){
					$('.fl ul li').animate({'marginLeft': 0, 'opacity':1}, {duration:'slow'});
					$('.fr ul li').animate({'marginRight': 0, 'opacity':1}, {duration:'slow'});
				}, 180);*/
			}
			if ( !markCourse3 && oWinTop > (pos4 + 420)) {
				markCourse3 = true;
				$('.fl a.more_btn').animate({'marginLeft': 0, 'opacity':1});
				$('.fr a.more_btn').animate({'marginRight': 0, 'opacity':1});
			}
		}
		
		if(pcFlag){
			_pcMove();
		}	 
		// news & Announcement end

		//
		//数值变化js
		//
		var cont10 = $('#cont_11'),
		  checkpos = cont10.offset().top - win.height();
		if( markRollingDigital && oWinTop < checkpos ){
		  markRollingDigital = false;
		  $(".digital li .num").text('0');
	
		}else if( markRollingDigital === false && oWinTop > (checkpos + 200) ){ 
		  markRollingDigital = true;
	
		  var nums = $(".digital li .num"),
		  timer = setInterval(function (){
			var arrived = 0;
			nums.each(function (){
			  var num = $(this),
			  target = parseFloat(num.data('target')),
			  current = parseFloat(num.text().replace(/,/g, ''));
	
			  var dis = (target - current);
			  if (dis > 0.5) {
				num.text( format_num(Math.ceil(current + dis / 3.25)) );
			  }else{
				num.text( format_num(target) );
				arrived ++;
			  }
			});
	
			if (arrived == nums.length) {
			  clearInterval(timer);
			}
		  }, 30);
	
		}
		
	}); //scroll 滚动事件 end
	
			
	var kv = $('.kv_box .kv-img'),_kvtime = 1200, _kvinterval = 6000,
	_kvlooper = function (){
	var current = kv.filter('.active'),
	next = current.next(".kv-img");
	if(next.length == 0) next = kv.eq(0);
	
	next.css('z-index', 999).fadeIn(1200, function (){
		current.fadeOut();
	}).addClass('active');
	current.removeClass('active');
	
	setTimeout(_kvlooper, _kvinterval);
	};
	
	kv.filter('.active').css('z-index', 999);
	kv.filter('.hidden').removeClass('hidden').fadeOut(10);
	// _kvlooper();
	setTimeout(_kvlooper, _kvinterval);
	
	//
	// 温馨每周切换js
	//
	$(".week_next").click(function(){
		var arrLi = $(this).parent().parent().find("li"),_current = arrLi.filter(".cur"),_next =_current.next();
		if(_next.length == 0) _next = arrLi.eq(0);
		_current.fadeOut(600,function(){
			_next.fadeIn().addClass("cur");
		}).removeClass("cur");		
	});
	
	$(".week_prev").click(function(){
		var arrLi = $(this).parent().parent().find("li"),_current = arrLi.filter(".cur"),_prev =_current.prev(),
		    leng = arrLi.length;
			//alert(leng);
		if(_prev.length == 0) _prev = arrLi.eq(leng-1);
		_current.fadeOut(600,function(){
			_prev.fadeIn().addClass("cur");
		}).removeClass("cur");
	});
	
	//
	// 课程表tab切换 start
	//
	var curriculumCon = $(".curriculum_con");
	var timer;
	var curriculumNav = $(".curriculum_nav li").mouseenter(function(){
		var idx = curriculumNav.index(this);
		
		$(this).addClass("current").siblings().removeClass("current");
		clearTimeout(timer);
		timer = setTimeout(function(){
			curriculumCon.css("display","none");
			curriculumCon.eq(idx).fadeIn();	
		},100)
		
	});
	
	//
	// about mobile more   xiaobin-2015/3/4修改
	//
	$(".mobile-more").click(function(){
		var cont = $(".about_text .content");
			contH = cont.outerHeight(),
			mH = $(".about_text .m_con").outerHeight();
			re =  /^[A-Za-z]*$/;
			str   =  $(this).text().replace(/\s+/g,""); 
		if(contH == 160){
			cont.animate({height:mH},400);
			if(re.test(str))
			{
				$(this).text("Wheel Up");
			}
			else
			{
				$(this).text("收起");
			}
			
		}else{
			cont.animate({height:160},400);
			if(re.test(str))
			{
				$(this).text("Read More");
			}
			else
			{
				$(this).text("展开阅读更多");
			}
			
		}
			
		//alert(mH)
	})
	
	//
	//
	//
	impression = $(".impression_list li").mouseover(function(event){
		$(".hover_bg").css("display",'block');
		$(this).find('.hover_bg').css("display",'none')
		//event.stopPropagation()
	})
	
	$(".impression_list").mouseleave(function(){
		$(".hover_bg").fadeOut();
	})
	
});
