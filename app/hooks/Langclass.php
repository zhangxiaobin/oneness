<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * DiliCMS pre-controller Hook
 *
 * @package     gem
 * @subpackage  Hooks
 * @category    Hooks
 * @author      xiaobin
 * @link        xiaobin@gemdesign.cn
 */

class LangClass extends CI_Controller{
 
    function set_lang() {
 
                //从Uri中分解出当前的语言，如 '', 'sc' 或 'ch'
    	        // $CI =& get_instance();
                $my_lang = $this->uri->segment(1);
 
                //默认语言为英语english
                if ($my_lang=='en' || $my_lang=='cn')
                {
                        //动态设置当前语言为'sc' 或 'ch'
                        $this->config->set_item('language', $my_lang);
                }
 
                // $this->load->helper('language');
 
    }
 
}