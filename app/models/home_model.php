<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home_model extends CI_Model
{
	public function get_all_impress_img($num=0)
	{
		$this->db->select('id,pic_name,pic_cover,pic_spec,pic_size,upload_time');
		if($num)
		{
         $data = $this->db->limit($num)->get('un_pics')->result_array();  
		}
		else
		{
          $data = $this->db->get('un_pics')->result_array();
		}
		//获取6张图片
		
		return $data;
	}

	//获取校园简介
	public function get_conpus_about($table)
	{
		$this->db->select('id,title,desc');
		$about = $this->db->get($table)->row_array();
		$data = array();
		$str = str_replace("\r", '|', $about['desc']);
		$arr = explode('|', $str);
		return $arr;
	}

	//获取校区图片
	public function get_uni_pics()
	{
		$this->db->select('id,uname,enuname,upics');
		$data = $this->db->get_where('university',array('status'=>1))->result_array();
		return $data;
	}

	//获取所有的合一智慧图片
	public function get_all_wisdom($table)
	{
		$this->db->select('id,title,desc,image,content,vedio,updatetime,type');
		$this->db->order_by("sort", "asc");
		$this->db->order_by('updatetime','desc');
		$this->db->where(array('status'=>1));
		$data = $this->db->limit(9)->get($table)->result_array();
		 return $data;
	}

	//获取温馨每周
	public function get_all_week_words($table)
	{
		$this->db->select('id,content,addtime');
		$this->db->order_by('addtime','desc');
		$data = $this->db->limit(30)->get($table)->result_array();
		 return $data;	
	}

	//获取名人语录
	public function get_all_sayings($table)
	{
		$this->db->select('id,name,image,desc,content');
	    $this->db->order_by("sort", "asc");
		$this->db->where(array('status'=>1));
		$data = $this->db->limit(30)->get($table)->result_array();
		 return $data;		
	}

	//获取所有新闻公告
	public function get_all_notice($table,$num =3)
	{
		$this->db->select('id,title,tag,desc,content,vedio,type,updatetime');
		$this->db->order_by("tag", "asc");
	    $this->db->order_by("sort", "asc");
	    $this->db->order_by('updatetime','desc');
		$this->db->where(array('status'=>1));
		$data = $this->db->limit($num)->get($table)->result_array();
		 return $data;	
	}

	//获取所有新闻
	public function get_all_news($table,$num =3)
	{
		$this->db->select('id,title,tag,desc,content,vedio,type,updatetime');
		$this->db->order_by("tag", "asc");
	    $this->db->order_by("sort", "asc");
	    $this->db->order_by('updatetime','desc');
		$this->db->where(array('status'=>1));
		$data = $this->db->limit($num)->get($table)->result_array();
		 return $data;	
	}

	//获取课后辅导
	public function get_all_aftersch($table)
	{
		$this->db->select('id,title,name,desc,type,updatetime');
		$this->db->order_by("sort", "asc");
		$this->db->order_by('updatetime','desc');
		$this->db->where(array('status'=>1));
		$data = $this->db->limit(20)->get($table)->result_array();
		return $data;
	}

	//获取学员分享
	public function get_all_stu_shares($table)
	{
		$this->db->select('id,title,desc,type,image,name,updatetime');
		$this->db->order_by("sort", "asc");
		$this->db->order_by('updatetime','desc');
		$this->db->where(array('status'=>1));
		$data = $this->db->limit(4)->get($table)->result_array();
		return $data;	
	}

/**
 *     合一智慧 合一     wisdom start
 */
	//温馨每周分页
	public function get_all_weeks($perpage,$offset,$flag,$wisdom = '')
	{
		if($wisdom == '')
		{
			$this->db->order_by('addtime','desc');
		}
		else
		{
			$this->db->order_by("sort", "asc");
			$this->db->order_by('updatetime','desc');
			$this->db->where(array('status'=>1));
		}
		$data = $this->db->get($perpage,$offset,$flag)->result_array();
		return $data;
	}

	// 获取一片何以智慧文章
	public function get_one_wis_art($table,$id)
	{
		$this->db->select('id,title');
		$this->db->where(array('status'=>1));       
		$this->db->order_by("sort", "asc");
		$this->db->order_by('updatetime','desc');
		$arr = $this->db->get($table)->result_array();

		$count = count($arr);
		$tmp = array();
		foreach ($arr as $k => $v) {
		   if($v['id'] == $id)
		   {
		      $tmp['pre']  = $k >= 1 ? $arr[$k-1]['id'] : 0 ;
		      $tmp['pre_title'] = $k >= 1 ? $arr[$k-1]['title'] : 0;
		      $tmp['nex'] = ($k+1) >= $count ? 0 : $arr[$k+1]['id'];
		      $tmp['next_title'] = ($k+1) >= $count ? 0 : $arr[$k+1]['title'];
		      break;
		   }
		}

		$this->db->select('id,title,desc,image,content,vedio,updatetime,type');
		$this->db->where(array('status'=>1,'id'=>$id));
		$data = $this->db->get($table)->row_array();
		 $tmp_info = array_merge($data,$tmp);
		 return $tmp_info;
	}
	/**
	 * 何以智慧  合一     wisdom end
	*/

	/**
	 * 新闻部分的model   start   news   ======================
	 */
    public function get_one_not_newinfo($table,$id)
    {
    	$this->db->select('id,title');
    	$this->db->where(array('status'=>1));       
    	$this->db->order_by("sort", "asc");
    	$this->db->order_by('updatetime','desc');
    	$arr = $this->db->get($table)->result_array();

    	$count = count($arr);
    	$tmp = array();
    	foreach ($arr as $k => $v) {
    	   if($v['id'] == $id)
    	   {
    	      $tmp['pre']  = $k >= 1 ? $arr[$k-1]['id'] : 0 ;
    	      $tmp['pre_title'] = $k >= 1 ? $arr[$k-1]['title'] : 0;
    	      $tmp['nex'] = ($k+1) >= $count ? 0 : $arr[$k+1]['id'];
    	      $tmp['next_title'] = ($k+1) >= $count ? 0 : $arr[$k+1]['title'];
    	      break;
    	   }
    	}

    	$this->db->select('id,title,desc,content,vedio,updatetime,type');
    	$this->db->where(array('status'=>1,'id'=>$id));
    	$data = $this->db->get($table)->row_array();
    	 $tmp_info = array_merge($data,$tmp);
    	 return $tmp_info;
    }

    //获取所有的新闻信息
    public function get_all_no_newinfo($perpage,$offset,$flag)
    {
    	$this->db->order_by("tag", "asc");
	    $this->db->order_by("sort", "asc");
	    $this->db->order_by('updatetime','desc');
	    $this->db->where(array('status'=>1));
	    $data = $this->db->get($perpage,$offset,$flag)->result_array();
	    return $data;	
    }
	/**
	 * 新闻部分的model  end  news   ======================
	 */


	/**
	 * 新闻分享部分          interact  ====start
	 */
    public function get_all_share_news($perpage,$offset,$flag)
    {
	    $this->db->order_by("sort", "asc");
	    $this->db->order_by('updatetime','desc');
	    $this->db->where(array('status'=>1));
	    $data = $this->db->get($perpage,$offset,$flag)->result_array();
	    return $data;	
    }
	/**
	 * 新闻分享部分          interact  =====end
	 */


	/**
	 * 课程分享部分          course  ====start
	 */
    public function get_all_course($table)
    {
	    $this->db->select('id,title,day,desc,image,content');
	    $this->db->order_by("sort", "asc");
	    $this->db->where(array('status'=>1));
	    $data = $this->db->get($table)->result_array();
	    return $data;	
    }

    //获取所有课程安排   (当前年份)
    public function get_all_schedule($table,$year)
    {
    	$this->db->select('id,year,con');
    	if(! empty($year))
    	{
        	$this->db->where(array('year'=>$year));	
        	$data = $this->db->get($table)->row_array();	
    	}
    	else
    	{
        	$data = $this->db->get($table)->result_array();		
    	}
    	return $data;	    	
    }
	/**
	 * 课程分享部分          course  ====end
	 */
//获取文章的前5个
	public function get_other_art($table,$num = 5)
	{
		$this->db->select('id,title,desc,image,updatetime,type');
		$this->db->order_by("sort", "asc");
		$this->db->order_by('updatetime','desc');
		$this->db->where(array('status'=>1));
		$data = $this->db->limit($num)->get($table)->result_array();
		 return $data;	
	}

	// 获取一个学院分享
	public function get_one_stu_share($table,$id)
	{
		$this->db->select('id,title');
		$this->db->where(array('status'=>1));       
		$this->db->order_by("sort", "asc");
		$this->db->order_by('updatetime','desc');
		$arr = $this->db->get($table)->result_array();

		$count = count($arr);
		$tmp = array();
		foreach ($arr as $k => $v) {
		   if($v['id'] == $id)
		   {
		      $tmp['pre']  = $k >= 1 ? $arr[$k-1]['id'] : 0 ;
		      $tmp['pre_title'] = $k >= 1 ? $arr[$k-1]['title'] : 0;
		      $tmp['nex'] = ($k+1) >= $count ? 0 : $arr[$k+1]['id'];
		      $tmp['next_title'] = ($k+1) >= $count ? 0 : $arr[$k+1]['title'];
		      break;
		   }
		}
		$this->db->where(array('status'=>1,'id'=>$id));
		$data = $this->db->get($table)->row_array();
		 $tmp_info = array_merge($data,$tmp);
		 return $tmp_info;	
	}

	public function get_other_stu_share($table,$num=5)
	{
		$this->db->where(array('status'=>1));
		$data = $this->db->limit($num)->get($table)->result_array();
		 return $data;
	}


	/**
	 * 联络中心
	 */

	//获取3前5个联系方式
	public function get_all_contacter($table,$num = 5)
	{
		$this->db->select('id,name,team,phone,city,wechat,remark');
		$this->db->order_by("sort", "asc");
		$this->db->where(array('status'=>1));
		$data = $this->db->limit($num)->get($table)->result_array();
		 return $data;	
	}

	public function get_all_interprete($table,$num = '')
	{
		$this->db->select('id,title,desc,type,image,name,updatetime');
		$this->db->order_by("sort", "asc");
		$this->db->order_by('updatetime','desc');
		$this->db->where(array('status'=>1));
		if($num !== '')
		{
			$this->db->limit(3);
		}
		$data = $this->db->get($table)->result_array();
		return $data;
	}
}
