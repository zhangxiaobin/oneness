<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller 
{
	private $common_msg;
	private $common_url;
	private $common_lang;
    /**
     * 构造函数
     */
    public function __construct()
    {
    	parent::__construct();
    	   $this->load->model('home_model', 'home');
    	$this->common_url = $this->uri_str;  //获取当前链接
    	$langs = substr($this->common_url, 0,2);
    	if($langs != 'cn' && $langs != 'en')
    	{
    	   $this->common_lang = 'cn';
    	}
    	else
    	{
    	  $this->common_lang = $langs;
    	}

    	$this->lang->load('contact');
    	$this->lang->load('common');   //获取语言文件
    	$this->common_msg = array_merge(lang('common'),lang('con')) ;    //获取语言文字
    }
	/**
	 * Index Page for this controller.
	 */

	public function index()
	{
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $flag = $data['lang'] == 'en' ? 0 : 1;  //数据库标识
        //获取联系方式
        $con_table = $data['lang'] == 'en' ? 'contacter_'.$data['lang'] : 'contacter';
        $data['contacters'] = $this->home->get_all_contacter($con_table,50);
		template('contact/index',$data);
	}

    //获取志愿者申请信息
        //留言信息添加
    public function leave_msg()
    {
        $url  = $this->uri_str;  //获取当前链接
        $lang = substr($url, 0,2);
        $ip = $this->input->ip_address();
        //2014/1/19  ch
        $name  = $this->input->post('name');
        $tel   = $this->input->post('tel');
        $email = $this->input->post('email');
        $msg   = $this->input->post('msg');
        $name_length = strlen($name);
        if($name_length > 24) $this->error('姓名字数超出限制了:-(');
         $msg_length = strlen($msg);
         if($msg_length > 1500) $this->error('信息字数超出限制了:-(');           
        if(empty($name) || $name == '您的姓名' || $name == 'Your Name')
        {
            $this->error('请输入您的姓名:(');
        }
        if(empty($tel) || $tel == '您的联系方式' || $tel == 'Your contact information')
        {
            $this->error('请输入您的联系方式:(');
        }
        if(empty($email) || $email == '您的邮箱地址' || $email == 'Your e-mail')
        {
            $this->error('请输入您的联系方式:(');
        }
        if(empty($msg) || $msg == '您的留言' || $msg == 'Your message')
        {
            $this->error('请输入信息内容:(');
        }
        //end
        $data = array();//获取表单内容
        $data = array(
            'name'            => $name,
            'tel'             => $tel,
            'email'           => $email,
            'updatetime'      => NOW,
            'message'             => $msg,
            'ip'              => $ip
         );

        $cdata = $this->db->from('volunteer')->where('ip',$ip)->order_by("updatetime", "desc")->limit(1)->get()->row_array();
        if(! empty($cdata))
        {
            $lasttime = $cdata['updatetime'];
            //简单的判断
            if((time() - $lasttime) < 300)
            {
                $this->error('输入过于频繁！稍后5分钟后再尝试');               
            } 
            else
            {
                 $this->db->insert('volunteer',$data);
                 $this->success('申请成功！工作人员会尽快回复您的:)',$lang.'/contact');                 
            }       
        }
        else
        {
            $this->db->insert('volunteer',$data);
            $this->success('申请成功！工作人员会尽快回复您的:)',$lang.'/contact');  
        }   
    }
    
    //留言信息管理
    public function message()
    {
        $url  = $this->uri_str;  //获取当前链接
        $lang = substr($url, 0,2);
        $ip = $this->input->ip_address();
        //2014/1/19  ch
        $name  = $this->input->post('lname');
        $email = $this->input->post('lemail');
        $msg   = $this->input->post('lmsg');
        $name_length = strlen($name);
        if($name_length > 24) $this->error('姓名字数超出限制了:-(');
         $msg_length = strlen($msg);
         if($msg_length > 1500) $this->error('信息字数超出限制了:-(');           
        if(empty($name) || $name == '您的姓名' || $name == 'Your Name')
        {
            $this->error('请输入您的姓名:(');
        }
        if(empty($email) || $email == '您的邮箱地址' || $email == 'Your e-mail')
        {
            $this->error('请输入您的联系方式:(');
        }
        if(empty($msg) || $msg == '您的留言' || $msg == 'Your message')
        {
            $this->error('请输入信息内容:(');
        }
        //end
        $data = array();//获取表单内容
        $data = array(
            'lname'            => $name,
            'lemail'           => $email,
            'ltime'            => NOW,
            'lmsg'             => $msg,
            'ip'               => $ip
         );
        $cdata = $this->db->from('message')->where('ip',$ip)->order_by("ltime", "desc")->limit(1)->get()->row_array();
        if(! empty($cdata))
        {
            $lasttime = $cdata['ltime'];
            //简单的判断
            if((time() - $lasttime) < 300)
            {
                $this->error('输入过于频繁！稍后5分钟后再尝试');               
            } 
            else
            {
                 $this->db->insert('message',$data);
                 $this->success('留言成功！工作人员会尽快回复您的:)',$lang.'/contact');                 
            }       
        }
        else
        {
            $this->db->insert('message',$data);
            $this->success('留言成功！工作人员会尽快回复您的:)',$lang.'/contact');  
        } 
    }
}
