<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class interact extends MY_Controller 
{
	private $common_msg;
	private $common_url;
	private $common_lang;
    /**
     * 构造函数
     */
    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('home_model', 'home');
    	$this->common_url = $this->uri_str;  //获取当前链接
    	$langs = substr($this->common_url, 0,2);
    	if($langs != 'cn' && $langs != 'en')
    	{
    	   $this->common_lang = 'cn';
    	}
    	else
    	{
    	  $this->common_lang = $langs;
    	}

    	$this->lang->load('interact');
    	$this->lang->load('common');   //获取语言文件
    	$this->common_msg = array_merge(lang('common'),lang('inter')) ;    //获取语言文字
    }
	/**
	 * Index Page for this controller.
	 */

	public function index()
	{
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $flag = $data['lang'] == 'en' ? 0 : 1;  //数据库标识
        $this->load->library('page_list');//引入分页类
        //学员风采  分享
        $sh_table = $data['lang'] == 'en' ? 'stu_share_'.$data['lang'] : 'stu_share';
        $tmp_weeks = $this->_get_pages($sh_table,8);
        $data['all_shares'] = $tmp_weeks['news'];
        $data['spl'] = $tmp_weeks['pl'];
        
        //智慧解读
        $in_table = $data['lang'] == 'en' ? 'interprete_'.$data['lang'] : 'interprete';
        $tmp_inters = $this->_get_pages($in_table,8);
        $data['inters'] = $tmp_inters['news'];
        $data['ipl'] = $tmp_inters['pl'];

        //课后辅导
        $af_table = $data['lang'] == 'en' ? 'afterschool_'.$data['lang'] : 'afterschool';
        $tmp_aftersch = $this->_get_pages($af_table,3);
        $data['all_afsch'] = $tmp_aftersch['news'];
        $data['apl'] = $tmp_aftersch['pl'];

        $con_table = $data['lang'] == 'en' ? 'contacter_'.$data['lang'] : 'contacter';
        $data['contacters'] = $this->home->get_all_contacter($con_table,50);
        
		template('interact/index',$data);
	}

    //分页获取数据
    //getpages   分页
    private function _get_pages($table,$perpage)
    {
        $perpage = $perpage;  //每页显示条数
        $total = $this->db->count_all_results($table);//总条数
        $page = @intval($this->input->get('page'));
        if($page<=1) $page = 1;
         $this->page_list->initialize(array('total'=>$total,'size'=>$perpage,'page'=>$page));
        $offset=$perpage*($page-1);
        $data = $this->home->get_all_share_news($table,$perpage,$offset);
        $pl = $total > $perpage ? $this->page_list->display(site_url($this->common_lang.'/interact/index/?page=-page-')) : '';
        return array(
          'pl' =>$pl,
          'news'=>$data
        );
    }

    // 获取单个的分享文章
    public function newinfo()
    {
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $ne_table = $data['lang'] == 'en' ? 'stu_share_'.$data['lang'] : 'stu_share';
        $id = $this->uri->segment(4);
        $data['art'] = $this->home->get_one_stu_share($ne_table,$id);
        //获取其他的
        $data['other_art'] = $this->home->get_other_stu_share($ne_table,5);
        template('interact/newinfo',$data);
    }
    public function interinfo()
    {
         $data['uri'] = $this->common_url;  //获取当前链接
         $data['lang']= $this->common_lang;  //获取语言标识
         $data['lan'] = $this->common_msg;   //获取语言包信息
         $in_table = $data['lang'] == 'en' ? 'interprete_'.$data['lang'] : 'interprete';
         $id = $this->uri->segment(4);
         $data['art'] = $this->home->get_one_stu_share($in_table,$id);
         //获取其他的
         $data['other_art'] = $this->home->get_other_stu_share($in_table,5);
         template('interact/newinfo',$data);   
    }
    public function afterinfo()
    {
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $af_table = $data['lang'] == 'en' ? 'afterschool_'.$data['lang'] : 'afterschool';
        $id = $this->uri->segment(4);
        $data['art'] = $this->home->get_one_stu_share($af_table,$id);
        //获取其他的
        $data['other_art'] = $this->home->get_other_stu_share($af_table,5);
        template('interact/newinfo',$data);   
    }
    // ajax 获取所有的学员分享
    public function load_page_share()
    {
        $num = intval($this->input->post('num'));
        $lang = trim($this->input->post('lang'));
        $table = $lang == 'en' ? 'stu_share_'.$lang : 'stu_share';          
        $perpage = 8;  //每页显示条数
        $total = $this->db->count_all_results($table);//总条数

        $page = $num;
        $page = @intval($page);
        if($page<=1) $page = 1;
        $this->load->library('page_list',array('total'=>$total,'size'=>$perpage,'page'=>$page));
        $offset=$perpage*($page-1);
        $data  = $this->home->get_all_share_news($table,$perpage,$offset);
        $html = '';
        if(! empty($data))
        {
            foreach ($data as $v)
            {
              if($lang == 'cn')
              {
                  $html .= "<li><a href='".site_url($lang.'/interact/newinfo/'.$v['id'])."'>";
                  $html .= "<img src='".base_url().'/uploads/share/'.$v['image']."'>";
                  $html .= "<p class='tab'>学员分享<span></span></p>";
                  $html .= "<p class='share_text'>".$v['title']."</p>";
                  $html .= "<p class='share_author'><span>●</span>".$v['name']." /  文</p></a></li>";
              }
              else
              {
                $html .= "<li><a href='".site_url($lang.'/interact/newinfo/'.$v['id'])."'>";
                $html .= "<img src='".base_url().'/uploads/share/'.$v['image']."'>";
                $html .= "<p class='tab'>学员分享<span></span></p>";
                $html .= "<p class='share_text'>".$v['title']."</p>";
                $html .= "<p class='share_author'><span>●</span>".$v['name']." /  文</p></a></li>";
              }
        
            }    
        }
        $pl = $total > $perpage ? $this->page_list->display(site_url($lang.'/interact/index/?page=-page-')) : '';

        $callback = array(
          'pl' =>$pl,
          'news'=>$html
        );

        echo json_encode($callback);exit(); 
    }



    //=============================获取所有课后辅导

    public function load_page_aftersch()
    {
       $num = intval($this->input->post('num'));
       $lang = trim($this->input->post('lang'));
       $table = $lang == 'en' ? 'afterschool_'.$lang : 'afterschool';          
       $perpage = 3;  //每页显示条数
       $total = $this->db->count_all_results($table);//总条数

       $page = $num;
       $page = @intval($page);
       if($page<=1) $page = 1;
       $this->load->library('page_list',array('total'=>$total,'size'=>$perpage,'page'=>$page));
       $offset=$perpage*($page-1);
       $data  = $this->home->get_all_share_news($table,$perpage,$offset);
       $html = '';
       if(! empty($data))
       {
           foreach ($data as $v)
           {
             if($lang == 'cn')
             {
                 $html .= "<dl><a href='".site_url($lang.'/interact/afterinfo/'.$v['id'])."'>";
                 $html .= "<dt><h4>".$v['title']."</h4><p><span>●</span>主讲人: ".$v['name']." | 发布时间:";
                 $html .= date('Y-m-d',$v['updatetime'])."</p></dt><dd>【内文】".msubstr($v['desc'],0,50,'utf-8','...')."</dd></a></dl>";
             }
             else
             {
                $html .= "<dl><a href='".site_url($lang.'/interact/afterinfo/'.$v['id'])."'>";
                $html .= "<dt><h4>".$v['title']."</h4><p><span>●</span>主讲人: ".$v['name']." | 发布时间:";
                $html .= date('Y-m-d',$v['updatetime'])."</p></dt><dd>【内文】".msubstr($v['desc'],0,50,'utf-8','...')."</dd></a></dl>";
             }
       
           }    
       }
       $pl = $total > $perpage ? $this->page_list->display(site_url($lang.'/interact/index/?page=-page-')) : '';

       $callback = array(
         'pl' =>$pl,
         'news'=>$html
       );

       echo json_encode($callback);exit();  
    }

    //==============================interprete
    public function load_page_inter()
    {
     $num = intval($this->input->post('num'));
     $lang = trim($this->input->post('lang'));
     $table = $lang == 'en' ? 'interprete_'.$lang : 'interprete';          
     $perpage = 8;  //每页显示条数
     $total = $this->db->count_all_results($table);//总条数

     $page = $num;
     $page = @intval($page);
     if($page<=1) $page = 1;
     $this->load->library('page_list',array('total'=>$total,'size'=>$perpage,'page'=>$page));
     $offset=$perpage*($page-1);
     $data  = $this->home->get_all_share_news($table,$perpage,$offset);
     $html = '';
     if(! empty($data))
     {
         foreach ($data as $v)
         {
           if($lang == 'cn')
           {
               $html .= "<li><a href='".site_url($lang.'/interact/newinfo/'.$v['id'])."'>";
               $html .= "<img src='".base_url().'/uploads/share/'.$v['image']."'>";
               $html .= "<p class='tab'>智慧解读<span></span></p>";
               $html .= "<p class='share_text'>".$v['title']."</p>";
               $html .= "<p class='share_author'><span>●</span>".$v['name']." /  文</p></a></li>";
           }
           else
           {
             $html .= "<li><a href='".site_url($lang.'/interact/newinfo/'.$v['id'])."'>";
             $html .= "<img src='".base_url().'/uploads/share/'.$v['image']."'>";
             $html .= "<p class='tab'>智慧解读<span></span></p>";
             $html .= "<p class='share_text'>".$v['title']."</p>";
             $html .= "<p class='share_author'><span>●</span>".$v['name']." /  文</p></a></li>";
           }
     
         }    
     }
     $pl = $total > $perpage ? $this->page_list->display(site_url($lang.'/interact/index/?page=-page-')) : '';

     $callback = array(
       'pl' =>$pl,
       'news'=>$html
     );

     echo json_encode($callback);exit(); 
    }
}