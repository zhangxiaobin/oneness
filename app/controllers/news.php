<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends MY_Controller 
{
	private $common_msg;
	private $common_url;
	private $common_lang;
    /**
     * 构造函数
     */
    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('home_model', 'home');
    	$this->common_url = $this->uri_str;  //获取当前链接
    	$langs = substr($this->common_url, 0,2);
    	if($langs != 'cn' && $langs != 'en')
    	{
    	   $this->common_lang = 'cn';
    	}
    	else
    	{
    	  $this->common_lang = $langs;
    	}

    	$this->lang->load('news');
    	$this->lang->load('common');   //获取语言文件
    	$this->common_msg = array_merge(lang('common'),lang('news')) ;    //获取语言文字
    }
	/**
	 * Index Page for this controller.
	 */

	public function index()
	{
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $flag = $data['lang'] == 'en' ? 0 : 1;  //数据库标识
        $this->load->library('page_list');
         //获取所有公告
        $no_table = $data['lang'] == 'en' ? 'notice_'.$data['lang'] : 'notice';
        $tmp_weeks = $this->_get_pages($no_table,10);
        $data['all_notice'] = $tmp_weeks['news'];
        $data['npl'] = $tmp_weeks['pl'];

       //获取所有新闻
        $ne_table = $data['lang'] == 'en' ? 'news_'.$data['lang'] : 'news';
        $tmp_nots = $this->_get_pages($ne_table,10);
        $data['all_news'] = $tmp_nots['news'];
        $data['nepl'] = $tmp_nots['pl'];
        template('news/index',$data);
	}

    //获取单个新闻
    public function newinfo()
    {
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $ne_table = $data['lang'] == 'en' ? 'news_'.$data['lang'] : 'news';
        $id = $this->uri->segment(4);
        $data['art'] = $this->home->get_one_not_newinfo($ne_table,$id);
         $data['other_art'] = $this->home->get_all_news($ne_table,5);
        template('news/notice',$data);
    }

    //获取单个公告
    public function notice()
    {
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $no_table = $data['lang'] == 'en' ? 'notice_'.$data['lang'] : 'notice';
        $id = $this->uri->segment(4);
        $data['art'] = $this->home->get_one_not_newinfo($no_table,$id);

        //获取其他
        $data['other_art'] = $this->home->get_all_notice($no_table,5);
        template('news/notice',$data);
    }

    //分页获取数据
    //getpages   分页
    private function _get_pages($table,$perpage)
    {
        $perpage = $perpage;  //每页显示条数
        $total = $this->db->count_all_results($table);//总条数
        $page = @intval($this->input->get('page'));
        if($page<=1) $page = 1;
        $this->page_list->initialize(array('total'=>$total,'size'=>$perpage,'page'=>$page));
        $offset=$perpage*($page-1);
        $data = $this->home->get_all_no_newinfo($table,$perpage,$offset);
        $pl = $total > $perpage ? $this->page_list->display() : '';
        return array(
          'pl' =>$pl,
          'news'=>$data
        );
    }

    //ajax获取分页
    public function load_page_notice()
    {
        $num = intval($this->input->post('num'));
        $lang = trim($this->input->post('lang'));
        $flag = intval($this->input->post('flag'));
        if(! empty($flag))
        {
            $table = $lang == 'en' ? 'news_'.$lang : 'news';
        }
        else
        {
           $table = $lang == 'en' ? 'notice_'.$lang : 'notice';     
        }

        //测试页数  后期修改  10
        $perpage = 10;
        $total = $this->db->count_all_results($table);
        $page = $num;
        $page = @intval($page);
        if($page<=1) $page = 1;
        $this->load->library('page_list',array('total'=>$total,'size'=>$perpage,'page'=>$page));
        $offset=$perpage*($page-1);
        $data  = $this->home->get_all_no_newinfo($table,$perpage,$offset);
        $html = '';
        if(! empty($data))
        {
    if(! empty($flag))
    {
            if($num === 1)
            {
                foreach ($data as $k=>$v) 
                { 
                    switch ($k) 
                    {
                        case 0:
                            $html .= "<li class='top'><a href='".site_url($lang.'/news/newinfo/'.$v['id'])."'><span class='tab'>置顶</span><h4>".$v['title']."<span class='date'>";
                            $html .= date('Y-m-d',$v['updatetime'])."</span></h4><span>By 合一大学官方中文网s站</span><p>".msubstr($v['desc'], 0,50,'utf-8','>>')."</p></a></li>";
                            break;
                        case 1:
                            $html .= "<li class='hot'><a href='".site_url($lang.'/news/newinfo/'.$v['id'])."'><span class='tab'>热门</span><h4>".$v['title']."<span class='date'>";
                            $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
                            break;
                        case 2:
                            $html .= "<li class='hot'><a href='".site_url($lang.'/news/newinfo/'.$v['id'])."'><span class='tab'>最新</span><h4>".$v['title']."<span class='date'>";
                            $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
                            break;    
                        default:
                            $html .= "<li><a href='".site_url($lang.'/news/newinfo/'.$v['id'])."'><h4>".$v['title']."<span class='date'>";
                            $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
                            break;
                    }
                }  
            }
            else
            {
                foreach ($data as $v) 
                {
                    $html .= "<li><a href='".site_url($lang.'/news/newinfo/'.$v['id'])."'><h4>".$v['title']."<span class='date'>";
                    $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
                }  
            }
}
else
{
 if($num === 1)
 {
     foreach ($data as $k=>$v) 
     { 
         switch ($k) 
         {
             case 0:
                 $html .= "<li class='top'><a href='".site_url($lang.'/news/notice/'.$v['id'])."'><span class='tab'>置顶</span><h4>".$v['title']."<span class='date'>";
                 $html .= date('Y-m-d',$v['updatetime'])."</span></h4><span>By 合一大学官方中文网s站</span><p>".msubstr($v['desc'], 0,50,'utf-8','>>')."</p></a></li>";
                 break;
             case 1:
                 $html .= "<li class='hot'><a href='".site_url($lang.'/news/notice/'.$v['id'])."'><span class='tab'>热门</span><h4>".$v['title']."<span class='date'>";
                 $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
                 break;
             case 2:
                 $html .= "<li class='hot'><a href='".site_url($lang.'/news/notice/'.$v['id'])."'><span class='tab'>最新</span><h4>".$v['title']."<span class='date'>";
                 $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
                 break;    
             default:
                 $html .= "<li><a href='".site_url($lang.'/news/notice/'.$v['id'])."'><h4>".$v['title']."<span class='date'>";
                 $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
                 break;
         }
     }  
 }
 else
 {
     foreach ($data as $v) 
     {
         $html .= "<li><a href='".site_url($lang.'/news/notice/'.$v['id'])."'><h4>".$v['title']."<span class='date'>";
         $html .= date('Y-m-d',$v['updatetime'])."</span></h4></a></li>";
     }  
 }   
}
        }
        $pl = $total > $perpage ? $this->page_list->display(site_url($lang.'/news/index/?page=-page-')) : '';

        $callback = array(
          'pl' =>$pl,
          'news'=>$html
        );
        echo json_encode($callback);exit();
    }
}