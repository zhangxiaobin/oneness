<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wisdom extends MY_Controller 
{
	private $common_msg;
	private $common_url;
	private $common_lang;
    /**
     * 构造函数
     */
    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('home_model', 'home');
    	$this->common_url = $this->uri_str;  //获取当前链接
    	$langs = substr($this->common_url, 0,2);
    	if($langs != 'cn' && $langs != 'en')
    	{
    	   $this->common_lang = 'cn';
    	}
    	else
    	{
    	  $this->common_lang = $langs;
    	}

    	$this->lang->load('wisdom');
    	$this->lang->load('common');   //获取语言文件
    	$this->common_msg = array_merge(lang('common'),lang('wis')) ;    //获取语言文字
    }
	/**
	 * Index Page for this controller.
	 */

	public function index()
	{
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $flag = $data['lang'] == 'en' ? 0 : 1;  //数据库标识
        $this->load->library('page_list');

        $wi_table = $data['lang'] == 'en' ? 'wisdom_art_'.$data['lang'] : 'wisdom_art';
        $tmp_wisdoms = $this->_get_pages($wi_table,6,2);
        $data['all_wisdom'] = $tmp_wisdoms['news'];
        $data['wpl'] = $tmp_wisdoms['pl'];

        //获取所有温馨每周
        $we_table = $data['lang'] == 'en' ? 'week_words_'.$data['lang'] : 'week_words';
        $tmp_weeks = $this->_get_pages($we_table,10,'');
        $data['all_weeks'] = $tmp_weeks['news'];
        $data['pl'] = $tmp_weeks['pl'];
        // p($data['pl'] );

        //获取合一智慧
        template('wisdom/index',$data);
	}

  //获取合一智慧文章强请
  public function article()
  {
    $id = $this->uri->segment(4);//获取id
    $data['uri'] = $this->common_url;  //获取当前链接
    $data['lang']= $this->common_lang;  //获取语言标识
    $data['lan'] = $this->common_msg;   //获取语言包信息
    $wi_table = $data['lang'] == 'en' ? 'wisdom_art_'.$data['lang'] : 'wisdom_art';
    $data['art'] = $this->home->get_one_wis_art($wi_table,$id);
    //获取其他比较靠前的文章
    $data['other_art'] = $this->home->get_other_art($wi_table);
    // p($data['other_art']);
    template('wisdom/article_info',$data);
  }

//ajax获取分页
    public function load_page_weeks()
    {
      $num = intval($this->input->post('num'));
      $lang = trim($this->input->post('lang'));
      $table = $lang == 'en' ? 'week_words_'.$lang : 'week_words';          
      $perpage = 10;  //每页显示条数
      $total = $this->db->count_all_results($table);//总条数

      $page = $num;
      $page = @intval($page);
      if($page<=1) $page = 1;
      $this->load->library('page_list',array('total'=>$total,'size'=>$perpage,'page'=>$page));
      $offset=$perpage*($page-1);
      $data  = $this->home->get_all_weeks($table,$perpage,$offset);
      $html = '';
      if(! empty($data))
      {
          foreach ($data as $v)
          {
            if($lang == 'cn')
            {
                $html .= "<li><p class='week_word'>".$v['content']."</p>";
                $html .= "<p class='time'>发布时间: ".date('Y-m-d',$v['addtime'])."<span>【第".current_week($v['addtime'])."周】</span></p></li>";
            }
            else
            {
                $html .= "<li><p class='week_word'>".$v['content']."</p>";
                $html .= "<p class='time'>Release time: ".date('Y-m-d',$v['addtime'])."<span>【第".current_week($v['addtime'])."周】</span></p></li>";
            }
      
          }    
      }
      $pl = $total > $perpage ? $this->page_list->display(site_url($lang.'/about/news/?page=-page-')) : '';

      $callback = array(
        'pl' =>$pl,
        'news'=>$html
      );

      echo json_encode($callback);exit();  
    }

//ajax获取合一智慧分页
    public function load_page_wisdoms()
    {
        $num = intval($this->input->post('num'));
        $lang = trim($this->input->post('lang'));  
        $table = $lang == 'en' ? 'wisdom_art_'.$lang : 'wisdom_art';   
        $perpage = 6;  //每页显示条数
        $total = $this->db->count_all_results($table);//总条数  
        $page = $num;
        $page = @intval($page);
        if($page<=1) $page = 1;
        $this->load->library('page_list',array('total'=>$total,'size'=>$perpage,'page'=>$page));
        $offset=$perpage*($page-1);
        $data  = $this->home->get_all_weeks($table,$perpage,$offset,2);
        $html = '';
        if(! empty($data))
        {
            foreach ($data as $v)
            {
              if($v['type'] !=='vedio')
              {
                $html .= "<li><a href='".site_url($lang.'/wisdom/article/'.$v["id"])."'><p class='event_pic'><img src='".base_url()."uploads/wisdom/".$v['image']."'><span class='active_bg'></span></p>";
              }
              else
              {
                $html .= "<li><a href='".site_url($lang.'/wisdom/article/'.$v["id"])."'><p class='event_pic'><img src='".base_url()."uploads/wisdom/".$v['image']."'><span class='active_bg'></span><span class='icon_paly'/></span></p>";    
              }

                $html .= "<p class='event_text text-cn'>".$v['title']."</p></li>";
            }    
        }
        $pl = $total > $perpage ? $this->page_list->display(site_url($lang.'/about/news/?page=-page-')) : '';

        $callback = array(
          'pl' =>$pl,
          'news'=>$html
        );

        echo json_encode($callback);exit(); 
    }
    //getpages   分页
    private function _get_pages($table,$perpage,$wisdom='')
    {
        $perpage = $perpage;  //每页显示条数
        $total = $this->db->count_all_results($table);//总条数
        $page = @intval($this->input->get('page'));
        if($page<=1) $page = 1;
        $this->page_list->initialize(array('total'=>$total,'size'=>$perpage,'page'=>$page));
        $offset=$perpage*($page-1);
        $data = $this->home->get_all_weeks($table,$perpage,$offset,$wisdom);
        $pl = $total > $perpage ? $this->page_list->display() : '';
        return array(
          'pl' =>$pl,
          'news'=>$data
        );
    }
}