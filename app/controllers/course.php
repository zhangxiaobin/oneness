<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course extends MY_Controller 
{
	private $common_msg;
	private $common_url;
	private $common_lang;
    private $month_d = array(
            0   => array('一月','二月','三月'),
            1   => array('四月','五月','六月'),
            2   => array('七月','八月','九月'),
            3  => array('十月','十一月','十二月')
         );
    /**
     * 构造函数
     */
    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('home_model', 'home');
    	$this->common_url = $this->uri_str;  //获取当前链接
    	$langs = substr($this->common_url, 0,2);
    	if($langs != 'cn' && $langs != 'en')
    	{
    	   $this->common_lang = 'cn';
    	}
    	else
    	{
    	   $this->common_lang = $langs;
    	}

    	$this->lang->load('course');
    	$this->lang->load('common');   //获取语言文件
    	$this->common_msg = array_merge(lang('common'),lang('cour')) ;    //获取语言文字
    }
	/**
	 * Index Page for this controller.
	 */

	public function index()
	{
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $flag = $data['lang'] == 'en' ? 0 : 1;  //数据库标识

        //获取所有的课程
        $co_table = $data['lang'] == 'en' ? 'course_'.$data['lang'] : 'course';
        $data['courses'] = $this->home->get_all_course($co_table);
        //获取所有的课程安排
        $year = date('Y',NOW);
        $sc_table = $data['lang'] == 'en' ? 'schedule_'.$data['lang'] : 'schedule';
        $data['schedules'] = $this->home->get_all_schedule($sc_table,$year);

        $arr = unserialize($data['schedules']['con']);
        if(! empty($arr))
        {
            //取出课程最大
            $data['max_num'] = $this->get_max_num($arr);      
        }
        else
        {
           $data['max_num'] = 1;
        }
        $data['month'] = $this->month_d;  
// p($arr);
        //当前月份
        $data['cur_mon'] = date('m',NOW);
		template('course/index',$data);
	}

    //ajax获取年度课程表
    public function load_schedules()
    {
        $year = intval($this->input->post('year'));
        $lang = trim($this->input->post('lang'));
        $table = $lang == 'en' ? 'schedule_'.$lang : 'schedule';
        $schedules = $this->home->get_all_schedule($table,$year);
        if(empty($schedules))
        {
           $callback = array(
             'str' =>'',
             'status'=>0
           );
           echo json_encode($callback);exit(); 
        }
        $arr = unserialize($schedules['con']);
        //取出课程最大
        $max_num = $this->get_max_num($arr);
        $month = $this->month_d;
        $cur_mon = date('m',NOW);

        $str = '';
        $tmp_allsch = unserialize($schedules['con']);
        $allsch = array_chunk($tmp_allsch, 3);
        foreach ($allsch as $k=>$v)
        {
          $num = $k*3+1; 
          $str .= '<ul>';
          foreach ($v as $ke=>$va)
          {
            if($cur_mon == ($num+$ke))
            {
                $str .= '<li class="cur">';
            }
            else
            {
                $str .= '<li>';      
            }
            $str .= '<dl><dt>'.$month[$k][$ke].'</dt><dd>';  

            $con_num = empty($va['con']) ? 0 : intval(count($va['con']));
            $blank_num = intval($max_num - $con_num);


            if(! empty($va['con']))
            {
              foreach($va['con'] as $key =>$val)
              {
                $str .= '<p class="color_bg'.($key+1).'"><span class="title pt_title">'.$val.'<i>'.$va['remark'][$key].'</i></span><span class="time pt_time">'.$va['time'][$key].'</span></p>';
               }
            }
                    
            if($blank_num > 0)
            {
                for($n = 0;$n < $blank_num; $n++)
                {
                   $str .= '<p class="mobile-p"></p>';  
                }           
            }




            $str .= '</dd></dl></li>';
          }
          $str .= '</ul>';
        }
        $callback = array(
          'str'            =>$str,
          'status'         =>1,
          'cur'            =>$year,
          'pre'            =>($year-1),
          'nex'            =>($year+1)
        );
        echo json_encode($callback);exit();
    }

    //获取最大的课程条数
    private function get_max_num($data)
    {
        if(empty($data) && ! is_array($data)) return 0;
        $max = 0;
        foreach ($data as $v)
        {
            if(! empty($v['con']))
            {
                $tmpmax = count($v['con']);
                $max = $tmpmax > $max ? $tmpmax : $max;
            }
        }
        return $max;
    }
}
