<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends MY_Controller 
{
	private $common_msg;
	private $common_url;
	private $common_lang;
    /**
     * 构造函数
     */
    public function __construct()
    {
    	parent::__construct();
        $this->load->model('home_model', 'home');
    	$this->common_url = $this->uri_str;  //获取当前链接
    	$langs = substr($this->common_url, 0,2);
    	if($langs != 'cn' && $langs != 'en')
    	{
    	   $this->common_lang = 'cn';
    	}
    	else
    	{
    	  $this->common_lang = $langs;
    	}

    	$this->lang->load('about');
    	$this->lang->load('common');   //获取语言文件
    	$this->common_msg = array_merge(lang('common'),lang('about')) ;    //获取语言文字
    }
	/**
	 * Index Page for this controller.
	 */

	public function index()
	{
        $data['uri'] = $this->common_url;  //获取当前链接
        $data['lang']= $this->common_lang;  //获取语言标识
        $data['lan'] = $this->common_msg;   //获取语言包信息
        $flag = $data['lang'] == 'en' ? 0 : 1;  //数据库标识

        //获取校园简介
        $ab_table = $data['lang'] == 'en' ? $data['lang'].'_about' : 'about';
        $data['compus'] = $this->home->get_conpus_about($ab_table);

        //获取校园图片
        $data['com_pic'] = $this->home->get_uni_pics();

		template('about/index',$data);
	}
}