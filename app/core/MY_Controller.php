<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $uri_str;
    public function __construct()
    {
        parent::__construct();
        // P($_SERVER);exit();
        if($_SERVER['HTTP_HOST'] == 'onenesschina.org' || $_SERVER['HTTP_HOST'] == 'www.onenesschina.org' )
        {
            header("Content-type: text/html; charset=utf-8");
            echo "<img src='".base_url()."soon.jpg' style='width:100%;height:100%'>";exit();
        }
        $tmp_lang = $this->uri->segment(1);
        if($tmp_lang != get_cookie('lang'))
        {
            $lang = empty($tmp_lang) ? 'cn' : $tmp_lang;
            setcookie('lang',$lang);        
        }

        $tmp_str = $this->uri->uri_string();
        $this->uri_str = empty($tmp_str) ? 'cn' : $tmp_str; 
    }

    /**
     * Ajax输出
     * @param $data 数据
     * @param string $type 数据类型 text html xml json
     */
    protected function ajax($data, $type = "JSON")
    {
        $type = strtoupper($type);
        switch ($type) 
        {
            case "TEXT" :
                $_data = $data;
                break;
            default :
                $_data = json_encode($data);
        }
        echo $_data;
        exit;
    }

   //error函数
    protected function error($msg = ':(  出错了！',$url = '',$time = 1)
    {
        $url = $url ? "window.location.href='" . site_url($url) . "'" : "window.location.href='".DX_HISTORY."'";
        $this->load->view('common/error',array('msg' => $msg, 'url' => $url, 'time' => $time));
        echo $this->output->get_output();
        exit();
    }
    //success函数
    protected function success($msg = ':)  操作成功',$url = '',$time = 1)
    {
        $url = $url ? "window.location.href='" . site_url($url) . "'" : "window.location.href='".DX_HISTORY."'";
        $this->load->view('common/success',array('msg' => $msg, 'url' => $url, 'time' => $time));
        echo $this->output->get_output();
        exit();
    }
}