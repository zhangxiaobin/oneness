<?php 
$lang['cour']  = array(
   'home_lang_test'               => '合一课程',
   'cou_introduction'             =>'课程介绍',
   'cou_connect_peop'             =>'联系组团人',
   'cou_schedule'                 =>'课程表',
   'cou_keep_week'                =>'维持周期',    
   'cou_pre_schedule'             =>'上一年课程表',   
   'cou_next_schedule'            =>'下一年课程表',         
);