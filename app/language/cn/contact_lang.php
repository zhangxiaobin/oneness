<?php 
$lang['con']  = array(
   'home_lang_test'               => 'contact',
   'con_vou_name'                 => '您的姓名',
   'con_vou_contract'             => '您的联系方式',
   'con_vou_email'                => '您的邮箱地址',
   'con_vou_leav_msg'             => '您的留言',
   'con_vou_desc'                 => '合一志愿者的招募分为图片编辑，学员分享，文字编辑等方面招募',
   'con_vou_submit'               => '提交申请',
   'con_vou_recruit'              =>'志愿者招募',
   'con_vou_con'                  =>'联系我们',
   'con_vou_con_add'              =>'欢迎加入我们，地址: 印度安德拉邦',
   'con_vou_post'                 =>'邮编'
);