<?php 
$lang['home']  = array(
   'home_lang_test'               => '测试',
   'home_sch_desc'                => '学校简介',
   'home_sch_walk'                => '走进校园',
   'home_sch_wisdom'              => '合一智慧',
   'home_sch_study'               => '学习',
   'home_sch_grow'                => '成长',
   'home_sch_yulu'                =>'“生命就是关系。”',
   'home_abo_more'                => '展开阅读更多',
   'home_warm_week'               => '|温|馨|每|周|',


   'home_more'                    =>'查看更多 >',
   'home_announce'                =>'合一公告',
   'home_an_news'                 =>'合一新闻',
   'home_present_more'            => '我们为您呈现的还有更多',
   'home_schedule'                => '课程表',
   'home_course_intro'            => '课程介绍',

   'home_first_qua'               =>'第一季度',
   'home_second_qua'              =>'第二季度',
   'home_third_qua'               =>'第三季度',
   'home_fouth_qua'               =>'第四季度',
   'home_qua_desc'                =>'全部日期包括抵达日和离开日期',
   'home_course_desc'             =>'学员必须之前参加了完整4周深化课/觉醒之旅课程',

   'home_meet_oneness'            =>'遇见合一',
   'home_meet_yourself'           =>'遇见最美的自己',


   'home_art_write'               =>'文',
   'home_stu_share'               =>'学员分享',
   'home_present'                 =>'主讲人',
   'home_publish'                 =>'发布时间',
   'home_grow_up'                 =>'你成长，我们陪伴',
   'home_after_sch'               =>'|课|后|辅|导|',
   'home_day'                     => '天',
   'home_student'                 => '学员',
   'home_country'                 => '国家',

   'home_contact_us'              =>'联系我们',
   'home_submit'                  =>'提交',
   'home_email'                   =>'你的电子邮箱:',

   'home_contact_desc'            =>'请通过电话、微信联系离你最近的组团人，获取更多课程信息。',
   'home_our_address'             =>'我们的地址：',
   'home_address_c'               =>'印度安德拉邦曼达尔奇区 Varadailahpalem Battallavallam',
   'home_apply_info1'             =>'要获取合一大学的最新信息，',
   'home_apply_info2'             =>'请留下你的邮箱地址。',
   'home_an_huaxu'                =>'活动花絮',
   'in_nav_zixun'                 =>'合一资讯',
   'home_stu_inter'               => '智慧解读'
 );