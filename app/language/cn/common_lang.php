<?php 
$lang['common']  = array(
	//公共头部部分
   'in_lang_cn'               => '中文',
   'in_lang_en'               => 'English',
   'in_nav_about'             => '走进合一', 
   'in_nav_dom'               => '合一智慧', 
   'in_nav_news'              => '校园快讯',
   'in_nav_cours'             => '合一课程',
   'in_nav_inter'             => '互动天地',
   'in_nav_con'               => '联络中心',
   'in_nav_menu'              => '目录',

   'wis_release_time'             => '发布时间',
   'wis_previous_art'             => '上一篇',
   'wis_next_art'                 => '下一篇',
   'wis_source'                   => '来源',
   'wis_official'                 => '合一大学中文官方网站',

   'in_view_more'                 => '查看更多 >',
      'in_foot_uni_name'          =>'合一大学',
   'in_foot_f_link'               =>'友情链接',
   'in_foot_en_web'               =>'英文官方网站',
   'in_foot_sina'                 =>'官方新浪博客',
   'in_foort_wechat'              =>'官方微信',
   'in_foot_course'               =>'合一课程',

   'in_foot_copyright'            =>'Copyright 2015 合一大学 版权所有 ©',
);