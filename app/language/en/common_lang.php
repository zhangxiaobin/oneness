<?php 
$lang['common']  = array(
	//公共头部部分
   'in_lang_cn'               => '中文',
   'in_lang_en'               => 'English',
   'in_nav_about'             => 'Entering Oneness ', 
   'in_nav_dom'               => 'Oneness wisdom', 
   'in_nav_news'              => 'News',
   'in_nav_cours'             => 'Courses',
   'in_nav_inter'             => 'Interaction',
   'in_nav_con'               => 'Contact',
   'in_nav_menu'              => 'Menu',



   'wis_release_time'             => 'Release Time',
   'wis_previous_art'             => 'Previous',
   'wis_next_art'                 => 'Next',
   'wis_source'                   => 'Source',
   'wis_official'                 => 'Oneness university official website',

   'in_view_more'                 => 'View more >',
   'in_foot_uni_name'             =>'合一大学',
   'in_foot_f_link'               =>'友情链接',
   'in_foot_en_web'               =>'英文官方网站',
   'in_foot_sina'                 =>'官方新浪博客',
   'in_foort_wechat'              =>'官方微信',
   'in_foot_course'               =>'合一课程',

   'in_foot_copyright'            =>'Copyright 2015 合一大学 版权所有 ©',

);