<?php 
$lang['home']  = array(
   'home_lang_test'               => 'test',
   'home_sch_desc'                => 'university intro',
   'home_sch_walk'                => 'ENTERING THE CAMPUS',
   'home_sch_wisdom'              => 'ONENESS WISDOM',
   'home_sch_study'               => 'Study',
   'home_sch_grow'                => 'Growing',
   'home_sch_yulu'                =>'"life is relationship."',
   'home_abo_more'                => 'Read More',
   'home_warm_week'               => 'Warm | Week',

   'home_more'                    =>'check more >',
   'home_announce'                =>'Announcement',
   'home_an_news'                 =>'NEWS',
   'home_present_more'            =>'We present to you more',
   'home_schedule'                => 'Curriculum',
   'home_course_intro'            => 'Course Introduction',

   'home_first_qua'               =>'First quarter',
   'home_second_qua'              =>'First quarter',
   'home_third_qua'               =>'First quarter',
   'home_fouth_qua'               =>'First quarter',
   'home_qua_desc'                =>'All dates include arrival and departure dates',
   'home_course_desc'             =>'Students must attend the full four weeks before the deepening class / wake Tour Courses',

   'home_meet_oneness'            =>'Meet the oneness',
   'home_meet_yourself'           =>'Find the most beautiful yourself',

   'home_art_write'               =>'write',
   'home_stu_share'               =>'Participants share',
   'home_present'                 =>'Presenter',
   'home_publish'                 =>'Published',
   'home_grow_up'                 =>'You grow up, we accompany',
   'home_after_sch'               =>'After-school | tutoring ',
   'home_day'                     =>'Days',
   'home_student'                 =>'Students',
   'home_country'                 =>'Countries',

   'home_contact_us'              =>'Contact us',
   'home_submit'                  =>'SUBMIT',
   'home_email'                   =>'Your E-mail:',

   'home_contact_desc'            =>'请通过电话、微信联系离你最近的组团人，获取更多课程信息。',
   'home_our_address'             =>'我们的地址：',
   'home_address_c'               =>'印度安德拉邦曼达尔奇区 Varadailahpalem Battallavallam',
   'home_apply_info1'             =>'要获取合一大学的最新信息，',
   'home_apply_info2'             =>'请留下你的邮箱地址。',
   'home_an_huaxu'                =>'活动花絮',
   'in_nav_zixun'                 =>'合一资讯',
   'home_stu_inter'               => '智慧解读'

);