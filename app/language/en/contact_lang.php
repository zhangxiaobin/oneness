<?php 
$lang['con']  = array(
   'home_lang_test'               => 'contact',
   'con_vou_name'                 => 'Your Name',
   'con_vou_contract'             => 'Your contact information',
   'con_vou_email'                => 'Your e-mail',
   'con_vou_leav_msg'             => 'Your message',
   'con_vou_desc'                 => 'Oneness volunteer recruitment into image editing, the participants share, text editing',
   'con_vou_submit'               => 'submit',
   'con_vou_recruit'              =>'志愿者招募',
   'con_vou_con'                  =>'联系我们',
   'con_vou_con_add'              =>'欢迎加入我们，地址: 印度安德拉邦',
   'con_vou_post'                 =>'邮编'
);