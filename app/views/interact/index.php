<link href="<?php echo base_url();?>/resource/css/interactive.css" rel="stylesheet" type="text/css">
<!--banner start-->
<section class="page_banner banner_inter" id="cont_1">
    <div class="banner_text">
          <img src="<?php echo base_url();?>/resource/images/banner_inter_text.png" data-width="365" data-height="450">
      </div>
</section>
<!--banner end-->

<!--Trainees share start-->
<?php if(! empty($all_shares) && is_array($all_shares)): ?>
<section class="main_box2" id="cont_2">
	<h3 class="title-font title-h2"><?php echo $lan['int_stu_share'] ?><span class="title_line"></span></h3>
    <div class="wisdom_con share_con">
      <ul>
        <?php foreach ($all_shares as $v): ?>
          <li>
            <a href="<?php echo site_url($lang.'/interact/newinfo/'.$v['id']) ?>">
            <img src="<?php echo base_url();?>/uploads/share/<?php echo $v['image'] ?>">
            <p class="tab">学员分享<span></span></p>
            <p class="share_text"><?php echo $v['title'] ?></p>
            <p class="share_author"><span>●</span><?php echo $v['name'] ?>  /  文</p>
            </a> 
          </li>     
        <?php endforeach ?>
      </ul>
    </div>
    
    <div class="page inter_page">
       <?php echo $spl; ?>
    </div>
    
</section>
<?php endif; ?>
<!--Trainees share start-->
<?php if(! empty($inters) && is_array($inters)): ?>
<section class="main_box2" id="cont_2">
    <h3 class="title-font title-h2">智慧解读<span class="title_line"></span></h3>
    <div class="wisdom_con inter_con">
      <ul>
        <?php foreach ($inters as $v): ?>
          <li>
            <a href="<?php echo site_url($lang.'/interact/newinfo/'.$v['id']) ?>">
            <img src="<?php echo base_url();?>/uploads/share/<?php echo $v['image'] ?>">
            <p class="tab">智慧解读<span></span></p>
            <p class="share_text"><?php echo $v['title'] ?></p>
            <p class="share_author"><span>●</span><?php echo $v['name'] ?>  /  文</p>
            </a> 
          </li>     
        <?php endforeach ?>
      </ul>
    </div>
    
    <div class="page prete_page">
       <?php echo $ipl; ?>
    </div>
    
</section>
<?php endif; ?>





<!--After-school tutoring start-->
<section class="main_box2" id="cont_3">
	<h3 class="title-font title-h2"><?php echo $lan['int_after_sch'] ?><span class="title_line"></span></h3>
    <div class="tutoring_con">
    <?php if(! empty($all_afsch) && is_array($all_afsch)): ?>
      <?php foreach ($all_afsch as $v): ?>
    	<dl>
        	<a href="<?php echo site_url($lang.'/interact/afterinfo/'.$v['id']) ?>">
        	<dt>
                <h4><?php echo $v['title'] ?></h4>
                <p><span>●</span><?php echo $lan['int_people'] ?>: <?php echo $v['name'] ?>     |     <?php echo $lan['int_pulish_time'] ?>: <?php echo date('Y-m-d',$v['updatetime']) ?></p>
            </dt>
            <dd>【<?php echo $lan['int_art_content'];?>】<?php echo msubstr($v['desc'],0,50,'utf-8','...');?> </dd>
            </a>
        </dl>
         <?php endforeach ?>

        <?php endif; ?>
    </div>
    <div class="page aftsch_page">
        <?php echo $apl; ?>
    </div>
</section>
<!--After-school tutoring end-->

<!--Contact Center start-->
<?php if(! empty($contacters) && is_array($contacters)): ?>
<section id="cont_4">
  <div class="main_box2">
    <h3 class="title-font title-h2"><?php echo $lan['in_nav_con'] ?><span class="title_line"></span></h3>
    <div class="contact_center">
    <?php foreach ($contacters as $k=>$v): ?>
        <dl <?php if($k > 2): ?>  class="noline"<?php endif; ?>>
            <dt>
            <h4><?php echo $v['name'] ?></h4>
              <p><?php echo $v['team'] ?><span class="line"></span></p>
            </dt>
            <dd>
              <?php $phones = explode(',', $v['phone'])?>
            <p class="phone">
            <span>
            <?php foreach ($phones as $ke => $va): ?>
              <a href="tel:<?php echo $va ?>"><?php echo $va ?></a>
            <?php endforeach ?>
            </span>
            </p>
              <p class="wechat"><?php echo $v['wechat'] ?></p>
              <p class="address"><?php echo $v['city'] ?></p>
            </dd>
        </dl>
        <?php endforeach ?>

    </div>
  </div>
</section>
<?php endif; ?>
<!--Contact Center end--> 
<script>
   $(function (){
    $(".inter_page").on('click',".page_con a",function(){
        num = $(this).attr('data-num');
        if(typeof(num) === undefined)
        {
            return false;
        }
        lang = "<?php echo $lang; ?>";
        $.ajax({
            url:"<?php echo site_url('interact/load_page_share'); ?>",
            type: 'POST',
            data: {num:num,lang:lang},
            dataType:'json', 
            success: function(data)
            {
                 if(data)
                 {
                    $(".inter_page").html('');
                    $(".inter_page").append(data.pl);
                    $('.share_con ul li').remove();
                    $('.share_con ul').append(data.news);
                 }
                 else
                 {
                    alert('没有了');
                 }
            }
        });
    })

    //==================
    $(".aftsch_page").on('click',".page_con a",function(){
        num = $(this).attr('data-num');
        if(typeof(num) === undefined)
        {
            return false;
        }
        lang = "<?php echo $lang; ?>";
        $.ajax({
            url:"<?php echo site_url('interact/load_page_aftersch'); ?>",
            type: 'POST',
            data: {num:num,lang:lang,flag : 1},
            dataType:'json', 
            success: function(data)
            {
                 if(data)
                 {
                    $(".aftsch_page").html('');
                    $(".aftsch_page").append(data.pl);
                    $('.tutoring_con dl').remove();
                    $('.tutoring_con').append(data.news);
                 }
                 else
                 {
                    alert('没有了');
                 }
            }
        });
    })

    //
    $(".prete_page").on('click',".page_con a",function(){
        num = $(this).attr('data-num');
        if(typeof(num) === undefined)
        {
            return false;
        }
        lang = "<?php echo $lang; ?>";
        $.ajax({
            url:"<?php echo site_url('interact/load_page_inter'); ?>",
            type: 'POST',
            data: {num:num,lang:lang},
            dataType:'json', 
            success: function(data)
            {
                 if(data)
                 {
                    $(".prete_page").html('');
                    $(".prete_page").append(data.pl);
                    $('.inter_con ul li').remove();
                    $('.inter_con ul').append(data.news);
                 }
                 else
                 {
                    alert('没有了');
                 }
            }
        });
    })
   })
</script>