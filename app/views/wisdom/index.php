<link href="<?php echo base_url();?>/resource/css/wisdom.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!--banner start-->
<section class="page_banner banner_wisdom" id="cont_1">
    <div class="banner_text">
        <img src="<?php echo base_url();?>/resource/images/banner_wisdom_text.png" data-width="365" data-height="450">
    </div>
</section>
<!--banner end-->

<!--oneness wisdom start-->
<section id="cont_3">
  <div class="main_box2">
    <h3 class="title-font title-h2"><?php echo $lan['in_nav_dom'] ?><span class="title_line"></span></h3>
    <!--合一智慧列表 start-->
    <?php if(! empty($all_wisdom) && is_array($all_wisdom)): ?>
        <div class="wisdom_box">
            <ul class="wisdom_list">
            <?php foreach ($all_wisdom as $v): ?>
              <li>
              	<a href="<?php echo site_url($lang.'/wisdom/article/'.$v["id"]) ?>">
                <p class="event_pic"><img src="<?php echo base_url();?>/uploads/wisdom/<?php echo $v['image'] ?>"><span class="active_bg"></span></p>
                <p class="event_text text-cn"><?php echo $v['title'] ?></p>
                </a>
              </li>
            <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="wisdom_page page">
    <?php echo $wpl; ?>
    </div>
    <!--合一智慧列表 end--> 
  </div>
</section>
<!--oneness wisdom end--> 

<!--温馨每周 start-->
<section>
    <div class="main_box2">
    	<h3 class="title-font title-h2"><?php echo $lan['home_warm_week'] ?><span class="title_line"></span></h3>
        <div class="week_con">
        	<ul>
            <?php if(! empty($all_weeks) && is_array($all_weeks)): ?>
                <?php foreach ($all_weeks as $v): ?>
                <li>
                    <p class="week_word"><?php echo $v['content'] ?></p>
                    <p class="time"><?php echo $lan['home_re_time'] ?>: <?php echo date('Y-m-d',$v['addtime']); ?><span>【第<?php echo current_week($v['addtime']) ?>周】</span></p>
                </li>
                <?php endforeach ?>
             <?php endif; ?>
            </ul>
        </div>
        <div class="week_page page">
           <?php echo $pl; ?>
        </div>
    <!--合一智慧列表 end--> 
    </div>	
</section>
<!--温馨每周 end-->
<script>
   $(function (){
    $(".week_page").on('click',".page_con a",function(){
        num = $(this).attr('data-num');
        if(typeof(num) === undefined)
        {
            return false;
        }
        lang = "<?php echo $lang; ?>";
        $.ajax({
            url:"<?php echo site_url('wisdom/load_page_weeks'); ?>",
            type: 'POST',
            data: {num:num,lang:lang},
            dataType:'json', 
            success: function(data)
            {
                 if(data)
                 {
                    $(".week_page").html('');
                    $(".week_page").append(data.pl);
                    $('.week_con ul li').remove();
                    $('.week_con ul').append(data.news);
                 }
                 else
                 {
                    alert('没有了');
                 }
            }
        });
    })
//合一智慧文章
    $(".wisdom_page").on('click',".page_con a",function(){
        num = $(this).attr('data-num');
        if(typeof(num) === undefined)
        {
            return false;
        }
        lang = "<?php echo $lang; ?>";
        $.ajax({
            url:"<?php echo site_url('wisdom/load_page_wisdoms'); ?>",
            type: 'POST',
            data: {num:num,lang:lang},
            dataType:'json', 
            success: function(data)
            {
                 if(data)
                 {
                    $(".wisdom_page").html('');
                    $(".wisdom_page").append(data.pl);
                    $('.wisdom_box ul li').remove();
                    $('.wisdom_box ul').append(data.news);
                 }
                 else
                 {
                    alert('没有了');
                 }
            }
        });
    })
   })
</script>