<link href="<?php echo base_url();?>/resource/css/contact.css" rel="stylesheet" type="text/css">
<style>
    .reg_error{
      font-size: 14px;
      color: #e60033;
      display: block;
    }
    .reg_right{
        display: none;
    }
     p.error{
     border:1px solid #e60033;
    }
</style>
<!--banner start-->
<section class="page_banner banner_contact" id="cont_1">
    <div class="banner_text">
        <img src="<?php echo base_url();?>/resource/images/banner_contact_text.png" data-width="365" data-height="450">
    </div>
</section>
<!--banner end-->

<!--Contact Center start-->
<?php if(! empty($contacters) && is_array($contacters)): ?>
<section id="cont_2">
  <div class="main_box2">
    <h3 class="title-font title-h2"><?php echo $lan['in_nav_con'] ?><span class="title_line"></span></h3>
    <div class="contact_center">
    <?php foreach ($contacters as $k=>$v): ?>
    	<dl <?php if($k > 2): ?>  class="noline"<?php endif; ?>>
        	<dt>
            <h4><?php echo $v['name'] ?></h4>
              <p><?php echo $v['team'] ?><span class="line"></span></p>
            </dt>
            <dd>
              <?php $phones = explode(',', $v['phone'])?>
            <p class="phone">
            <span>
            <?php foreach ($phones as $ke => $va): ?>
              <a href="tel:<?php echo $va ?>"><?php echo $va ?></a>
            <?php endforeach ?>
            </span>
            </p>
              <p class="wechat"><?php echo $v['wechat'] ?></p>
              <p class="address"><?php echo $v['city'] ?></p>
            </dd>
        </dl>
        <?php endforeach ?>

<!--        
        <dl>
        	<dt>
            	<h4>张亚娟</h4>
                <p>合一家园<span class="line"></span></p>
            </dt>
            <dd>
            	<p class="phone"><span><a href="tel:13706111789">13706111789</a> | <a href="tel:15906118313">15906118313</a></span><span><a href="tel:400-822-0989">400-822-0989</a></span></p>
                <p class="wechat">oneness100</p>
                <p class="address">全国各地 | 常州</p>
            </dd>
        </dl> -->
    </div>
  </div>
</section>
<?php endif; ?>
<!--Contact Center end--> 

<!--Volunteer Recruitment start-->
<section id="cont_3">
    <form name="postmsg" action="contact/leave_msg" method="post">
    	<div class="main_box2">
        	<h3 class="title-font title-h2"><?php echo $lan['con_vou_recruit'] ?><span class="title_line"></span></h3>
            <div class="recruitmen_form">
            	<p>
                  <input name="name" type="text" value="<?php echo $lan['con_vou_name'] ?>" onFocus="if(this.value=='<?php echo $lan['con_vou_name'] ?>'){this.value='';style.color='#333'}" onBlur="if(this.value==''){this.value='<?php echo $lan['con_vou_name'] ?>';style.color='#999'}" class="input">
                  <span class = 'reg_right'></span>
                </p>
                <p>
                    <input name="tel" type="text" value="<?php echo $lan['con_vou_contract'] ?>" onFocus="if(this.value=='<?php echo $lan['con_vou_contract'] ?>'){this.value='';style.color='#333'}" onBlur="if(this.value==''){this.value='<?php echo $lan['con_vou_contract'] ?>';style.color='#999'}" class="input">
                    <span class = 'reg_right'></span>
                </p>
                <p>
                    <input name="email" type="text" value="<?php echo $lan['con_vou_email'] ?>" onFocus="if(this.value=='<?php echo $lan['con_vou_email'] ?>'){this.value='';style.color='#333'}" onBlur="if(this.value==''){this.value='<?php echo $lan['con_vou_email'] ?>';style.color='#999'}" class="input">
                    <span class = 'reg_right'></span>
                </p>
                <p>
                   <textarea name="msg" cols="" rows="" onFocus="if(this.value=='<?php echo $lan['con_vou_leav_msg'] ?>'){this.value='';style.color='#333'}" onBlur="if(this.value==''){this.value='<?php echo $lan['con_vou_leav_msg'] ?>';style.color='#999'}"><?php echo $lan['con_vou_leav_msg'] ?></textarea>
                    <span class = 'reg_right'></span>
                </p>
            </div>
            <div class="btn_box"><p><span>●</span><?php echo $lan['con_vou_desc'] ?></p><input name="" type="submit" value="<?php echo $lan['con_vou_submit'] ?> >" class="submit_btn"></div>
        </div>
    </form>
</section>
<!--Volunteer Recruitment start-->

<!--Contact Us start-->
<section id="cont_3">
	<div class="main_box2">
    	<h3 class="title-font title-h2"><?php echo $lan['con_vou_con'] ?><span class="title_line"></span></h3>
        <div class="contact_map">
        	<p class=""><img src="<?php echo base_url();?>/resource/images/contact_map.jpg" data-width="554" data-height="377"></p>
            <p class="fr"><img src="<?php echo base_url();?>/resource/images/contact_map.jpg" data-width="554" data-height="377"></p>
        </div>
      <div class="contact_con">
      		<div class="contact_text">
                <p><span><?php echo $lan['con_vou_con_add'] ?></span>  <span><?php echo $lan['con_vou_post'] ?>: 517541</span></p><P>E-mail: <a href="mailto:onenesschina@vip.163.com">onenesschina@vip.163.com</a></p>
            </div>
            <form name="leavemsg" action="contact/message" method="post">
            	<div class="contact_form">
                    <p>
                       <input name="lname" type="text" value="<?php echo $lan['con_vou_name'] ?>" onFocus="if(this.value=='<?php echo $lan['con_vou_name'] ?>'){this.value='';style.color='#333'}" onBlur="if(this.value==''){this.value='<?php echo $lan['con_vou_name'] ?>';style.color='#999'}" class="input">
                       <span class = 'reg_right'></span>
                    </p>
                    <p>
                      <input name="lemail" type="text" value="<?php echo $lan['con_vou_email'] ?>" onFocus="if(this.value=='<?php echo $lan['con_vou_email'] ?>'){this.value='';style.color='#333'}" onBlur="if(this.value==''){this.value='<?php echo $lan['con_vou_email'] ?>';style.color='#999'}" class="input">
                      <span class = 'reg_right'></span>
                    </p>
                    <p class="message">
                       <textarea  name="lmsg" cols="" rows="" onFocus="if(this.value=='<?php echo $lan['con_vou_leav_msg'] ?>'){this.value='';style.color='#333'}" onBlur="if(this.value==''){this.value='<?php echo $lan['con_vou_leav_msg'] ?>';style.color='#999'}"><?php echo $lan['con_vou_leav_msg'] ?></textarea>
                        
                    </p>
                    <span class = 'reg_right'></span>
                    <input name="" type="submit" value="提交申请 >" class="submit_btn">
                </div>
            </form>
      </div>
    </div>
</section>
<!--Contact Us start-->
<script type="text/javascript">
$(function(){
	_focus = function(){
	
	}
})
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>resource/js/validate.js"></script>