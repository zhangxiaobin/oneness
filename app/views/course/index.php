<link href="<?php echo base_url();?>/resource/css/course.css" rel="stylesheet" type="text/css">
<style>
  .text p{
    padding-bottom: 20px;
  }
</style>
<!--banner start-->
<section class="page_banner banner_curriculum" id="cont_1">
    <div class="banner_text">
          <img src="<?php echo base_url();?>/resource/images/banner_course_text.png" data-width="365" data-height="450">
      </div>
</section>
<!--banner end--> 

<!--Contact Us start-->
<section id="cont_2">
  <div class="main_box2">
    <h3 class="title-font title-h2"><?php echo $lan['cou_introduction'] ?><span class="title_line"></span></h3>
    <div class="tab_nav"> 
    <?php if(! empty($courses)): ?>
      <?php foreach ($courses as $k=>$v): ?>
          <a href="javascript:" <?php if($k == 0): ?>class="active" <?php endif; ?>>
             <?php echo $v['title'] ?>
          </a>
      <?php endforeach; ?>
      <?php endif; ?>
    </div>
    
    <!--1觉醒之旅 start-->
    <?php if(! empty($courses)): ?>
      <?php foreach ($courses as $k=>$v): ?>
        <div class="introduction_box"<?php if($k != 0): ?> style="display:none;" <?php endif; ?>>
          <div class="curriculum_pic">
             <img src="<?php echo base_url();?>/uploads/course/<?php echo $v['image'] ?>">
          </div>
          <div class="introduction_con">
            <div class="title">
              <div class="time_box">
                <p class="time"><?php echo $v['day'] ?></p>
                <p class="name"><?php echo $lan['cou_keep_week'] ?></p>
              </div>
              <div class="title_text">
                <p>帮助你进入满足、转化、自由和觉醒的课程</p>
                <h4><?php echo $v['title'] ?></h4>
                <!-- <p class="gray">合一合作伙伴</p> -->
              </div>
            </div>
            <div class="text">
              <p style="padding-bottom:10px"><?php echo $v['desc'] ?></p>
              <?php echo $v['content'] ?>
              <a href="<?php echo site_url($lang.'/contact');?>" class="contact_btn"><?php echo $lan['cou_connect_peop'] ?></a> 
              </div>
          </div>
        </div>
    <?php endforeach; ?>
    <?php endif; ?>
    <!--1觉醒之旅 end--> 
    
  </div>
</section>
<!--Contact Us start--> 

<!--Curriculum start-->
<?php if(! empty($schedules) && is_array($schedules)): ?>
<section class="main_box3">
  <h3 class="title-font title-h2"><?php echo $lan['cou_schedule'] ?><span class="title_line"></span></h3>
  <div class="year_switch">
    <a href="javascript:"  year = '<?php echo ($schedules['year']-1); ?>'  class="year_prev"><?php echo $lan['cou_pre_schedule'] ?></a>
      <h4><?php echo $schedules['year']; ?></h4>
    <a href="javascript:"  year = '<?php echo ($schedules['year']+1); ?>' class="year_next"><?php echo $lan['cou_next_schedule'] ?></a></div>
  <div class="curriculum">
   <?php 
      $tmp_allsch = unserialize($schedules['con']);
      $allsch = array_chunk($tmp_allsch, 3);
      foreach ($allsch as $k=>$v): 
        $num = $k*3+1; 
    ?> 

    <ul>
      <?php foreach ($v as $ke=>$va): ?>
       <li <?php if($cur_mon == ($num+$ke)):?> class="cur"<?php endif; ?>>
        <dl>
          <dt><?php echo $month[$k][$ke] ?></dt>
          <dd>
<!-- 2015.3.20 ad -->
          <!-- 获取课程的个数   con -->
           <?php 
              $con_num = empty($va['con']) ? 0 : intval(count($va['con']));
              $blank_num = intval($max_num - $con_num);

            ?>

      <?php if(! empty($va['con'])): ?>
           <?php foreach($va['con'] as $key =>$val){ ?>
              <p class="color_bg<?php echo ($key+1) ?>">
                  <span class="title pt_title"><?php echo $val ?><i><?php echo $va['remark'][$key];?></i></span>
                  <span class="time pt_time"><?php echo $va['time'][$key] ?></span>
              </p>
           <?php } ?>
      <?php endif; ?>
      <?php if($blank_num > 0): ?>
            <?php for($n = 0;$n < $blank_num; $n++){ ?>
               <p class="mobile-p"></p>
            <?php } ?>
      <?php endif; ?>

          </dd>
        </dl>
      </li> 
      <?php endforeach ?>
    </ul>
<?php endforeach; ?>
  </div>
</section>
<?php endif; ?>
<!--Curriculum end--> 
<script type="text/javascript">
$(function(){
	var campusNav = $(".tab_nav a").click(function(){
		var idx = campusNav.index(this);
		$(this).addClass("active").siblings().removeClass("active");
		$(".introduction_box").eq(idx).fadeIn(800).siblings(".introduction_box").fadeOut(30);
	})


  //ajax获取一年课程安排
  $('.year_switch a').click(function (){
    year = $(this).attr('year');
    if(typeof(year) === undefined)
    {
        return false;
    }
    lang = "<?php echo $lang; ?>";
    $.ajax({
        url:"<?php echo site_url('course/load_schedules'); ?>",
        type: 'POST',
        data: {year:year,lang:lang},
        dataType:'json', 
        success: function(data)
        {
          if(data.status == 1)
          {
              $(".curriculum").html('');
              $(".curriculum").append(data.str);
              $('.year_switch .year_prev').attr('year',data.pre);
              $('.year_switch .year_next').attr('year',data.nex);
               $('.year_switch h4').text(data.cur);
          }
          else
          {
            alert('没有了');
          }
          // alert(3)
             // if(data)
             // {
             //    $(".new_page").html('');
             //    $(".new_page").append(data.pl);
             //    $('.new_news ul li').remove();
             //    $('.new_news ul').append(data.news);
             // }
             // else
             // {
             //    alert('没有了');
             // }
        }
    });
  })
})
</script>