<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resource/fancybox/jquery.fancybox.css" media="screen" />
<link href="<?php echo base_url();?>/resource/css/about.css" rel="stylesheet" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.<?php echo base_url();?>/resource/js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Add fancyBox main JS and CSS files -->
<!--banner start-->
<section class="page_banner banner_about" id="cont_1">
    <div class="banner_text">
        <img src="<?php echo base_url();?>/resource/images/banner_about_text.png" data-width="365" data-height="450">
    </div>
</section>
<!--banner end-->

<!--School Profile start -->
<section id="cont_2" class="main_box">
	<h3 class="title-font title-h2"><?php echo $lan['ab_compus_intro'] ?><span class="title_line"></span></h3>
    <div class="introduction_box">
        <div class="introduction_pic"><img src="<?php echo base_url();?>/resource/images/introduction_pic.jpg" data-width="562" data-height="561"></div>
        <div class="introduction_text">
        	<h4><?php echo $lan['ab_stu_grow'] ?><span></span></h4>
            <p><?php echo $compus[0]; ?></p>
            <P><?php echo empty($compus[1]) ? '' : $compus[1]; ?></P>
        </div>
    </div>
</section>
<!--School Profile end -->

<!--Into the campus start-->
<section id="cont_3" class="campus_box">
	<h3 class="title-font title-h2"><?php echo $lan['ab_step_compus'] ?><span class="title_line"></span></h3>
    <?php if(! empty($com_pic)): ?>
    <div class="main_box">
    	<div class="tab_nav">
        <!-- class="active" -->
            <?php foreach ($com_pic as $v): ?>
                <a href="javascript:"><?php echo $lang == 'cn' ? $v['uname'] : $v['enuname'];?></a> 
            <?php endforeach ?>
        </div>
        
        <?php foreach ($com_pic as $k=>$v): ?>
        <?php $co_pics = unserialize($v['upics']); ?>
        <div class="campus_list" <?php if($k > 0): ?> style="display:none" <?php endif; ?>>
        	<ul>
            <?php foreach ($co_pics as $va): ?>
               <li>
                    <a class="fancybox" href="<?php echo base_url();?>/uploads/unipics/<?php echo $va['pic'] ?>" data-fancybox-group="gallery" title="合一大殿">
                        <img src="<?php echo base_url();?>/uploads/unipics/<?php echo get_thumb($va['pic'],'_200_200');?>" data-width="260" data-height="240">
                        <p class="num">01</p>
                        <p class="campus_oneness">
                          <span>
                           <img src="<?php echo base_url();?>/resource/images/campus_oneness.png" data-width="196" data-height="66">
                          </span>
                         </p>
                        <p class="more"> <  more  > </p>
                        <p class="mask"></p>
                    </a>
                </li>                 
                <?php endforeach ?>	    
            </ul>
        </div>
        <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
</section>
<!--Into the campus end-->

<script type="text/javascript" src="<?php echo base_url();?>/resource/fancybox/jquery.fancybox.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url();?>/resource/js/jquery.mousewheel-3.0.6.pack.js"></script>

<script type="text/javascript">

$(function(){
    var pad = 15;
    pad = pcFlag==true ? 15 : 6;
    
    $('.fancybox').fancybox({
        
        padding:pad,
        //transitionIn:'elastic',
        //transitionOut:'elastic',
        
        openEffect  : 'none',
        closeEffect : 'none',
        
        prevEffect : 'none', //切换方式
        nextEffect : 'none',
        
        helpers : {
            title : {
                type : 'outside'
            },
        },
        afterLoad : function() {
                    this.title =(this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
        }   
    });
})

</script>
<script type="text/javascript">
$(function(){
	$(".campus_list").each(function(index, element) {
		$(this).find("li").each(function(i, element) {
			var _num = i > 8 ? ("000"+(i+1)).substr(3):("000"+(i+1)).substr(2);
			$(this).find(".num").html(_num)
        });
    });
	
	var campusNav = $(".tab_nav a").click(function(){
		var idx = campusNav.index(this);
		$(this).addClass("active").siblings().removeClass("active");
		$(".campus_list").eq(idx).fadeIn(800).siblings(".campus_list").fadeOut(30);
	})
	
    //2015.3.04xiaobin add
    $(".tab_nav a").eq(0).trigger('click');
	
	
})
</script>