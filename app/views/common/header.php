<!DOCTYPE html>
<html lang="zh">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<title>合一大学中文官方网站</title>
<!--<link href='http://fonts.useso.com/css?family=Open+Sans:300,400,600&subset=latin,latin-ext' rel='stylesheet'>
-->
<link rel="icon" type="image/png" href="/oneness/oneness-favicon.png">
<link href="<?php echo base_url();?>/resource/css/base.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>/resource/css/common.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url();?>/resource/js/jquery-1.10.1.min.js"></script> 

</head>
<body>
<header class="header">

  <div class="top_box">
    <a class="mobile-nav-list" href="javascript:"></a>
    <nav class="nav"> 
      <!-- <a>Home</a> -->
      <ul>
        <li><a <?php if(strstr($uri,'about')){?>class="cur"<?php } ?> href="<?php echo site_url($lang.'/about'); ?>"><?php echo $lan['in_nav_about'] ?></a></li>
        <li><a <?php if(strstr($uri,'wisdom')){?>class="cur"<?php } ?> href="<?php echo site_url($lang.'/wisdom'); ?>"><?php echo $lan['in_nav_dom'] ?></a></li>
        <li><a <?php if(strstr($uri,'news')){?>class="cur"<?php } ?> href="<?php echo site_url($lang.'/news'); ?>"><?php echo $lan['in_nav_news'] ?></a></li>
        <li><a <?php if(strstr($uri,'course')){?>class="cur"<?php } ?> href="<?php echo site_url($lang.'/course'); ?>"><?php echo $lan['in_nav_cours'] ?></a></li>
        <li><a <?php if(strstr($uri,'interact')){?>class="cur"<?php } ?> href="<?php echo site_url($lang.'/interact'); ?>"><?php echo $lan['in_nav_inter'] ?></a></li>
        <li><a <?php if(strstr($uri,'contact')){?>class="cur"<?php } ?> href="<?php echo site_url($lang.'/contact'); ?>"><?php echo $lan['in_nav_con'] ?></a> </li>
        
        <?php if($lang == 'cn'): ?>
          <li><a href="<?php echo strlen($uri) > 2 ? site_url(str_replace('cn', 'en', $uri)) : base_url().'index.php/'.'en'; ?>" class="lang_switch"> <span>| </span>
           <?php echo $lan['in_lang_en'] ?>
        <?php else: ?>
          <li><a href="<?php echo strlen($uri) > 2 ? site_url(str_replace('en', 'cn', $uri)) : base_url().'index.php/'.'cn'; ?>" class="lang_switch"> <span>| </span>
           <?php echo $lan['in_lang_cn'] ?>
        <?php endif; ?>
           </a></li>
      </ul>
    </nav>
    <div class="logo_box"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url();?>/resource/images/logo-color.png"></a></div>
  </div>
  
  <div class="header_bg"></div>
</header>