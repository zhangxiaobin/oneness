<!--bottom start-->
<footer class="footer">
  <div class="main_box">
    <div class="return_top"> <a href="#top" title="返回顶部"><img src="<?php echo base_url();?>/resource/images/btn_top.png" data-width="272" data-height="130"></a></div>
    <div class="footer_con"> 
      <!--bottom subnav-->
      <div class="bot_nav">
        <dl>
          <dt><?php echo $lan['in_foot_uni_name'] ?></dt>
          <dd>
            <ul>
            <li><a href="<?php echo site_url($lang.'/about'); ?>"><?php echo $lan['in_nav_about'] ?></a></li>
            <li><a href="<?php echo site_url($lang.'/wisdom'); ?>"><?php echo $lan['in_nav_dom'] ?></a></li>
            <li><a href="<?php echo site_url($lang.'/news'); ?>"><?php echo $lan['in_nav_news'] ?></a></li>
            <li><a href="<?php echo site_url($lang.'/course'); ?>"><?php echo $lan['in_nav_cours'] ?></a></li>
            <li><a href="<?php echo site_url($lang.'/interact'); ?>"><?php echo $lan['in_nav_inter'] ?></a></li>
            <li><a href="<?php echo site_url($lang.'/contact'); ?>"><?php echo $lan['in_nav_con'] ?></a> </li>
            </ul>
          </dd>
        </dl>
        <dl>
          <dt><?php echo $lan['in_foot_f_link'] ?></dt>
          <dd>
            <ul>
              <li><a href="javascript:"><?php echo $lan['in_foot_en_web'] ?></a></li>
              <li><a href="javascript:"><?php echo $lan['in_foot_sina'] ?></a></li>
              <li><a href="javascript:"><?php echo $lan['in_foort_wechat'] ?></a></li>
            </ul>
          </dd>
        </dl>
        <dl>
          <dt><?php echo $lan['in_foot_course'] ?></dt>
          <dd>
            <ul>
              <li><a href="javascript:">觉醒之旅</a></li>
              <li><a href="javascript:">合一合作伙伴</a></li>
              <li><a href="javascript:">黄金纪元合作伙伴</a></li>
              <li><a href="javascript:">进入更高智慧的旅程</a></li>
            </ul>
          </dd>
        </dl>
      </div>
      
      <!--sns-->
      <div class="sns_box fr"> 
        <img src="<?php echo base_url();?>/resource/images/bot_logo.jpg" data-width="270" data-height="90">
        <ul class="sns_list">
          <li class="sns_icon01"><a href="javascript:"></a></li>
          <li class="sns_icon02"><a href="javascript:"></a></li>
          <li class="sns_icon03"><a href="javascript:"></a></li>
          <li class="sns_icon04"><a href="javascript:"></a></li>
          <li class="sns_icon05"><a href="javascript:"></a></li>
        </ul>
      </div>
      
    </div>
    
    <div class="copyright"><?php echo $lan['in_foot_copyright'] ?></div>
  </div>
</footer>
<!--bottom end--> 
<script type="text/javascript" src="<?php echo base_url();?>/resource/js/common.js"></script>


</body>
</html>