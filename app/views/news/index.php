<link href="<?php echo base_url();?>/resource/css/news.css" rel="stylesheet" type="text/css">
<!--banner start-->
<section class="page_banner banner_news" id="cont_1">
        <div class="banner_text">
        <img src="<?php echo base_url();?>/resource/images/banner_news_text.png" data-width="365" data-height="450">
    </div>
</section>
<!--banner end-->

<!--Oneness announcement start -->
<section id="cont_2">
	<div class="main_box2">
        <h3 class="title-font title-h2"><?php echo $lan['news_tit_notice'] ?><span class="title_line"></span></h3>
        <div class="news_list news_notice">
        	<ul>
            	<?php if(! empty($all_notice) && is_array($all_notice)): ?>
                    <?php foreach ($all_notice as $k => $v): ?>
                     <?php if($k == 0): ?>
                     <li class="top">
                         <a href="<?php echo site_url($lang.'/news/notice/'.$v['id']) ?>">
                             <span class="tab">置顶</span>
                             <h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4>
                             <!-- <span class="author">By 合一大学官方中文网站</span> -->
                             <p><?php echo msubstr($v['desc'], 0,50,'utf-8','>>') ?></p>
                         </a>
                     </li>
                     <?php endif; ?>
                    <?php if($k ==1): ?>
                        <li class="hot">
                             <a href="<?php echo site_url($lang.'/news/notice/'.$v['id']) ?>">
                               <span class="tab">热门</span>
                               <h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4>
                             </a>
                        </li>
                    <?php endif; ?>
                    <?php if($k ==2): ?>
                        <li class="hot">
                            <a href="<?php echo site_url($lang.'/news/notice/'.$v['id']) ?>">
                               <span class="tab">最新</span>
                               <h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($k > 2): ?>
                    <li><a href="<?php echo site_url($lang.'/news/notice/'.$v['id']) ?>"><h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4></a></li>
                    <?php endif ?>
                    <?php endforeach ?>
            <?php endif; ?>
            </ul>
        </div>
        <div class="page notice_page">
          <?php echo $npl; ?>
        </div>
    </div>
</section>
<!--Oneness announcement end -->

<!--Oneness news start -->
<section id="cont_2">
	<div class="main_box2">
        <h3 class="title-font title-h2"><?php echo $lan['news_tit_news'] ?><span class="title_line"></span></h3>
        <div class="news_list new_news">
        	<ul>
                <?php if(! empty($all_news) && is_array($all_news)): ?>
                    <?php foreach ($all_news as $k => $v): ?>
                     <?php if($k == 0): ?>
                     <li class="top">
                         <a href="<?php echo site_url($lang.'/news/newinfo/'.$v['id']) ?>">
                             <span class="tab">置顶</span>
                             <h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4>
                             <!-- <span class="author">By 合一大学官方中文网站</span> -->
                             <p><?php echo msubstr($v['desc'], 0,50,'utf-8','>>') ?></p>
                         </a>
                     </li>
                     <?php endif; ?>
                    <?php if($k ==1): ?>
                        <li class="hot">
                             <a href="<?php echo site_url($lang.'/news/newinfo/'.$v['id']) ?>">
                               <span class="tab">热门</span>
                               <h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4>
                             </a>
                        </li>
                    <?php endif; ?>
                    <?php if($k ==2): ?>
                        <li class="hot">
                            <a href="<?php echo site_url($lang.'/news/newinfo/'.$v['id']) ?>">
                               <span class="tab">最新</span>
                               <h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if($k > 2): ?>
                    <li><a href="<?php echo site_url($lang.'/news/newinfo/'.$v['id']) ?>"><h4><?php echo $v['title'] ?><span class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></span></h4></a></li>
                    <?php endif ?>
                    <?php endforeach ?>
            <?php endif; ?>
            </ul>
        </div>
        <div class="page new_page">
           <?php echo $nepl; ?>
        </div>
    </div>
</section>
<!--Oneness news end -->
<script>
   $(function (){
    $(".notice_page").on('click',".page_con a",function(){
        num = $(this).attr('data-num');
        if(typeof(num) === undefined)
        {
            return false;
        }
        lang = "<?php echo $lang; ?>";
        $.ajax({
            url:"<?php echo site_url('news/load_page_notice'); ?>",
            type: 'POST',
            data: {num:num,lang:lang},
            dataType:'json', 
            success: function(data)
            {
                 if(data)
                 {
                    $(".notice_page").html('');
                    $(".notice_page").append(data.pl);
                    $('.news_notice ul li').remove();
                    // $('.new_news ul li').remove();
                    $('.news_notice ul').append(data.news);
                 }
                 else
                 {
                    alert('没有了');
                 }
            }
        });
    })

    //==================
    $(".new_page").on('click',".page_con a",function(){
        num = $(this).attr('data-num');
        if(typeof(num) === undefined)
        {
            return false;
        }
        lang = "<?php echo $lang; ?>";
        $.ajax({
            url:"<?php echo site_url('news/load_page_notice'); ?>",
            type: 'POST',
            data: {num:num,lang:lang,flag : 1},
            dataType:'json', 
            success: function(data)
            {
                 if(data)
                 {
                    $(".new_page").html('');
                    $(".new_page").append(data.pl);
                    $('.new_news ul li').remove();
                    $('.new_news ul').append(data.news);
                 }
                 else
                 {
                    alert('没有了');
                 }
            }
        });
    })
   })
</script>