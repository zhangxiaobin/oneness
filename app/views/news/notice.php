 <link href="<?php echo base_url();?>/resource/css/page.css" rel="stylesheet" type="text/css">
<?php $url_flag = $this->uri->segment(3); ?>
<!--内页三级内容框架 start-->
<section class="infor_box" id="cont_1">
  <div class="top_line"></div>
  <!--面包屑 start-->
  <div class="bread-crumbs">
    <p><a href="<?php echo base_url(); ?>">首页</a> &nbsp;>&nbsp; <a href="<?php echo site_url($lang.'/news'); ?>">校园快讯</a> &nbsp;>&nbsp; <span class="cur">正文</span> </p>
  </div>
  <!--面包屑 end-->
  <div class="page-main"> 
    <!--左侧主体内容 start-->
    
    <?php if($art['type'] !== 'vedio'): ?>
    <div class="left-main">
      <div class="left_box">
        <div class="infor_title">
          <h2><?php echo $art['title']; ?></h2>
          <p>
            <span><?php echo $lan['wis_source'] ?>: <?php echo $lan['wis_official'] ?></span> 
            <span><?php echo $lan['wis_release_time'] ?>:  <?php echo date('Y-m-d',$art['updatetime']); ?></span>
          </p>
        </div>
        <div class="news_con">
          <?php echo $art['content']?>
          <!-- <p class="text-r">合一大学官方中文网站</p> -->
        </div>
<!--         <div class="accessory">
          <p>附件1:<a href="javascript:"> 活动方案.doc </a></p>
          <p>附件2:<a href="javascript:"> 微课评选指标.doc </a></p>
        </div> -->
        <div class="page_switch">
        <?php if($art["pre"] !=0): ?>
          <p><a href="<?php echo site_url($lang.'/news/'.$url_flag.'/'.$art["pre"]) ?>"><span><?php echo $lan['wis_previous_art'] ?></span> <?php echo $art['pre_title'] ?></a></p>
        <?php else: ?>
          <p><a href="javascript:;"><span></a></p>
        <?php endif; ?>

          <?php if($art["nex"] !=0): ?>
            <p class="page-next"><a href="<?php echo site_url($lang.'/news/'.$url_flag.'/'.$art["nex"]) ?>"><span><?php echo $lan['wis_next_art'] ?></span> <?php echo $art['next_title'] ?></a></p>
          <?php else: ?>
            <p class="page-next"><a href="javascript:;"><span></span></a></p>
          <?php endif; ?>

        </div>
      </div>
    </div>
    <?php else: ?>
      <!--左侧主体内容 start-->
      <div class="left-main">
        <div class="left_box">
          <div class="infor_title">
            <h2>视频: <?php echo $art['title']; ?></h2>
            <p><span><?php echo $lan['wis_source'] ?>: <?php echo $lan['wis_official'] ?></span> <span><?php echo $lan['wis_release_time'] ?>:  <?php echo date('Y-m-d',$art['updatetime']); ?></span></p>
          </div>
          <div class="video_box">
            <?php echo htmlspecialchars_decode(unserialize($art['vedio'])); ?>
          </div>
          
          <div class="page_switch">
          <?php if($art["pre"] !=0): ?>
            <p><a href="<?php echo site_url($lang.'/news/'.$url_flag.'/'.$art["pre"]) ?>"><span><?php echo $lan['wis_previous_art'] ?></span> <?php echo $art['pre_title'] ?></a></p>
          <?php else: ?>
            <p><a href="javascript:;"><span></a></p>
          <?php endif; ?>

            <?php if($art["nex"] !=0): ?>
              <p class="page-next"><a href="<?php echo site_url($lang.'/news/'.$url_flag.'/'.$art["nex"]) ?>"><span><?php echo $lan['wis_next_art'] ?></span> <?php echo $art['next_title'] ?></a></p>
            <?php else: ?>
              <p class="page-next"><a href="javascript:;"><span></span></a></p>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <!--左侧主体内容 start--> 
    <?php endif; ?>

    <!--左侧主体内容 start--> 
    
    <!--右侧文章列表 start-->
    <?php if(! empty($other_art) && is_array($other_art)): ?>
    <div class="right-main">
      <h2 class="list-title">文章列表</h2>
      <ul class="listview">
       <?php foreach ($other_art as $v): ?>
            <li class="has-thum"> 
            <a href="<?php echo site_url($lang.'/news/'.$url_flag.'/'.$v["id"]) ?>">
          <div class="list-text">
            <p><?php echo $v['title'] ?></p>
            <p class="date"><?php echo $lan['wis_release_time'] ?>:  <?php echo date('Y-m-d',$art['updatetime']); ?></p>
          </div>
          <?php if(! empty($v['image'])): ?>
          <div class="list-thum"> 
            <img src="<?php echo base_url();?>/uploads/wisdom/<?php echo $v['image'] ?>">
            <?php if($v['type'] == 'vedio'): ?>
             <span class="video-icon"></span>
           <?php endif; ?>
          </div>
        <?php endif; ?>
          </a>
         </li>     
       <?php endforeach ?>
      </ul>
    </div>
  <?php endif; ?>
    <!--右侧文章列表 start--> 
    
  </div>
</section>