<link rel="stylesheet" href="<?php echo base_url();?>/resource/css/idangerous.swiper.css">
<link href="<?php echo base_url();?>/resource/css/index.css" rel="stylesheet" type="text/css">
<style>
  .course_list li {
      padding-bottom: 50px;
  }
  .course_list {
  padding: 60px 0 20px 0;
  }
</style>
<!--首屏切换 start-->
<section class="kv_box" id="cont_1">
  <ul>
    <li class="kv-img kv1 active">
      <!-- <p class="overlay"> <img src="<?php echo base_url();?>/resource/images/kv01_title.png"/> </p> -->
    </li>
    <li class="kv-img kv2 hidden">
      <!-- <p class="overlay"> <img src="<?php echo base_url();?>/resource/images/kv02_title.png"/> </p> -->
    </li>
    <li class="kv-img kv3 hidden">
      <!-- <p class="overlay"> <img src="<?php echo base_url();?>/resource/images/kv03_title.png"/> </p> -->
    </li>
    <li class="kv-img kv4 hidden">
      <!-- <p class="overlay"> <img src="<?php echo base_url();?>/resource/images/kv04_title.png"/> </p> -->
    </li>
  </ul>
</section>
<!--首屏切换 start--> 

<!--about us start-->
<section class="about_box" id="cont_2">
  <div class="main_box"> 
    <!--校园简介 start-->
    <div class="about_text">
      <h3 class="title-font title-h3"><?php echo $lan['home_sch_desc'] ?></h3>
      <div class="content">
      	<div class="m_con">	
            <p class="text-cn"> 
               <?php echo $compus[0]; ?>
            </p>
            <p class="text-cn">
               <?php echo empty($compus[1]) ? '' : $compus[1]; ?>
            </p>
        </div>
      </div>
      <a class="mobile-more" href="javascript:"><?php echo $lan['home_abo_more'] ?></a>
      
      <div class="about_logo">
         <span class="l"><i><?php echo $lan['home_sch_study'] ?></i></span>
         <b class="about_logo_pic"><img src="<?php echo base_url();?>/resource/images/logo.png"></b> 
         <span class="r"><i><?php echo $lan['home_sch_grow'] ?></i></span>
      </div>
      <p class="em"><?php echo $lan['home_sch_yulu'] ?></p>
    </div>
    <!--校园简介 end--> 
    
    <!--校园风光展示 start-->
    <div class="about_pic">
      <h3 class="title-font title-h3"><?php echo $lan['home_sch_walk'] ?></h3>
      <div class="swiper-container swiper_box1">
        <div class="swiper-wrapper">
          <div class="swiper-slide">
            <ul class="about_list">

              <?php foreach ($com_pic as $v): ?>
                <li>
                  <a href="<?php echo site_url($lang.'/about/index/?uni='.$v['uni']) ?>">
                    <img src="<?php echo base_url();?>uploads/unipics/<?php echo get_thumb($v['pic'],'_300_200');?>" alt='<?php echo $v['title'] ?>'>
                  </a>
                </li>
              <?php endforeach ?>
            
            </ul>
          </div>
        </div>
      </div>
      <div class="img_switch"> <a class="prev prev1" href="javascript:">&lt; prev</a><a class="next next1" href="javascript:"><span>next &gt;</span></a> </div>
    </div>
    <!--校园风光展示 end--> 
  </div>
</section>
<!--about us end--> 

<!--oneness wisdom start-->
<section class="section_bg" id="cont_3">
  <div class="main_box">
    <h3 class="title-font title-h2"><?php echo $lan['home_sch_wisdom'] ?><span class="title_line"></span></h3>
    <div class="wisdom_box">
      <div class="swiper-container swiper-parent">
        <ul class="swiper-wrapper wisdom_list">
         <?php foreach ($wisdom as $v): ?>
          <li class="swiper-slide">
          <!-- 没有加链接 -->
            <a href="<?php echo site_url($lang.'/wisdom/article/'.$v["id"]) ?>">
            <p class="event_pic"><img src="<?php echo base_url();?>/uploads/wisdom/<?php echo $v['image'] ?>"><span class="active_bg"></span><?php if($v['type'] !== 'text'): ?><span class="icon_paly"></span><?php endif; ?></p>
            <p class="event_text text-cn"><?php echo $v['title'] ?></p>
            </a>
          </li>  
          <?php endforeach ?> 



        </ul>
        <div class="pagination pagination-parent"></div>
      </div>
      
      <!--更多按钮-->
      <div class="btn_box"><a href="<?php echo site_url($lang.'/wisdom') ?>" class="more_btn"><?php echo $lan['home_more'] ?></a></div>
    </div>
  </div>
</section>
<!--oneness wisdom end--> 

<!--每周一句话 start-->
<section id="cont_4">
  <div class="week_box">
    <div class="week_words">
      <ul>
<?php if(! empty($weeks)): ?>
  <?php foreach ($weeks as $k => $v): ?>
      <li <?php if($k ===0):?> class="cur" <?php else: ?> style="display:none" <?php endif; ?>>
          <h3><span><?php echo $lan['home_warm_week'] ?></span> — <span class="week"><?php echo $lang === 'cn' ? date('Y年m月d日',$v['addtime']) : date('Y-m-d',$v['addtime']); ?></span></h3>
          <p><?php echo $v['content']; ?></p>
      </li>
  <?php endforeach ?>

<?php endif; ?>
      </ul>
      <div class="week_btn"> 
           <a class="prev week_prev" href="javascript:"><span></span></a> 
           <a class="next week_next" href="javascript:"><span></span></a> 
      </div>
    </div>
  </div>
</section>
<!--每周一句话 end--> 

<!--news calendar start-->
<section class="section_bg2" id="cont_5">
  <div class="main_box">
    <div class="news_box fl">
      <h3 class="title-font title-h3"><?php echo $lan['in_nav_zixun'] ?></h3>
      <ul>
       <?php foreach ($notices as $v): ?>
         <li>
         <!-- 没有加链接 -->
          <a href="<?php echo site_url($lang.'/news/notice/'.$v['id']) ?>">
          <p class="news_text"><?php echo $v['title'] ?></p>
          <p class="date"><?php echo date('Y-m-d',$v['updatetime']) ?></p>
          </a>
        </li>
        <?php endforeach ?> 
      </ul>
      <a href="<?php echo site_url($lang.'/news') ?>" class=" more_btn"><?php echo $lan['home_more'] ?></a> </div>
    <div class="news_box fr">
      <h3 class="title-font title-h3"><?php echo $lan['home_an_huaxu'] ?></h3>
      <ul>
       <?php foreach ($news as $va): ?>
          <li>
          <!-- 没有加链接 -->
            <a href="<?php echo site_url($lang.'/news/newinfo/'.$va['id']) ?>">
            <p class="news_text"><?php echo $va['title'] ?></p>
            <p class="date"><?php echo date('Y-m-d',$va['updatetime']) ?></p>
          </a></li>
       <?php endforeach ?>
      </ul>
      <a href="<?php echo site_url($lang.'/news') ?>" class="more_btn"><?php echo $lan['home_more'] ?></a> </div>
  </div>
</section>
<!--news calendar end--> 

<!--We present to you there are more start-->
<section class="section_bg3" id="cont_6">
  <div class="main_box">
    <h3 class="title-font title-h2 title-white"><?php echo $lan['home_present_more'] ?><span class="title_line"></span></h3>
    <ul class="course_list">
    <?php if(! empty($courses)): ?>
      <?php foreach ($courses as $v): ?>
       <li>
        <div>
          <h4><?php echo $v['title'] ?></h4>
          <p>
            <span class="time">
               <?php echo $v['day'] ?>
               <?php echo is_numeric($v['day']) ? $lan['home_day'] : ''; ?> 
             </span>
          </p>
          <p class="course_description"></p>
          <a href="javascript:"><?php echo $lan['home_course_intro'] ?></a> </div>
      </li>
      <?php endforeach ?>

    <?php endif; ?>
 <!--      <li>
        <div>
          <h4>进入内在智慧的旅程</h4>
          <p><span class="time">30天</span></p>
          <p class="course_description"></p>
          <a href="javascript:">课程介绍</a> </div>
      </li> 
      <li>
        <div>
          <h4>进入自由的旅程<span>合一合作伙伴</span></h4>
          <p><span class="time">19天</span></p>
          <p class="course_description"></p>
          <a href="javascript:">课程介绍</a> </div>
      </li>
      <li>
        <div>
          <h4>进入生活的旅程<span>黄金纪元合作伙伴</span></h4>
          <p><span class="time">31天</span></p>
          <p class="course_description"> <span>第一阶段: 17天</span> <span>第二、三阶段: 各7天</span> </p>
          <a href="javascript:">课程介绍</a> </div>
      </li>-->

    </ul>
    <div class="course_text">
      <p><?php echo $lan['home_course_desc']; ?></p>
      （<?php echo $lan['home_qua_desc'] ?>）</div>
  </div>
</section>
<!--We present to you there are more end--> 

<!--The curriculum start-->
<section class="curriculum_bg" id="cont_7">
  <div class="triangle"><img src="<?php echo base_url();?>/resource/images/triangle.jpg"></div>
  <div class="main_box">
    <h3 class="title-font title-h2"><?php echo $lan['home_schedule'] ?><span class="title_line"></span></h3>
    <div class="curriculum_box">
      <div class="curriculum_nav">
        <ul>
          <li class="current"><a href="javascript:">
            <p>2015.01--2015.03</p>
            <span>（<?php echo $lan['home_first_qua']?>）</span><span class="arrow-r"></span></a></li>
          <li><a href="javascript:">
            <p>2015.04--2015.06</p>
            <span>（<?php echo $lan['home_second_qua']?>）</span><span class="arrow-r"></span></a></li>
          <li><a href="javascript:">
            <p>2015.07--2015.09</p>
            <span>（<?php echo $lan['home_third_qua']?>）</span><span class="arrow-r"></span></a></li>
          <li><a href="javascript:">
            <p>2015.10--2015.12</p>
            <span>（<?php echo $lan['home_fouth_qua']?>）</span><span class="arrow-r"></span></a></li>
        </ul>
        <p class="curriculum_tip"><?php echo $lan['home_qua_desc'] ?></p>
      </div>
      
<?php 
   $tmpschs = unserialize($schedules['con']); 
   $schs = array_chunk($tmpschs, 3);
   // p($schs);
   if(! empty($schs) && is_array($schs)):
   foreach ($schs as $k => $v):
     $num1 = $k == 3 ? '.'.($k*3+1) : '.0'.($k*3+1); 
     $num2 = $k == 3 ? '.'.($k*3+2) : '.0'.($k*3+2); 
     $num3 = $k == 3 ? '.'.($k*3+3) : '.0'.($k*3+3); 
     ?>
      <div class="curriculum_con" <?php if($k != 0): ?>style='display:none;' <?php endif; ?>>
        <dl>
          <dt><?php echo $schedules['year'].$num1 ?></dt>
          <dd>
            <ul>
            <?php  if(! empty($v[0]['con'])):?>
              <?php foreach ($v[0]['time'] as $k1 => $va): ?>
                <?php if(! empty($v[0]['con'][$k1])): ?>
                <li>
                  <h4><?php echo $v[0]['con'][$k1]; ?></h4>
                  <span class="time"><?php echo $va; ?></span>
                </li>  
                <?php endif; ?>        
              <?php endforeach; ?>
            <?php endif; ?>
            </ul>
          </dd>
        </dl>
        
        <dl>
          <dt><?php echo $schedules['year'].$num2 ?></dt>
          <dd>
            <ul>
            <?php  if(! empty($v[1]['con'])):?>
              <?php foreach ($v[1]['time'] as $k2 => $va): ?>
                <?php if(! empty($v[1]['con'][$k2])): ?>
                <li>
                  <h4><?php echo $v[1]['con'][$k2]; ?></h4>
                  <span class="time"><?php echo $va; ?></span>
                </li>  
                <?php endif; ?>        
              <?php endforeach; ?>
            <?php endif; ?>
            </ul>
          </dd>
        </dl>
        
        <dl>
          <dt><?php echo $schedules['year'].$num3 ?></dt>
          <dd>
            <ul>
              <?php  if(! empty($v[2]['con'])):?>
                <?php foreach ($v[2]['time'] as $k3 => $va): ?>

                  <?php if(! empty($v[2]['con'][$k3])): ?>
                    <li>
                      <h4><?php echo $v[2]['con'][$k3]; ?></h4>
                      <span class="time"><?php echo $va; ?></span>
                    </li>  
                  <?php endif; ?>        
                <?php endforeach; ?>
              <?php endif; ?>
            </ul>
          </dd>
        </dl>
      </div>
  <?php 
    endforeach;
    endif; 
  ?> 

    </div>
  </div>
</section>
<!--The curriculum end--> 

<!--WHAT THE WORLD IS SAYING? start-->
<?php if(! empty($says)): ?>
<section class="section_bg2" id="cont_8">
  <div class="main_box">
    <div class="world_box">
      <a class="world_btn world_prev" href="javascript:"><span></span></a>
      <a class="world_btn world_next" href="javascript:"><span></span></a>
      <div class="swiper-container swiper_box2">
        <div class="swiper-wrapper">
         
<?php foreach ($says as $v): ?>
            <div class="swiper-slide">
            <div class="world_con">
              <div class="world_pic"><img src="<?php echo base_url();?>/uploads/share/<?php echo $v['image'] ?>"></div>
              <div class="world_text">
                <p><?php echo $v['content'] ?></p>
                <p><span class="name"><?php echo $v['name'] ?></span> <span class="description"><?php echo $v['desc'] ?></span></p>
              </div>
            </div>
          </div>
<?php endforeach ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
<!--WHAT THE WORLD IS SAYING? end--> 

<!--meet oneness start-->
<section class="section_bg4" id="cont_9">
  <div class="main_box">
    <h3 class="title-font title-h2 meet_title"><?php echo $lan['home_meet_oneness'] ?><span>— <?php echo $lan['home_meet_yourself'] ?> —</span></h3>
    <!--在线微课堂 start-->
    <div class="meet_box">
      <h3><?php echo $lan['home_after_sch'] ?><span>— <?php echo $lan['home_grow_up'] ?></span></h3>
      <ul class="meet_list">
      <?php if(! empty($aftsch)): ?>
        <?php foreach ($aftsch as $k => $v): ?>
          <li <?php if($k ===0):?> class="cur" <?php else: ?> style="display:none" <?php endif; ?>>
              <p class="meet_text"><a href="<?php echo site_url($lang.'/interact/afterinfo/'.$v['id']) ?>"><?php echo $v['title'] ?></a> </p>
              <p><span><?php echo $lan['home_present'] ?>: <?php echo $v['name'] ?></span><span class="meet_line">|</span><span><?php echo $lan['home_publish'] ?>: <?php echo date('Y-m-d',$v['updatetime']) ?></span></p>
          </li>
        <?php endforeach ?>

      <?php endif; ?>


      </ul>
      <div class="week_btn"> <a class="prev week_prev" href="javascript:"><span></span></a> <a class="next week_next" href="javascript:"><span></span></a> </div>
    </div>
    <!--在线微课堂 end--> 
    
    <!--学生分享 start-->
    <div class="student_share">
      <ul>
      <?php if(! empty($stushare)): ?>
        <?php foreach ($stushare as $k => $v): ?>
        <li>
            <a href="<?php echo site_url($lang.'/interact/newinfo/'.$v['id']) ?>">
              <img src="<?php echo base_url();?>/uploads/share/<?php echo $v['image'] ?>">
              <p class="tab"><?php echo $lan['home_stu_share'] ?><span></span></p>
              <p class="share_text"><?php echo $v['title'] ?></p>
              <p class="share_author"><span>●</span><?php echo $v['name'] ?>  /  <?php echo $lan['home_art_write'] ?></p>
            </a> 
        </li>
        <?php endforeach ?>
      <?php endif; ?>

      <?php if(! empty($inters)): ?>
        <?php foreach ($inters as $k => $v): ?>
        <li>
            <a href="<?php echo site_url($lang.'/interact/interinfo/'.$v['id']) ?>">
              <img src="<?php echo base_url();?>/uploads/share/<?php echo $v['image'] ?>">
              <p class="tab"><?php echo $lan['home_stu_inter'] ?><span></span></p>
              <p class="share_text"><?php echo $v['title'] ?></p>
              <p class="share_author"><span>●</span><?php echo $v['name'] ?>  /  <?php echo $lan['home_art_write'] ?></p>
            </a> 
        </li>
        <?php endforeach ?>
      <?php endif; ?>
      </ul>
    </div>
    <!--学生分享 end--> 
    <!--智慧解读 start-->
<!--     <div class="student_share">
      <ul>
      <?php //if(! empty($stushare)): ?>
        <?php //foreach ($stushare as $k => $v): ?>
        <li>
            <a href="<?php echo site_url($lang.'/interact/newinfo/'.$v['id']) ?>">
              <img src="<?php echo base_url();?>/uploads/share/<?php echo $v['image'] ?>">
              <p class="tab"><?php echo $lan['home_stu_share'] ?><span></span></p>
              <p class="share_text"><?php echo $v['title'] ?></p>
              <p class="share_author"><span>●</span><?php echo $v['name'] ?>  /  <?php echo $lan['home_art_write'] ?></p>
            </a> 
        </li>
        <?php //endforeach ?>
      <?php //endif; ?>
      </ul>
    </div> -->
    <!--智慧解读 end--> 
  </div>
</section>
<!--meet oneness end--> 

<!--Contact Center start-->
<?php if(! empty($contacters) && is_array($contacters)): ?>
<section id="cont_10">
  <div class="main_box">
    <h3 class="title-font title-h2"><?php echo $lan['in_nav_con'] ?><span class="title_line"></span></h3>
    <div class="contact_center">
    <?php foreach ($contacters as $k=>$v): ?>
        <dl <?php if($k > 2): ?>  class="noline"<?php endif; ?>>
          <dt>
              <h4><?php echo $v['name'] ?></h4>
                <p><?php echo $v['team'] ?><span class="line"></span></p>
            </dt>
            <?php $phones = explode(',', $v['phone'])?>
            <dd>
              <p class="phone">
              <?php foreach ($phones as $ke => $va): ?>
                <?php if($ke >1){break;} ?>
                <span><a href="tel:<?php echo $va ?>"><?php echo $va ?></a></span>
              <?php endforeach ?>
              </p>
                <p class="wechat"><?php echo $v['wechat'] ?></p>
                <p class="address"><?php echo $v['city'] ?></p>
            </dd>
        </dl>
    <?php endforeach ?>
        <dl <?php if($k > 2): ?>  class="noline"<?php endif; ?>>
        	<dt>
            	<h4><span class="groups_pic"><img src="<?php echo base_url();?>/resource/images/groups_pic.jpg" data-width="100" data-height="100"></span></h4>
            </dt>
            <dd>
            	<p class="pt4" style="background:none;padding-left:0"><?php echo $lan['home_contact_desc'] ?></p>
            </dd>
        </dl>
    </div>
  </div>
</section>
<?php endif; ?>
<!--Contact Center end--> 

<!--Digital change start-->
<section class="section_bg5" id="cont_11">
  <div class="main_box">
    <ul class="digital">
      <li>
        <p class="digital_icon"><img src="<?php echo base_url();?>resource/images/icon_countries.png"></p>
        <p class="num" data-target="126">0</p>
        <p class="list_title"><span><?php echo $lan['home_country'] ?></span></p>
      </li>
      <li>
        <p class="digital_icon"><img src="<?php echo base_url();?>resource/images/icon_students.png"></p>
        <p class="num" data-target="1500000">0</p>
        <p class="list_title"><span><?php echo $lan['home_student'] ?></span></p>
      </li>
      <li>
        <p class="digital_icon"><img src="<?php echo base_url();?>resource/images/icon_days.png"></p>
        <p class="num" data-target="28">0</p>
        <p class="list_title"><span><?php echo $lan['home_day'] ?></span></p>
      </li>
    </ul>
  </div>
</section>
<!--Digital change end--> 
<!--India impression start-->
<section class="section_bg6">
  <div class="main_box">
      <ul class="impression_list">
          <li><img src="<?php echo base_url();?>/resource/images/india_impression.jpg" data-width="285" data-height="188"></li>

<?php if(! empty($impress)): ?>
  <?php foreach ($impress as $v): ?>
       <li><a href="javascript:;"><img src="<?php echo base_url();?>/uploads/pics/<?php echo get_thumb($v['pic_name'],'_300_200');?>" data-width="285" data-height="188"><span class="hover_bg"></span></a></li>
  <?php endforeach ?>       
<?php endif; ?>

            <li><a href="<?php echo site_url($lang.'/home/impress'); ?>"><img src="<?php echo base_url();?>/resource/images/india_impression-08.jpg" data-width="285" data-height="188"><span class="hover_bg"></span></a></li>
        </ul>
    </div>
</section>
<!--India impression end-->
<!--Contact us start-->
<section class="contact_bg" id="cont_12">
  <div class="main_box">
    <div class="contact_con">
      <h3 class="title-font"><?php echo $lan['home_contact_us'] ?></h3>
      <p><?php echo $lan['home_our_address'] ?></p>
      <p><?php echo $lan['home_address_c'] ?></p>
      <p>E-mail: <a class="email" href="mailto:info@onenessuniversity.org">info@onenessuniversity.org</a></p>
      <p class="em" style="text-align:left;">
        <?php echo $lan['home_apply_info1'] ?>
         <br/>
       <?php echo $lan['home_apply_info2'] ?>
        </p>
      <p class="input_box">
        <input name="" type="text" placeholder="<?php echo $lan['home_email'] ?>"class="email_input" />
        <input name="" type="button" value="<?php echo $lan['home_submit'] ?>" class="submit_btn" />
      </p>
    </div>
  </div>
</section>
<!--Contact us end--> 
<script type="text/javascript" src="<?php echo base_url();?>/resource/js/idangerous.swiper-2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/resource/js/index.js"></script>