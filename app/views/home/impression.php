<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/resource/fancybox/jquery.fancybox.css" media="screen" />
<link href="<?php echo base_url();?>/resource/css/about.css" rel="stylesheet" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo base_url();?>/resource/fancybox/jquery.fancybox.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url();?>/resource/js/jquery.mousewheel-3.0.6.pack.js"></script>

<script type="text/javascript">

$(function(){
	var pad = 15;
	pad = pcFlag==true ? 15 : 6;
	
	$('.fancybox').fancybox({
		
		padding:pad,
		//transitionIn:'elastic',
        //transitionOut:'elastic',
		
		openEffect  : 'none',
		closeEffect : 'none',
		
		prevEffect : 'none', //切换方式
		nextEffect : 'none',
		
		helpers : {
			title : {
				type : 'outside'
			},
		},
		afterLoad : function() {
					this.title =(this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
		}	
	});
})

</script>
<!--banner start-->
<section class="page_banner banner_about" id="cont_1">
	<div class="banner_text">
        <img src="<?php echo base_url();?>/resource/images/banner_about_text.png" data-width="365" data-height="450">
    </div>
</section>
<!--banner end-->

<!--Into the campus start-->
<section id="cont_2" class="campus_box2">
    <div class="main_box">
    	<h3 class="title-font title-h2">印度印象<span class="title_line"></span></h3>
        <!--1-->
        <div class="campus_list campus_list2">
        	<ul>
            <?php if(! empty($impress)): ?>
              <?php foreach ($impress as $v): ?>
                    <li>
                      <a class="fancybox" href="<?php echo base_url();?>/uploads/pics/<?php echo $v['pic_name'] ?>" data-fancybox-group="gallery" title="合一大殿">
                          <!-- <img src="<?php //echo base_url();?>/uploads/pics/<?php //echo $v['pic_name'] ?>" data-width="260" data-height="240"> -->
                          <img src="<?php echo base_url();?>/uploads/pics/<?php echo get_thumb($v['pic_name'],'_300_200');?>" data-width="260" data-height="240">
                          <p class="num">01</p>
                          <p class="campus_oneness">
                            <span>
                               <img src="<?php echo base_url();?>/resource/images/campus_oneness.png" data-width="196" data-height="66">
                            </span>
                          </p>
                          <p class="more"> <  more  > </p>
                          <p class="mask"></p>
                      </a>
                      </li>
              <?php endforeach ?>       
            <?php endif; ?>
            </ul>
        </div>
        
    </div>
</section>
<!--Into the campus end-->
<!--bottom end--> 
<script type="text/javascript">
$(function(){
	$(".campus_list").each(function(index, element) {
		$(this).find("li").each(function(i, element) {
			var _num = i > 8 ? ("000"+(i+1)).substr(3):("000"+(i+1)).substr(2);
			$(this).find(".num").html(_num)
        });
    });
	
})
</script>