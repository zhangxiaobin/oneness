<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>图片管理</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <link href="./css/seller_center2.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/myconfirm.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <style>
     table.table1 tr th{
     	text-align: center;
     }
     .upload-con {
         position: absolute;
         right: 92px;
         top: 41px;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="javascript:void(0);" class='action'>印度图片</a></li>
		  <li style="float:right;margin-right:85px;">
		     <a style="display:inline-block;padding:4px 12px;color:#fff;line-height:20px;" id="open_uploader" href="JavaScript:void(0);" class="ncsc-btn ncsc-btn-acidblue"><i class="icon-cloud-upload"></i>上传图片</a>
		  </li>
		</ul> 
		<!-- 上传图片 -->
		<div class="upload-con" id="uploader" style="display: none;">
		  <form method="post" action="" id="fileupload" enctype="multipart/form-data">
		    <div class="upload-con-div">选择文件：
		      <div class="ncsc-upload-btn"> <a href="javascript:void(0);">
		        <span>
		          <input type="file" hidefocus="true" size="1" class="input-file" name="file" multiple="multiple"/>
		        </span>
		        <p><i class="icon-upload-alt"></i>上传图片</p>
		        </a> 
		      </div>
		    </div>
		    <div nctype="file_msg"></div>
		    <div class="upload-pmgressbar" nctype="file_loading"></div>
		    <div class="upload-txt"><span>支持Jpg、Gif、Png格式，大小不超过2048KB的图片上传；浏览文件时可以按住ctrl或shift键多选。</span> </div>
		  </form>
		</div>
	   </div>
	  <div>
	  	<a href="JavaScript:void(0);" class="ncsc-btn-mini" onClick="switchAll()"><i class="icon-check-empty"></i>全选</a>
	  	<a href="JavaScript:void(0);" class="ncsc-btn-mini" onClick="submit_form()"><i class="icon-trash"></i>删除</a>
	  </div>
	   <div class="ncsc-picture-list">
	     <ul>
	     <?php if(! empty($pics)): ?>
	       <?php foreach($pics as $k =>$v){?>
	       <li>
	         <dl>
	           <dt>
	             <label for="ace<?php echo $k; ?>">
	             <div class="picture">
	                <a> 
	                   <img id="img" src="<?php echo $v['pic_cover'];?>">
	                </a>
	             </div>
	             </label>
	             <input id="ace<?php echo $k; ?>" value="<?php echo $v['id']; ?>" type="checkbox" class="checkbox" >
	           <dd class="date">
	             <p>上传时间: <?php echo date("Y-m-d",$v['upload_time'])?></p>
	             <p>图片规格: <?php echo $v['pic_spec']?></p>
	           </dd>
	         </dl>
	       </li>
	       <?php }?>
	   <?php else: ?>
	   	<li>没有图片:(</li>
	   <?php endif; ?>
	     </ul>
	   </div>


	</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>../resource/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url();?>../resource/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url();?>../resource/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script>
	$(function(){
		$('#open_uploader').unbind('click');
		$('#open_uploader').click(function(){
		    if($('#uploader').css('display') == 'none'){
		        $('#uploader').show();
		        $(this).find('.hide').attr('class','show');
		    }else{
		        $('#uploader').hide();
		        $(this).find('.show').attr('class','hide');
		    }
		});
		// ajax 上传图片
		var upload_num = 0; // 上传图片成功数量
		$('#fileupload').fileupload({
		    dataType: 'json',
		    url: "<?php echo site_url('about/uni_img_upload'); ?>",
		    add: function (e,data) {
		    	$.each(data.files, function (index, file) {
		            $('<div nctype=' + file.name.replace(/\./g, '_') + '><p>'+ file.name +'</p><p class="loading"></p></div>').appendTo('div[nctype="file_loading"]');
		        });
		    	data.submit();
		    },
		    done: function (e,data) {
		        var param = data.result;
		        $this = $('div[nctype="' + param.origin_file_name.replace(/\./g, '_') + '"]');
		        $this.fadeOut(2000, function(){
		            $(this).remove();
		            if ($('div[nctype="file_loading"]').html() == '') {
		                setTimeout("window.location.reload()", 1000);
		            }
		        });
		        if(param.state == 'true'){
		            upload_num++;
		            $('div[nctype="file_msg"]').html('<i class="icon-ok-sign">'+'</i>'+'成功上传'+upload_num+'张图片');

		        } else {
		            $this.find('.loading').html(param.message).removeClass('loading');
		        }
		    }
		});

	})

function switchAll() {
	$('#batchClass').hide();
	$('input[type="checkbox"]').each(function(){
		$(this).attr('checked',!$(this).attr('checked'));
	});
}
function submit_form()
{
	var ids = [];
	$('input[type="checkbox"]').each(function(){
		var flag = $(this).attr('checked');
		if(flag =='checked')
		{
			ids.push($(this).val());
		}
	});
	if(ids.length == 0)
	{
		$.dialog({
			message : "请至少选择一张图片",
			timeout : 1,
			type : "error"
		});	
		return false;	
	}

	$.MsgBox.Confirm("温馨提示", "执行删除后将无法恢复，确定继续吗？", function () {
		id = ids.join("-");
		url = "<?php echo site_url('about/uni_img_del') ?>";
		$.post(url, {id: id}, function(data){
				if(!data.status){
					$.dialog({
						message : data.message || "操作失败",
						timeout : data.timeout || 1,
						type : "error"
					});
				}
				else
				{
                    window.location.reload(true);
				}
			}, 'json');

	});
	// if(!window.confirm('你确定要删除所选图片吗？'))  return false;
	// ids =  hd_confirm('你确定要删除所选图片吗？');
	// alert(ids);
	// if(! hd_confirm('你确定要删除所选图片吗？'))
	// {
	// 	return false;
	// }

}
</script>
</html>