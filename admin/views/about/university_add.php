<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>添加</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <link href="./css/seller_center2.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/myconfirm.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <style>
     table.table1 tr th{
        text-align: center;
        vertical-align: top;
     }
     .btn_a {
       margin-right: 10px;
       background: #666;
         border: none;
         color: #FFFFFF;
         cursor: pointer;
         font-size: 12px;
         height: 25px;
         overflow: hidden;
         box-shadow: none;
         vertical-align: middle;
         padding: 0px 10px 0px;
         display: inline-block;
         text-align: center;
         text-indent: 0px;
         line-height: 2em;
         margin-right: 10px;
         box-shadow: none !important;
     }
    .btn_as {
       margin-right: 10px;
       background: #4BAED4;
     }
    </style>
<body>
    

    <div class="wrap">
       <div class="menu_list">
        <ul>
          <li><a href="<?php echo site_url('about/sch_pics'); ?>"> 校区列表 </a></li>
          <li><a href="javascript:;" class='action'>新增</a></li> 
        </ul> 
       </div>
       <form action="<?php echo site_url('about/sch_pics_add'); ?>" method="post" id="fileupload" enctype="multipart/form-data">
            <table class="table1 hd-form">
              <tr>
                <th class='w100'>中文名称</th>
                <td>
                   <input type="text" name="uname" value="" style="width:260px;"/>
                </td>
             </tr>
              <tr>
                <th class='w100'>英文名称</th>
                <td>
                   <input type="text" name="enuname" value="" style="width:260px;"/>
                </td>
             </tr>
              <tr>
                <th class='w100'>图片上传</th>
                <td>
                   <div class="upload-con-div">
                     <div class="ncsc-upload-btn"> <a href="javascript:void(0);">
                       <span>
                         <input type="file" hidefocus="true" size="1" class="input-file" name="file" multiple="multiple"/>
                       </span>
                       <p><i class="icon-upload-alt"></i>上传图片</p>
                       </a> 
                     </div>
                   </div>
                   <div nctype="file_msg"></div>
                   <div class="upload-pmgressbar" nctype="file_loading"></div>
                </td>
             </tr>
             <!-- 添加图集 s -->
                     <tr style="display:none;">
                         <th class="w100">图片</th>
                         <td>
                             <div >
                                 <table class="table1" id="pic">
                                    <thead>
                                     <tr>
                                         <td width="220">描述</td>
                                         <td width="150">图片</td>
                                         <td width="150">置顶</td>
                                         <td align="left">操作</td>
                                     </tr>
                                     </thead>
                                 </table>
                            </div>
                         </td>
                     </tr>

             <!-- 添加图集 end -->
            <tr>
                <th class='w100'>排序</th>
                <td>
                    <input type="text" name="sort" style='width:150px;' value="255" />
                </td>
             </tr> 
             <tr>
                <th class='w100' style="text-indent:-9999px;">操作</th>
                <td>
                    <input type="submit" class="btn1" value=" 确定提交 "/>
                </td>
             </tr>
           </table>
       </form>  
    </div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>../resource/fileupload/jquery.iframe-transport.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url();?>../resource/fileupload/jquery.ui.widget.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url();?>../resource/fileupload/jquery.fileupload.js" charset="utf-8"></script>
<script>
    $(function(){
        // ajax 上传图片
        $('#fileupload').fileupload({
            dataType: 'json',
            url: "<?php echo site_url('about/sch_pics_upload'); ?>",
            add: function (e,data)
            {
                data.submit();
            },
            done: function (e,data) {
                var param = data.result;
                if(param.state == 'true')
                {
                   var str ='<tr class="pic_list"><td>';
                    str += '<input type="text" name="data[title][]" class="w300"/></td><td>';
                    str += '<input type="hidden" name="data[tupian][]" value="'+param.file_name+'" /><img src="'+param.file_path+'" alt="" width="80" height="80"/></td>'+
                       '<td><input class="fs" type="hidden" name="data[picstat][]" value="0" /><button class="btn_a" type="button"> 置顶</button></td><td><button type="button" class="hd-cancel-small" onclick="remove_upload_one_img(this)"> 删除</button></td></tr>';
                    $('#pic').append(str);
                    $('#pic').parent().parent().parent().css({ display: ""});
                }
                else  
                {
                    $this.find('.loading').html(param.message).removeClass('loading');
                }
            }
        });

    })
$('.btn_a').live('click',function (){
    var pic_status = $(this).prev('input').val() == 0 ? 1 : 0;
    if($(this).hasClass('btn_as'))
    {
        $(this).removeClass('btn_as').prev('input').val(pic_status);
    }
    else
    {
       $(this).addClass('btn_as').prev('input').val(pic_status); 
    }
    
})

function remove_upload_one_img(obj) 
{
    $.MsgBox.Confirm("温馨提示", "执行删除后将无法恢复，确定继续吗？", function () {
        var delFiles = $(obj).parents('tr').find('input').eq(1).val();
        $.post("<?php echo site_url('about/upload_img_remove')?>", {file: delFiles}, function (data) {
        if (data == 1) {
          $(obj).parents('tr.pic_list').eq(0).remove();
        } 
       })
    });

}

function submit_form()
{
    var ids = [];
    $('input[type="checkbox"]').each(function(){
        var flag = $(this).attr('checked');
        if(flag =='checked')
        {
            ids.push($(this).val());
        }
    });
    if(ids.length == 0)
    {
        $.dialog({
            message : "请至少选择一张图片",
            timeout : 1,
            type : "error"
        }); 
        return false;   
    }
}
</script>
</html>