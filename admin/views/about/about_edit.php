<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>校园简介编辑</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <style>
     table.table1 tr th{
     	text-align: right;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="<?php echo site_url('about/un_desc')."/?ace =".rand(10,10000000);?>" >列表 </a></li>
		  <li><a href="javascript:void(0);" class='action'>编辑</a></li> 
		</ul> 
	   </div>

        <div class="tab">
        	<ul class="tab_menu">
				<li lab="base"><a> 中文信息编辑 </a></li>
  				<li lab="tpl"> <a> 英文信息编辑 </a></li>
        	</ul>
            <div class="tab_content">

				<div id="base">
				<form name="ch_about" method="post" enctype="multipart/form-data">
				   	<table class="table1 hd-form">
				   	  <tr>
				   	    <th class='w60'>标题:</th>
				   		<td>
				   		   <input type="text" name="title"  style="width:400px;" value="<?php echo $abouts['title']; ?>" />
				   		</td>
				   	 </tr>

				   	   <tr>
				   	     <th class='w60'>内容:</th>
				   	 	<td>
				   	 	   <textarea name="desc" id="" cols="80" rows="16"><?php echo $abouts['desc']; ?></textarea>
				   	 	</td>
				   	  </tr>
					  <tr>
						<th>&nbsp;</th>
						<td>
						       <input type="hidden" name="id" value="<?php echo $abouts['id'] ?>" />
							  <input type="button" class="btn1" value=" 确定提交 " onclick="save_form('ch_about','<?php echo site_url('about/un_desc_edit/ch'); ?>')"/>
						</td>
					  </tr>
		           </table>
                </form> 	
				</div>
			  <div id="tpl">
			    <form name = 'en_about' method="post" enctype="multipart/form-data">
				   	<table class="table1 hd-form">
				   	  <tr>
				   	    <th class='w60'>标题:</th>
				   		<td>
				   		   <input type="text" name="title" value='<?php echo $en_abouts['title']; ?>'  style="width:400px;"/>
				   		</td>
				   	 </tr>		   	
				   	   <tr>
				   	     <th class='w60'>内容:</th>
				   	 	<td>
				   	 	   <textarea name="desc" id="" cols="80" rows="16"><?php echo $en_abouts['desc']; ?></textarea>
				   	 	</td>
				   	  </tr>
					  <tr>
						<th>&nbsp;</th>
						<td>
						       <input type="hidden" name="id" value="<?php echo $en_abouts['id']; ?>" />
							  <input type="button" class="btn1" value=" 确定提交 " onclick="save_form('en_about','<?php echo site_url('about/un_desc_edit/en'); ?>')"/>
						</td>
					  </tr>
		           </table>  
	              </form> 
				</div>
	
            </div>
        </div>
   
	</div>
</body>
<script>
	  $(function (){
	   $("form").validate({
	     title: {
	       rule: {
	         required: true
	     },
	     error: {
	        required: " 名称不能为空! "
	     },
	     message: " 请填写名称",
	     success: "正确"
	    }
	  }) ;

	})
	  // function test()
	  // {
	  // 	$('#base').find('.btn1').trigger('click');
	  // }
</script>
</html>