<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>编辑</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <script src='./js/cal/lhgcalendar.min.js'></script>
    <style>
     table.table1 tr th{
     	text-align: right;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="<?php echo site_url('wisdom/week')."/?ace =".rand(10,10000000);?>" >列表 </a></li>
		  <li><a href="javascript:void(0);" class='action'>编辑</a></li> 
		</ul> 
	   </div>

        <div class="tab">
        	<ul class="tab_menu">
				<li lab="content_ch"><a> 中文信息编辑 </a></li>
  				<li lab="content_en"> <a> 英文信息编辑 </a></li>
        	</ul>
            <div class="tab_content">

				<div id="content_ch">
					<form name="ch_week" method="post" enctype="multipart/form-data">
					   	<table class="table1 hd-form">
					   	   <tr>
					   	     <th class='w60'>内容:</th>
					   	 	<td>
					   	 	   <textarea name="content" id="" cols="50" rows="10"><?php echo $weeks['content'] ?></textarea>
					   	 	</td>
					   	  </tr>
			   	  		 <tr>
			   	  			<th>时间:</th>
			   	  			<td>
			   	                  <input type="text" readonly="readonly" id="updatetime" name="addtime"
			   	                    value="<?php echo date('Y/m/d h:i:s',$weeks['addtime']); ?>"
			   	                    class="w150"/>
			   	                  <script>
			   	                    $('#updatetime').calendar({format: 'yyyy/MM/dd HH:mm:ss'});
			   	                  </script>
			   	  			</td>
			   	  		 </tr>
						  <tr>
							<th>&nbsp;</th>
							<td>
							       <input type="hidden" name="id" value="<?php echo $weeks['id'] ?>" />
								  <input type="button" class="btn1" value=" 确定提交 " onclick="save_form('ch_week','<?php echo site_url('wisdom/week_add/ch'); ?>')"/>
							</td>
						  </tr>
			           </table>
		            </form> 	
				</div>
			 
			  <div id="content_en">
			    <form name = 'en_week' method="post" enctype="multipart/form-data">
				   	<table class="table1 hd-form">	   	
				   	   <tr>
				   	     <th class='w60'>内容:</th>
				   	 	<td>
				   	 	   <textarea name="content" id="" cols="50" rows="10"><?php echo $en_weeks['content'] ?></textarea>
				   	 	</td>
				   	  </tr>
		   	  		 <tr>
		   	  			<th><a href="javascript:;" class='week_copy'>同步时间</a>时间:</th>
		   	  			<td>
	   	                  <input type="text" readonly="readonly" id="updatetime1" name="addtime"
	   	                    value="<?php echo date('Y/m/d h:i:s',$weeks['addtime']); ?>"
	   	                    class="w150"/>
	   	                  <script>
	   	                    $('#updatetime1').calendar({format: 'yyyy/MM/dd HH:mm:ss'});
	   	                  </script>
		   	  			</td>
		   	  		 </tr>
					  <tr>
						<th>&nbsp;</th>
						<td>
						       <input type="hidden" name="id" value="<?php echo $en_weeks['id'] ?>" />
							  <input type="button" class="btn1" value=" 确定提交 " onclick="save_form('en_week','<?php echo site_url('wisdom/week_add/en'); ?>')"/>
						</td>
					  </tr>
		           </table>  
	              </form> 
				</div>
	
            </div>
        </div>
   
	</div>
</body>
</html>