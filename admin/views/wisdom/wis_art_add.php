<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>合一智慧添加</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <link href="./css/seller_center.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <script src="./js/copy.js"></script>
    <script src='./js/cal/lhgcalendar.min.js'></script>

    <script type="text/javascript" src="<?php echo base_url().'../' ?>org/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'../' ?>org/ueditor/ueditor.all.min.js"></script>
	<script type="text/javascript">
		window.UEDITOR_HOME_URL = "<?php echo base_url().'../' ?>org/ueditor/";
		window.onload = function(){
			window.UEDITOR_CONFIG.initialFrameWidth = 900;
			window.UEDITOR_CONFIG.initialFrameHeight = 400;
            UE.getEditor('content', {autoHeightEnabled: false});
            UE.getEditor('content1', {autoHeightEnabled: false});
        }
	</script>
    <style>
     table.table1 tr th{
     	text-align: right;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="<?php echo site_url('wisdom/index')."/?ace =".rand(10,10000000);?>"> 列表 </a></li>
		  <li><a href="javascript:;" class='action'>新增</a></li> 
		</ul> 
	   </div>
	   
	   <div class="tab">
	       	<ul class="tab_menu">
				<li lab="content_ch"><a> 中文信息添加 </a></li>
	 			<li lab="content_en"> <a> 英文信息添加 </a></li>
	       	</ul>
	        <div class="tab_content">
	        <!-- 中文添加 -->
	   		<div id="content_ch">
			   <form name='ch_wis_art' method="post" enctype="multipart/form-data">
				   	<table class="table1 hd-form">
				   	  <tr>
				   	    <th class='w100'>标题</th>
				   		<td>
				   		   <input type="text" name="title" value="" style="width:260px;"/>
				   		</td>
				   	 </tr>
				   	 <tr>
				   	    <th class='w100'>概要</th>
				   		<td>
				   			<textarea name="desc" id= ""cols="45" rows="6"></textarea>
				   		</td>
				   	 </tr>  

				   	 <tr>
				   	    <th class='w60'>图片:</th>
				   	    <td>
				   	 	    <div class="container">
				   	 	      <div class="ncsc-goodspic-list">
				   	 	        <ul nctype="ul">
				   	 	          <li class="ncsc-goodspic-upload">
				   	 	            <div class="upload-thumb">
				   	 	              <img src="./img/default_goods_image_240.gif" nctype="file_21">
				   	 	              <input type="hidden" name="image" value="" nctype="file_21">
				   	 	            </div>
		                            <div class="show-default" nctype="file_21">
		            	               <a href="javascript:void(0)" nctype="del" class="del" title="移除">X</a>
		            	            </div>
				   	 	            <div class="ncsc-upload-btn dx_upload_btn">
				   	 	              <a href="javascript:void(0);">
				   	 	              <span>
				   	 	                 <input type="file" hidefocus="true" size="1" class="input-file" name="file_21" id="file_21"></span>
				   	 	                 <p><i class="icon-upload-alt"></i>上传</p>
				   	 	              </a>
				   	 	            </div>     
				   	 	          </li>
				   	 	        </ul>
				   	 	      </div>
				   	 	      <div>*建议上传图片大小 350px * 244px</div>
				   	 	    </div>
				   	    </td>
				   	  </tr>
				   	 <tr>
				   	    <th class='w100'>信息类型</th>
				   		<td>
				   		  <select name="type" id="myselect">
		<!-- 		   		   <option value="" selected>选择信息格式</option> -->
				   		  	<option value="text" >文本信息</option>
				   		  	<option value="vedio">视频信息</option>
				   		  </select>	
				   		</td>
				   	 </tr>

				   	  <tr class="select1">
						<th>内容</th>
						<td>
							<textarea name="content" id="content" style="width:900px;height:400px;"></textarea>
						</td>
					  </tr> 

				   	  <tr class="select2"  style="display:none;">
						<th>视频链接</th>
						<td>
							<input type="text" name="vedio" style='width:350px;' value="" />
						</td>
					   </tr> 
					 
					<tr>
				   	    <th class='w100'>排序</th>
				   		<td>
				   			<input type="text" name="sort" style='width:150px;' value="255" />
				   		</td>
				   	 </tr> 
					<tr>
						<th>时间</th>
						<td>
			                <input type="text" readonly="readonly" id="updatetime" name="updatetime"
			                  value="<?php echo date('Y/m/d h:i:s',time()); ?>"
			                  class="w150"/>
			                <script>
			                  $('#updatetime').calendar({format: 'yyyy/MM/dd HH:mm:ss'});
			                </script>
						</td>
					</tr> 

				   	 <tr>
				   	    <th class='w100' style="text-indent:-9999px;">操作</th>
				   		<td>
				   		    <input type="hidden" name="id" value="" />
				   			<input type="button" class="btn1" value="确定提交" onclick="save_form('ch_wis_art','<?php echo site_url('wisdom/wis_art_add/ch'); ?>')"/>
				   			<input type="reset" class="btn2" value="重置">
				   		</td>
				   	 </tr>
		           </table>
			   </form>
            </div>
        	   		
        	<div id="content_en">
        			   <form  name='en_wis_art' method="post" enctype="multipart/form-data">
        				   	<table class="table1 hd-form">
        				   	  <tr>
        				   	    <th class='w100'>标题</th>
        				   		<td>
        				   		   <input type="text" name="title" value="" style="width:260px;"/>
        				   		</td>
        				   	 </tr>
        				   	 <tr>
        				   	    <th class='w100'>概要</th>
        				   		<td>
        				   			<textarea name="desc" id= ""cols="45" rows="6"></textarea>
        				   		</td>
        				   	 </tr>  

        				   	 <tr>
        				   	    <th class='w60'><a href="javascript:;" class = 'data_copy'>同步图片</a>图片:</th>
        				   	    <td>
        				   	 	    <div class="container">
        				   	 	      <div class="ncsc-goodspic-list">
        				   	 	        <ul nctype="ul">
        				   	 	          <li class="ncsc-goodspic-upload">
        				   	 	            <div class="upload-thumb">
        				   	 	              <img src="./img/default_goods_image_240.gif" nctype="file_22">
        				   	 	              <input type="hidden" name="image" value="" nctype="file_22">
        				   	 	            </div>
        		                            <div class="show-default" nctype="file_22">
        		            	               <a href="javascript:void(0)" nctype="del" class="del" title="移除">X</a>
        		            	            </div>
        				   	 	            <div class="ncsc-upload-btn dx_upload_btn">
        				   	 	              <a href="javascript:void(0);">
        				   	 	              <span>
        				   	 	                 <input type="file" hidefocus="true" size="1" class="input-file" name="file_22" id="file_22"></span>
        				   	 	                 <p><i class="icon-upload-alt"></i>上传</p>
        				   	 	              </a>
        				   	 	            </div>     
        				   	 	          </li>
        				   	 	        </ul>
        				   	 	      </div>
        				   	 	      <div>*建议上传图片大小 350px * 244px</div>
        				   	 	    </div>
        				   	    </td>
        				   	  </tr>
        				   	 <tr>
        				   	    <th class='w100'>信息类型</th>
        				   		<td>
        				   		  <select name="type" id="myselect1">
        				   		  	<option value="text" >文本信息</option>
        				   		  	<option value="vedio">视频信息</option>
        				   		  </select>	
        				   		</td>
        				   	 </tr>

        				   	  <tr class="select11">
        						<th>内容</th>
        						<td>
        							<textarea name="content" id="content1" style="width:900px;height:400px;"></textarea>
        						</td>
        					  </tr> 

        				   	  <tr class="select22"  style="display:none;">
        						<th><a href="javascript:;" class = 'link_copy'>同步排序</a>视频链接</th>
        						<td>
        							<input type="text" name="vedio" style='width:350px;' value="" />
        						</td>
        					   </tr> 
        					<tr>
        				   	    <th class='w100'><a href="javascript:;" class = 'sort_copy'>同步排序</a>排序</th>
        				   		<td>
        				   			<input type="text" name="sort" style='width:150px;' value="255" />
        				   		</td>
        				   	 </tr> 
        					<tr>
        						<th><a href="javascript:;" class = 'time_copy'>同步时间</a>时间</th>
        						<td>
        			                <input type="text" readonly="readonly" id="updatetime" name="updatetime"
        			                  value="<?php echo date('Y/m/d h:i:s',time()); ?>"
        			                  class="w150"/>
        			                <script>
        			                  $('#updatetime').calendar({format: 'yyyy/MM/dd HH:mm:ss'});
        			                </script>
        						</td>
        					</tr> 

        				   	 <tr>
        				   	    <th class='w100' style="text-indent:-9999px;">操作</th>
        				   		<td>
    				   		        <input type="hidden" name="id" value="" />
    				   		    	<input type="button" class="btn1" value="确定提交" onclick="save_form('en_wis_art','<?php echo site_url('wisdom/wis_art_add/en'); ?>')"/>
    				   		    	<input type="reset" class="btn2" value="重置">
        				   		</td>
        				   	 </tr>
        		           </table>
        			   </form>
                    </div>
         </div>
        </div>

	</div>
	<script src="<?php echo base_url();?>../resource/ajaxfileupload/ajaxfileupload.js" ></script>
	<script>
	   var target_url = "<?php echo site_url('wisdom/wis_img_upload'); ?>";
	   var del_img_url = "<?php echo site_url('wisdom/wis_img_del'); ?>";
		$(function(){
			$('#myselect').change(function(){ 
               var p1=$(this).children('option:selected').val();//这就是selected的值 
               if(p1 == 'text')
               {
               	$('.select1').css('display','');
               	$('.select2').css('display','none');
               }else if(p1 == 'vedio'){
               	$('.select1').css('display','none');
               	$('.select2').css('display','');
               }
            }) 
			$('#myselect1').change(function(){ 
               var p1=$(this).children('option:selected').val();//这就是selected的值 
               if(p1 == 'text')
               {
               	$('.select11').css('display','');
               	$('.select22').css('display','none');
               }else if(p1 == 'vedio'){
               	$('.select11').css('display','none');
               	$('.select22').css('display','');
               }
            }) 
		 //  $("form").validate({
		 //    wtitle: {
		 //      rule: {
		 //        required: true
		 //    },
		 //    error: {
		 //       required: " 标题不能为空! "
		 //    },
		 //    message: " 请填写标题",
		 //    success: "正确"
		 //   },
		 //    description: {
		 //      rule: {
		 //        required: true
		 //    },
		 //    error: {
		 //       required: " 概要不能为空! "
		 //    },
		 //    message: " 请填写概要内容",
		 //    success: "正确"
		 //   },
		 //   sort: {
		 //      rule: {
		 //        required: true
		 //    },
		 //    error: {
		 //       required: " 排序不能为空! "
		 //    },
		 //    message: " 请输入排序!,默认255,数字越小,优先级越高",
		 //    success: "正确"
		 //   },
		 //   vedio:{
		 //   	message:"请输入正确的视频链接地址"
		 //   },
		 //   image: {
		 //   	message: "推荐上传 360px * 263px大小的图片"
		 //   }
		 // })   
		});
	</script>
</body>
</html>