<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>编辑</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
        <link href="./css/seller_center.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <script src="./js/copy.js"></script>
    <style>
     table.table1 tr th{
     	text-align: right;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="<?php echo site_url('share/saying')."/?ace =".rand(10,10000000);?>" >列表 </a></li>
		  <li><a href="javascript:void(0);" class='action'>编辑</a></li> 
		</ul> 
	   </div>

        <div class="tab">
        	<ul class="tab_menu">
				<li lab="content_ch"><a> 中文信息编辑 </a></li>
  				<li lab="content_en"> <a> 英文信息编辑 </a></li>
        	</ul>
            <div class="tab_content">

				<div id="content_ch">
					<form name="ch_week" method="post" enctype="multipart/form-data">
					   	<table class="table1 hd-form">
					   	  <tr>
					   	    <th class='w60'>姓名:</th>
					   		<td>
					   		   <input type="text" name="name"  style='width:250px;' value="<?php echo $says['name'] ?>"/>
					   		</td>
					   	 </tr>
					   	 		   	 <tr>
					   	 		   	    <th class='w60'>图片:</th>
					   	 		   	    <td>
					   	 		   	 	    <div class="container">
					   	 		   	 	      <div class="ncsc-goodspic-list">
					   	 		   	 	        <ul nctype="ul">
					   	 		   	 	          <li class="ncsc-goodspic-upload">
					   	 		   	 	            <div class="upload-thumb">
					   	 		   	 	            <?php if(! empty($says['image'])):?>
					   	 		   	 	            	 <img src="<?php echo base_url(); ?>../uploads/share/<?php echo $says['image'];?>" nctype="file_21">
					   	 		   	 	            	 <input type="hidden" name="image" value="<?php echo $says['image'] ?>" nctype="file_21">
					   	 		   	 	            <?php else:?>
					   	 		   	 	            	<img src="./img/default_goods_image_240.gif" nctype="file_21">
					   	 		   	 	            	<input type="hidden" name="image" value="" nctype="file_21">
					   	 		   	 	            <?php endif; ?>
					   	 		   	 	            </div>
					   	                             <div class="show-default" nctype="file_21">
					   	             	               <a href="javascript:void(0)" nctype="del" class="del" title="移除">X</a>
					   	             	            </div>
					   	 		   	 	            <div class="ncsc-upload-btn dx_upload_btn">
					   	 		   	 	              <a href="javascript:void(0);">
					   	 		   	 	              <span>
					   	 		   	 	                 <input type="file" hidefocus="true" size="1" class="input-file" name="file_21" id="file_21"></span>
					   	 		   	 	                 <p><i class="icon-upload-alt"></i>上传</p>
					   	 		   	 	              </a>
					   	 		   	 	            </div>     
					   	 		   	 	          </li>
					   	 		   	 	        </ul>
					   	 		   	 	      </div>
					   	 		   	 	      <div>*建议上传图片大小 210px * 210px</div>
					   	 		   	 	    </div>
					   	 		   	    </td>
					   	 		   	  </tr>
					   	  <tr>
					   	    <th class='w60'>名人简介:</th>
					   		<td>
					   		   <textarea name="desc" id="" cols="50" rows="8"><?php echo $says['desc'] ?></textarea>
					   		</td>
					   	 </tr>
					   	   <tr>
					   	     <th class='w60'>内容:</th>
					   	 	<td>
					   	 	   <textarea name="content" id="" cols="50" rows="8"><?php echo $says['content'] ?></textarea>
					   	 	</td>
					   	  </tr>
					   	  <tr>
				   	     	<th class='w100'>排序</th>
			   	     		<td>
			   	     			<input type="text" name="sort" style='width:250px;' value="<?php echo $says['sort'] ?>" />
			   	     		</td>
					   	   </tr> 
						  <tr>
							<th>&nbsp;</th>
							<td>
							       <input type="hidden" name="id" value="<?php echo $says['id'] ?>" />
								  <input type="button" class="btn1" value=" 确定提交 " onclick="save_form('ch_week','<?php echo site_url('share/saying_add/ch'); ?>')"/>
							</td>
						  </tr>
			           </table>
		            </form> 	
				</div>
			 
			  <div id="content_en">
			    <form name = 'en_week' method="post" enctype="multipart/form-data">
				   	<table class="table1 hd-form">	   	
				   	  <tr>
				   	    <th class='w60'>姓名:</th>
				   		<td>
				   		   <input type="text" name="name"  style='width:250px;' value="<?php echo $en_says['name'] ?>" />
				   		</td>
				   	 </tr>
				   	 		   	 <tr>
				   	 		   	    <th class='w60'><a href="javascript:;" class = 'data_copy'>同步图片</a>图片:</th>
				   	 		   	    <td>
				   	 		   	 	    <div class="container">
				   	 		   	 	      <div class="ncsc-goodspic-list">
				   	 		   	 	        <ul nctype="ul">
				   	 		   	 	          <li class="ncsc-goodspic-upload">
				   	 		   	 	            <div class="upload-thumb">
				   	 		   	 	            <?php if(! empty($en_says['image'])):?>
				   	 		   	 	            	 <img src="<?php echo base_url(); ?>../uploads/share/<?php echo $en_says['image'];?>" nctype="file_22">
				   	 		   	 	            	 <input type="hidden" name="image" value="<?php echo $en_says['image'] ?>" nctype="file_22">
				   	 		   	 	            <?php else:?>
				   	 		   	 	            	<img src="./img/default_goods_image_240.gif" nctype="file_22">
				   	 		   	 	            	<input type="hidden" name="image" value="" nctype="file_22">
				   	 		   	 	            <?php endif; ?>
				   	 		   	 	            </div>
				   	                             <div class="show-default" nctype="file_22">
				   	             	               <a href="javascript:void(0)" nctype="del" class="del" title="移除">X</a>
				   	             	            </div>
				   	 		   	 	            <div class="ncsc-upload-btn dx_upload_btn">
				   	 		   	 	              <a href="javascript:void(0);">
				   	 		   	 	              <span>
				   	 		   	 	                 <input type="file" hidefocus="true" size="1" class="input-file" name="file_22" id="file_22"></span>
				   	 		   	 	                 <p><i class="icon-upload-alt"></i>上传</p>
				   	 		   	 	              </a>
				   	 		   	 	            </div>     
				   	 		   	 	          </li>
				   	 		   	 	        </ul>
				   	 		   	 	      </div>
				   	 		   	 	      <div>*建议上传图片大小 210px * 210px</div>
				   	 		   	 	    </div>
				   	 		   	    </td>
				   	 		   	  </tr>
				   	  <tr>
				   	    <th class='w60'>名人简介:</th>
				   		<td>
				   		   <textarea name="desc" id="" cols="50" rows="8"><?php echo $en_says['desc'] ?></textarea>
				   		</td>
				   	 </tr>
				   	   <tr>
				   	     <th class='w60'>内容:</th>
				   	 	<td>
				   	 	   <textarea name="content" id="" cols="50" rows="8"><?php echo $en_says['content'] ?></textarea>
				   	 	</td>
				   	  </tr>
				   	  	<tr>
				   	     	<th class='w100'><a href="javascript:;" class = 'sort_copy'>同步排序</a>排序</th>
			   	     		<td>
			   	     			<input type="text" name="sort" style='width:250px;' value="<?php echo $en_says['sort'] ?>" />
			   	     		</td>
				   	    </tr>
					  <tr>
						<th>&nbsp;</th>
						<td>
						       <input type="hidden" name="id" value="<?php echo $en_says['id'] ?>" />
							  <input type="button" class="btn1" value=" 确定提交 " onclick="save_form('en_week','<?php echo site_url('share/saying_add/en'); ?>')"/>
						</td>
					  </tr>
		           </table>  
	              </form> 
				</div>
	
            </div>
        </div>
   
	</div>
</body>
<script src="<?php echo base_url();?>../resource/ajaxfileupload/ajaxfileupload.js" ></script>
<script>
   var target_url = "<?php echo site_url('share/saying_img_upload'); ?>";
   var del_img_url = "<?php echo site_url('share/saying_img_del'); ?>";
</script>
</html>