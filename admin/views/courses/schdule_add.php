<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>添加</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <link href="./css/seller_center2.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/myconfirm.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/copy.js"></script>
    <script src="./js/validate.js"></script>
    <style>
     table.table1 tr th{
        text-align: center;
        vertical-align: top;
     }
    </style>
<body>
    

    <div class="wrap">
       <div class="menu_list">
        <ul>
          <li><a href="<?php echo site_url('course/schedule')."/?ace =".rand(10,10000000);?>"> 列表 </a></li>
          <li><a href="javascript:;" class='action'>新增</a></li> 
        </ul> 
       </div>
      <div class="tab">
        <ul class="tab_menu">
            <li lab="content_ch"><a> 中文信息添加 </a></li>
            <li lab="content_en"> <a> 英文信息添加 </a></li>
        </ul>
        <div class="tab_content">
            
            <div id="content_ch">
            <form name="ch_course" action="" method="post">
                 <table class="table1 hd-form">
                   <tr>
                     <th class='w100'>时间</th>
                     <td>
                        <input type="text" name="year" value="" style="width:260px;"/>
                     </td>
                  </tr>
                  <!-- 添加课程 s -->
                          <tr style="">
                              <th class="w100">课程安排</th>
                              <td>
                                  <div >
                                      <table class="table1" id="pic">
                                         <thead>
                                          <tr>
                                              <td width="40">月份</td>
                                              <td width="">课程(文本框填写时间节点如：2.01-2.29)</td>
                                              <td width="10" align="left"></td>
                                          </tr>
                                          </thead>
                                         <?php for($i = 1;$i <13;$i++){ ?>
                                            <tr>
                                                 <td><?php echo $i ?>月</td>
                                                 <td>
                                                <?php if(! empty($courses)): ?>
                                                   <?php foreach ($courses as $k => $v): ?> 
                                                   <div style="float:left;padding-right:5px;border-right:1px solid #ccc">  
                                                      <label for="radio-<?php echo $i.$k; ?>">
                                                         <input type="checkbox" id="radio-<?php echo $i.$k; ?>" name="con[<?php echo $i;?>][con][<?php echo $k;?>]" value="<?php echo $v['title'] ?>">
                                                         <?php echo $v['title'] ?>
                                                         </label>   
                                                         <input style="width:66px" type="text" name="con[<?php echo $i;?>][time][<?php echo $k;?>]" value=''/>
                                                         <p style="margin-top:4px;padding-left:15px">备注： <input style="width:100px" type="text" name="con[<?php echo $i;?>][remark][<?php echo $k;?>]" value=''/></p>
                                                    </div>      
                                                   <?php endforeach ?>
                                                 <?php endif; ?>
                                                 </td> 
                                                <td><input class="w300" type="hidden" name="con[<?php echo $i;?>][extra]" value='0'/></td>
                                            </tr>
                                          <?php } ?>
                                      </table>
                                 </div>
                              </td>
                          </tr>

                  <!-- 添加课程 end -->
                  <tr>
                     <th class='w100' style="text-indent:-9999px;">操作</th>
                     <td>
                        <input type="hidden" name="id" value="" />
                        <input type="button" class="btn1" value="确定提交" onclick="save_form('ch_course','<?php echo site_url('course/schdule_add/ch'); ?>')"/>
                        <input type="reset" class="btn2" value="重置">
                     </td>
                  </tr>
                </table>
            </form> 
            </div>

            <div id="content_en">
            <form action="" name="en_course" method="post">
                 <table class="table1 hd-form">
                   <tr>
                     <th class='w100'><a href="javascript:;" class = 'year_copy'>同步时间</a> 时间</th>
                     <td>
                        <input type="text" name="year" value="" style="width:260px;"/>
                     </td>
                  </tr>
                  <!-- 添加课程 s -->
                          <tr style="">
                              <th class="w100">课程安排</th>
                              <td>
                                  <div >
                                      <table class="table1" id="pic">
                                         <thead>
                                          <tr>
                                              <td width="40">月份</td>
                                              <td width="">课程(文本框填写时间节点如：2.01-2.29)</td>
                                              <td width="10" align="left"></td>
                                          </tr>
                                          </thead>
                                         <?php for($i = 1;$i <13;$i++){ ?>
                                            <tr>
                                                 <td><?php echo $i ?>月</td>
                                                 <td>
                                                <?php if(! empty($en_courses)): ?>
                                                   <?php foreach ($en_courses as $k => $v): ?>  
                                                   <div style="float:left;padding-right:5px;border-right:1px solid #ccc">  
                                                      <label for="radio1-<?php echo $i.$k; ?>">
                                                         <input type="checkbox" id="radio1-<?php echo $i.$k; ?>" name="con[<?php echo $i;?>][con][<?php echo $k;?>]" value="<?php echo $v['title'] ?>">
                                                         <?php echo $v['title'] ?>
                                                      </label>  
                                                      <input style="width:66px" type="text" name="con[<?php echo $i;?>][time][<?php echo $k;?>]" value=''/>
                                                      <p style="margin-top:4px;padding-left:15px">备注： <input style="width:100px" type="text" name="con[<?php echo $i;?>][remark][<?php echo $k;?>]" value=''/></p>
                                                   </div>        
                                                   <?php endforeach ?>
                                                 <?php endif; ?>
                                                 </td> 
                                                <td>
                                                   <input class="w300" type="hidden" name="con[<?php echo $i;?>][extra]" value='0'/>
                                                </td>
                                            </tr>
                                          <?php } ?>
                                      </table>
                                 </div>
                              </td>
                          </tr>

                  <!-- 添加课程 end -->
                  <tr>
                     <th class='w100' style="text-indent:-9999px;">操作</th>
                     <td>
                     <input type="hidden" name="id" value="" />
                     <input type="button" class="btn1" value="确定提交" onclick="save_form('en_course','<?php echo site_url('course/schdule_add/en'); ?>')"/>
                     <input type="reset" class="btn2" value="重置">
                     </td>
                  </tr>
                </table>
            </form> 
            </div>
        </div>  
      </div>
    </div>
</body>
<script>
    $(function(){
      $("form").validate({
        year: {
        message: " 请填写正确格式年份，如：2014"
       }
     })   
    });
</script>
</html>