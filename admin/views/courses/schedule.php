<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>列表</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/myconfirm.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <style>
     table.table1 tr th{
     	text-align: center;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="javascript:void(0);" class='action'> 列表 </a></li>
		  <li><a href="<?php echo site_url('course/schdule_add')."/?ace =".rand(10,10000000); ?>"> 新增 </a></li>
		</ul> 
	   </div>
	   	<table class="table2 hd-form form-inline">
	   	<thead>
	   	  <tr>
	   	    <td class='w50'>ID</td>
	   		  <td class=''>年份</td>
	   		  <td class='w200'>操作</td>
	   	 </tr>	   		
	   	</thead>
        <tbody>
        <?php foreach($schs as $v): ?>
         <tr>
	   		    <td><?php echo $v['id'];?></td>
	   	      <td><?php echo $v['year'];?></td>
         		<td>
         		    <a class='btn1' href="<?php echo site_url('course/schedule_edit/'.$v['id'])."/?ace =".rand(10,10000000);?>">编辑</a>
         			<a class='btn2' href="javascript:;" onclick="obj_del(<?php echo $v['id']; ?>,'<?php echo site_url('course/schedule_del') ?>')">删除</a>
         		</td>        	
         </tr>
         <?php  endforeach;?>
         </tbody>
	   </table>
	</div>
</body>
</html>