1<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>新闻添加</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <script src="./js/copy.js"></script>

    <script type="text/javascript" src="<?php echo base_url().'../' ?>org/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'../' ?>org/ueditor/ueditor.all.min.js"></script>
	<script type="text/javascript">
		window.UEDITOR_HOME_URL = "<?php echo base_url().'../' ?>org/ueditor/";
		window.onload = function(){
			window.UEDITOR_CONFIG.initialFrameWidth = 900;
			window.UEDITOR_CONFIG.initialFrameHeight = 400;
            UE.getEditor('content', {autoHeightEnabled: false});
            UE.getEditor('content1', {autoHeightEnabled: false});
        }
	</script>
    <style>
     table.table1 tr th{
     	text-align: right;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="<?php echo site_url('news/onew')."/?ace =".rand(10,10000000);?>" >列表 </a></li>
		  <li><a href="javascript:void(0);" class='action'>编辑</a></li> 
		</ul> 
	   </div>

        <div class="tab">
        	<ul class="tab_menu">
				<li lab="content_ch"><a> 中文信息添加 </a></li>
  				<li lab="content_en"> <a> 英文信息添加 </a></li>
        	</ul>
            <div class="tab_content">
           <!-- 中文添加 -->
				<div id="content_ch">
					<form name='ch_event' method="post" enctype="multipart/form-data">
					   	<table class="table1 hd-form">
					   	  <tr>
					   	    <th class='w60'>标题:</th>
					   		<td>
					   		   <input type="text" name="title"  style="width:260px;" value="<?php echo $nots['title'] ?>"/>
					   		</td>
					   	 </tr>
					   	 	    <tr>
					   	      	    <th class='w100'>TAG:</th>
					   	      		<td>
					   	      		    <label for="radio-5"><input type="radio" id="radio-5" name="tag" <?php if($nots['tag'] == 2): ?> checked="true" <?php endif; ?> value="2">普通</label>
					   	      			<label for="radio-3"><input type="radio" id="radio-3" name="tag" <?php if($nots['tag'] == 1): ?> checked="true" <?php endif; ?>value="1">热门</label>
					   	      			<label for="radio-4"><input type="radio" id="radio-4" name="tag" <?php if($nots['tag'] == 0): ?> checked="true" <?php endif; ?>value="0">置顶</label>
					   	      		</td>
					   	 	    </tr> 				   	 
					   	  <tr>
					   	    <th class='w100'>描述:</th>
					   	 	<td>
					   	 		<textarea name="desc" id="" cols="46" rows="6"><?php echo $nots['desc'] ?></textarea>
					   	 	</td>
					   	  </tr>
					   	  				  <tr>
					   	  				   	    <th class='w100'>信息类型</th>
					   	  				   		<td>
					   	  			    <?php if($nots['type'] == 'text'): ?>
					   	  			    	<input type="hidden" name = 'type' value="text" />
					   	                          文本信息
					   	  				   		</td>
					   	  				   	 </tr>

					   	  				   	  <tr class="select11">
					   	  						<th>内容</th>
					   	  						<td>
					   	  							<textarea name="content" id="content" style="width:900px;height:400px;"><?php echo $nots['content'] ?></textarea>
					   	  						</td>
					   	  					  </tr> 
					   	                  <?php else: ?>
					   	                  	<input type="hidden" name = 'type' value="vedio" />
					   	                          视频信息
					   	  				   		</td>
					   	  				   	 </tr>
					   	  				   	  <tr class="select22">
					   	  						<th>视频链接</th>
					   	  						<td>
					   	  							<input type="text" name="vedio" style='width:350px;' value="<?php echo htmlspecialchars_decode(unserialize($nots['vedio'])) ?>" />
					   	  						</td>
					   	  					   </tr> 
					   	  				<?php endif; ?>	
						 <tr>
							<th>时间:</th>
							<td>
				                <script src='./js/cal/lhgcalendar.min.js'></script>
				                <input type="text" readonly="readonly" id="updatetime" name="updatetime"
				                  value="<?php echo date('Y/m/d h:i:s',$nots['updatetime']); ?>"
				                  class="w150"/>
				                <script>
				                  $('#updatetime').calendar({format: 'yyyy/MM/dd HH:mm:ss'});
				                </script>
							</td>
						  </tr> 
 
			   	          <tr>
					   	    <th class='w60'>排序:</th>
					   		<td>
					   		   <input type="text" name="sort" style="width:160px;" value="<?php echo $nots['sort'] ?>" />
					   		</td>
					   	 </tr>	

						  <tr>
							<th>&nbsp;</th>
							<td>
							    <input type="hidden" name="id" value="<?php echo $nots['id'] ?>" />
								<input style = 'display:inline;' type="button" class="btn1" value=" 确定提交 " onclick="save_form('ch_event','<?php echo site_url('news/news_add/ch'); ?>')"/>
								<input type="reset" class="btn2" value="重置">
							</td>
						  </tr>
			           </table>
	                </form> 	
				</div>
	         <!-- 中文结束-->		 

			  <div id="content_en">
			 <!-- 英语添加 -->
			   		<form name='en_event' method="post" enctype="multipart/form-data">
					   	<table class="table1 hd-form">
					   	  <tr>
					   	    <th class='w60'>标题:</th>
					   		<td>
					   		   <input type="text" name="title"  style="width:260px;" value="<?php echo $en_nots['title'] ?>" />
					   		</td>
					   	 </tr>	
					   	 	    <tr>
					   	      	    <th class='w100'>TAG:</th>
					   	      		<td>
					   	      		    <label for="radio-0"><input type="radio" id="radio-0" name="tag" <?php if($en_nots['tag'] == 2): ?> checked="true" <?php endif; ?> value="2">普通</label>
					   	      			<label for="radio-1"><input type="radio" id="radio-1" name="tag" <?php if($en_nots['tag'] == 1): ?> checked="true" <?php endif; ?> value="1">热门</label>
					   	      			<label for="radio-2"><input type="radio" id="radio-2" name="tag" <?php if($en_nots['tag'] == 0): ?> checked="true" <?php endif; ?> value="0">置顶</label>
					   	      		</td>
					   	 	    </tr> 			   	 
					   	  <tr>
					   	    <th class='w100'>描述:</th>
					   	 	<td>
					   	 		<textarea name="desc" id="" cols="46" rows="6"><?php echo $en_nots['desc'] ?></textarea>
					   	 	</td>
					   	  </tr>	
					   	          				   	 <tr>
					   	          				   	    <th class='w100'>信息类型</th>
					   	          				   		<td>
					   	          				   	<?php if($en_nots['type'] == 'text'): ?>
					   	  	   					    	<input type="hidden" name = 'type' value="text" />
					   	  	   		                        文本信息
					   	  	   						   		</td>
					   	  	   						   	 </tr>

					   	  	   						   	  <tr class="select1">
					   	  	   								<th>内容</th>
					   	  	   								<td>
					   	  	   									<textarea name="content" id="content1" style="width:900px;height:400px;"><?php echo $en_nots['content'] ?></textarea>
					   	  	   								</td>
					   	  	   							  </tr> 
					   	  	   		                <?php else: ?>
					   	  	   		                	<input type="hidden" name = 'type' value="vedio" />
					   	  	   		                        视频信息
					   	  	   						   		</td>
					   	  	   						   	 </tr>
					   	  	   						   	  <tr class="select2">
					   	  	   								<th>视频链接</th>
					   	  	   								<td>
					   	  	   									<input type="text" name="vedio" style='width:350px;' value="<?php echo htmlspecialchars_decode(unserialize($en_nots['vedio'])) ?>" />
					   	  	   								</td>
					   	  	   							   </tr> 
					   	  	   						<?php endif; ?>	 
						 <tr>
							<th><a href="javascript:;" class = 'time_copy'>同步时间</a>时间:</th>
							<td>
				                <script src='./js/cal/lhgcalendar.min.js'></script>
				                <input type="text" readonly="readonly" id="updatetime1" name="updatetime"
				                  value="<?php echo date('Y/m/d h:i:s',$en_nots['updatetime']); ?>"
				                  class="w150"/>
				                <script>
				                  $('#updatetime1').calendar({format: 'yyyy/MM/dd HH:mm:ss'});
				                </script>
							</td>
						  </tr>  
			   	          <tr>
					   	    <th class='w60'><a href="javascript:;" class = 'sort_copy'>同步排序</a>排序:</th>
					   		<td>
					   		   <input type="text" name="sort" style="width:160px;" value="<?php echo $en_nots['sort'] ?>" />
					   		</td>
					   	 </tr>	

						  <tr>
							<th>&nbsp;</th>
							<td>
							    <input type="hidden" name="id" value="<?php echo $en_nots['id'] ?>" />
								<input type="button" class="btn1" value="确定提交" onclick="save_form('en_event','<?php echo site_url('news/news_add/en'); ?>')"/>
								<input type="reset" class="btn2" value="重置">
							</td>
						  </tr>
			           </table>
	                </form> 
            <!-- 英语添加  end -->
				</div>   
            </div>
        </div>
   
	</div>
</body>
 <script>
   $(function (){
  	   $("form").validate({
  	     title: {
  	       rule: {
  	         required: true
  	     },
  	     error: {
  	        required: " 名称不能为空! "
  	     },
  	     message: " 请填写名称",
  	     success: "正确"
  	    },
  	    sort: {
  	    	message: "数字范围为0~255，数字越小越靠前"
  	    }
  	  })   	
  	})
 </script>
</html>