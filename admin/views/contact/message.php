<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>列表</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <script src="./js/myconfirm.js"></script>
    <style>
     table.table1 tr th{
     	text-align: center;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="javascript:;" class='action'> 列表 </a></li>
		</ul> 
	   </div>
	   	<table class="table2 hd-form form-inline">
	   	<thead>
	   	  <tr>
	   	    <td class='w50'>ID</td>
	   		<td class=''>内容</td>
	   	    <td class='w150'>姓名</td>
	   	    <td class='w150'>邮箱</td>
	   		<td class='w150'>时间</td>
	   		<td class='w200'>操作</td>
	   	 </tr>	   		
	   	</thead>
         <tbody>
         <?php foreach($messages as $k=>$v): ?>
         <tr>
	   		<td><?php echo (intval($k) + 1);?></td>
	   	    <td><?php echo $v['lmsg'];?></td>
	   		<td><?php echo $v['lname'];?></td>
	   		<td><?php echo $v['lemail'];?></td>
	   	    <td><?php echo date('Y-m-d h:i',$v['ltime']); ?></td>
	   		<td>
	   			<a class='btn2' href="javascript:;" onclick="obj_del(<?php echo $v['id']; ?>,'<?php echo site_url('contact/message_del') ?>')">删除</a>
	   		</td>         	
         </tr>
         <?php endforeach;?>
         </tbody>
	   </table>
	   <div class="page">
	   	 <?php echo $links ?>
	   </div>
	</div>
</body>
</html>