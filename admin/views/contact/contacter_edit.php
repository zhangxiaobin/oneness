<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>合一智慧添加</title>
</head>
    <base href="<?php echo base_url().'views/style/'; ?>" />
    <link href="./css/media.css" rel="stylesheet">
    <link href="./css/seller_center.css" rel="stylesheet">
    <script src="./js/jquery-1.8.2.min.js"></script>
    <script src="./js/media.js"></script>
    <script src="./js/validate.js"></script>
    <script src="./js/copy.js"></script>
    <script src='./js/cal/lhgcalendar.min.js'></script>
    <style>
     table.table1 tr th{
     	text-align: right;
     }
    </style>
<body>
	<div class="wrap">
	   <div class="menu_list">
	    <ul>
		  <li><a href="<?php echo site_url('contact/contacter')."/?ace =".rand(10,10000000);?>"> 列表 </a></li>
		  <li><a href="javascript:;" class='action'>新增</a></li> 
		</ul> 
	   </div>
	   
	   <div class="tab">
	       	<ul class="tab_menu">
				<li lab="content_ch"><a> 中文信息添加 </a></li>
	 			<li lab="content_en"> <a> 英文信息添加 </a></li>
	       	</ul>
	        <div class="tab_content">
	        <!-- 中文添加 -->
	   		<div id="content_ch">
			   <form name='ch_wis_art' method="post" enctype="multipart/form-data">
				   	<table class="table1 hd-form">
<!-- 				   	  <tr>
				   	    <th class='w100'>姓名</th>
				   		<td>
				   	
				   		   <input type="text" name="name" value="<?php echo $cons['name'] ?>" style="width:260px;"/>
				   		</td>
				   	 </tr> -->
				   	   <tr>
				   	     <th class='w100'>团队名称</th>
				   	 	<td>
				   	 	   <input type="text" name="team" value="<?php echo $cons['team'] ?>" style="width:260px;"/>
				   	 	</td>
				   	  </tr>
				   	    <tr>
				   	      <th class='w100'>微信</th>
				   	  	<td>
				   	  	   <input type="text" name="wechat" value="<?php echo $cons['wechat'] ?>" style="width:260px;"/>
				   	  	</td>
				   	   </tr>
				   	 <tr>
				   	    <th class='w100'>城市</th>
				   		<td>
				   			<textarea name="city" id= ""cols="40" rows="6"><?php echo $cons['city'] ?></textarea>
				   		</td>
				   	 </tr>  

				   	 <tr>
				   	    <th class='w60'>联系方式:</th>
				   	    <td>
				   	       <textarea name="phone" id= ""cols="40" rows="6"><?php echo $cons['phone'] ?></textarea>
				   	    </td>
				   	  </tr>
				   	  <tr>
				   	     <th class='w60'>备注:</th>
				   	     <td>
				   	        <textarea name="remark" id= ""cols="40" rows="6"><?php echo $cons['remark'] ?></textarea>
				   	     </td>
				   	   </tr>
					 
					<tr>
				   	    <th class='w100'>排序</th>
				   		<td>
				   			<input type="text" name="sort" style='width:150px;' value="<?php echo $cons['sort'] ?>" />
				   		</td>
				   	 </tr> 
 
				   	 <tr>
				   	    <th class='w100' style="text-indent:-9999px;">操作</th>
				   		<td>
				   		    <input type="hidden" name="status" style='width:150px;' value="" />
				   		    <input type="hidden" name="id" value="<?php echo $cons['id'] ?>" />
				   			<input type="button" class="btn1" value="确定提交" onclick="save_form('ch_wis_art','<?php echo site_url('contact/contacter_add/ch'); ?>')"/>
				   			<input type="reset" class="btn2" value="重置">
				   		</td>
				   	 </tr>
		           </table>
			   </form>
            </div>
        	   		
        	<div id="content_en">
        			   <form  name='en_wis_art' method="post" enctype="multipart/form-data">
        				   	<table class="table1 hd-form">
<!--         				   	   	  <tr>
        				   	   	    <th class='w100'>姓名</th>
        				   	   		<td>
        				   	   		   <input type="text" name="name" value="<?php echo $en_cons['name'] ?>" style="width:260px;"/>
        				   	   		</td>
        				   	   	 </tr> -->
        				   	   	   <tr>
        				   	   	     <th class='w100'>团队名称</th>
        				   	   	 	<td>
        				   	   	 	   <input type="text" name="team" value="<?php echo $en_cons['team'] ?>" style="width:260px;"/>
        				   	   	 	</td>
        				   	   	  </tr>
        				   	   	    <tr>
        				   	   	      <th class='w100'>微信</th>
        				   	   	  	<td>
        				   	   	  	   <input type="text" name="wechat" value="<?php echo $en_cons['wechat'] ?>" style="width:260px;"/>
        				   	   	  	</td>
        				   	   	   </tr>
        				   	   	 <tr>
        				   	   	    <th class='w100'>城市</th>
        				   	   		<td>
        				   	   			<textarea name="city" id= ""cols="40" rows="6"><?php echo $en_cons['city'] ?></textarea>
        				   	   		</td>
        				   	   	 </tr>  

        				   	   	 <tr>
        				   	   	    <th class='w60'>联系方式:</th>
        				   	   	    <td>
        				   	   	       <textarea name="phone" id= ""cols="40" rows="6"><?php echo $en_cons['phone'] ?></textarea>
        				   	   	    </td>
        				   	   	  </tr>
        				   	   	  <tr>
        				   	   	     <th class='w60'>备注:</th>
        				   	   	     <td>
        				   	   	        <textarea name="remark" id= ""cols="40" rows="6"><?php echo $en_cons['remark'] ?></textarea>
        				   	   	     </td>
        				   	   	   </tr>
        				   		 
        				   		<tr>
        				   	   	    <th class='w100'><a href="javascript:;" class = 'sort_copy'>同步排序</a>排序</th>
        				   	   		<td>
        				   	   			<input type="text" name="sort" style='width:150px;' value="<?php echo $en_cons['sort'] ?>" />
        				   	   		</td>
        				   	   	 </tr> 

        				   	 <tr>
        				   	    <th class='w100' style="text-indent:-9999px;">操作</th>
        				   		<td>
    				   		        <input type="hidden" name="id" value="<?php echo $en_cons['id'] ?>" />
    				   		    	<input type="button" class="btn1" value="确定提交" onclick="save_form('en_wis_art','<?php echo site_url('contact/contacter_add/en'); ?>')"/>
    				   		    	<input type="reset" class="btn2" value="重置">
        				   		</td>
        				   	 </tr>
        		           </table>
        			   </form>
                    </div>
         </div>
        </div>

	</div>
	<script>
		$(function(){
		  $("form").validate({
		    phone: {
		    message: "多个联系方式 以英文逗号','隔开 如：13811214737（北京）,021-62990259（上海）"
		   },
		    desc: {
		    message: " 请填写概要内容"
		   },
		   sort: {
		    message: " 请输入排序!,默认255,数字越小,优先级越高"
		   },
		   day: {
		   	message: "请输入数字，如：30"
		   }
		 })   
		});
	</script>
</body>
</html>