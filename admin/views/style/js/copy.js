$(function (){
	$('.dx_upload_btn').find('input[type="file"]').live('change', function(){
	    var id = $(this).attr('id');
	    ajaxFileUpload(id);
	});
	//同步一张图片
	 $('#content_en .data_copy').click(function(){
 	con = $("#content_ch input[nctype='file_21']").val();
 	img_add = $("#content_ch input[nctype='file_21']").siblings('img').attr('src');
	if (con.length == 0) {
	    return alert('没有可用于复制的数据');
	}
 	$("#content_en input[nctype='file_22']").val(con);
 	$("#content_en input[nctype='file_22']").siblings('img').attr('src',img_add);
 })

 //同步多张图片
 $('#content_en .list_pic_copy').click(function(){
    list = $("#content_ch .list_pic").find('li');
    var arr = [];
    var pics = [];
 	for (var i = 0; i < list.length; i++) {
 		Con = $("#content_ch .list_pic input[nctype='file_"+i+"']").val();
 		Img = $("#content_ch .list_pic input[nctype='file_"+i+"']").siblings('img').attr('src');
 		Len = arr.push(Con);
 		pic_len = pics.push(Img);
 	};
    for (var j = 5; j < 10; j++) {
    	num = parseInt(j-5);
	  $("#content_en .list_pic input[nctype='file_"+j+"']").val(arr[num]);
 	  $("#content_en .list_pic input[nctype='file_"+j+"']").siblings('img').attr('src',pics[num]);
    };
 })

 //同步排序
 $('#content_en .sort_copy').click(function(){
 	con = $("#content_ch input[name='sort']").val();
	if (con.length == 0) {
	    return alert('没有可用于复制的数据');
	}
 	$("#content_en input[name='sort']").val(con);
 }) 
  $('#content_en .year_copy').click(function(){
    con = $("#content_ch input[name='year']").val();
    if (con.length == 0) {
        return alert('没有可用于复制的数据');
    }
    $("#content_en input[name='year']").val(con);
  }) 
//同步链接
  $('#content_en .link_copy').click(function(){
    con = $("#content_ch input[name='vedio']").val();
    if (con.length == 0) {
        return alert('没有可用于复制的数据');
    }
    $("#content_en input[name='vedio']").val(con);
  })

  $('#content_en .data_copy_tel').click(function(){
    con = $("#content_ch input[name='contract']").val();
    if (con.length == 0) {
        return alert('没有可用于复制的数据');
    }
    $("#content_en input[name='contract']").val(con);
  }) 
  $('#content_en .data_copy_url').click(function(){
    con = $("#content_ch input[name='address_url']").val();
    if (con.length == 0) {
        return alert('没有可用于复制的数据');
    }
    $("#content_en input[name='address_url']").val(con);
  })
 //图片删除
$('a[nctype="del"]').click(function(){
    // path = $(this).parent('div').siblings('div.upload-thumb').find('img').attr('src');
    img_name = $(this).parent('div').siblings('div.upload-thumb').find('img').siblings('input').attr('value');
    if(img_name =='')
    {
        return false;
    }
    var obj = $(this);
    $.ajax({
        type : "POST",
        url : del_img_url,
        cache : false,
        data : {name : img_name},
        timeout : 10000,
        success : function(data) {
        if(data == 1)
        {
           obj.parent('div').siblings('div.upload-thumb').find('img').attr('src','./img/default_goods_image_240.gif').siblings('input').attr('value','');
           $('a[nctype="del"]').eq(0).trigger('click'); 
           $('a[nctype="del"]').eq(1).trigger('click');          
        }
      }
    })

});
 //同步时间
 $('#content_en .time_copy').click(function(){
 	con = $("#content_ch input[name='updatetime']").val();
	if (con.length == 0) {
	    return alert('没有可用于复制的数据');
	}
 	$("#content_en input[name='updatetime']").val(con);
 })

  $('#content_en .week_copy').click(function(){
    con = $("#content_ch input[name='addtime']").val();
    if (con.length == 0) {
        return alert('没有可用于复制的数据');
    }
    $("#content_en input[name='addtime']").val(con);
  })

})
 function ajaxFileUpload(id, o) {
     $('img[nctype="' + id + '"]').attr('src','./img/loading.gif');

     $.ajaxFileUpload({
         url : target_url,
         secureuri : false,
         fileElementId : id,
         dataType : 'json',
         data : {name : id},
         success : function (data, status) {
                     if (typeof(data.error) != 'undefined') {
                         // alert(data.error);
                         $('img[nctype="' + id + '"]').attr('src','./img/loading.gif');
                     } else {
                         $('input[nctype="' + id + '"]').val(data.name);
                         $('img[nctype="' + id + '"]').attr('src', data.thumb_name);
                         //修改 2015,2,19
                         if(data.flag == 'file_21good')
                         {
                            $('input[nctype="file_0"]').val(data.name);
                            $('img[nctype="file_0"]').attr('src', data.thumb_name);
                         }
                         if(data.flag == 'file_23good')
                         {
                            $('input[nctype="file_1"]').val(data.name);
                            $('img[nctype="file_1"]').attr('src', data.thumb_name);
                         }
                     }
                 }
     });
     return false;

 }