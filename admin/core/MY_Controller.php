<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->_check_login();
	}

	//检查是否登陆
	public function _check_login()
	{
		$username = $this->session->userdata('username');
		$uid = $this->session->userdata('uid');
		if(!$username || !$uid) 
		{
			redirect('login');
		}
	}
   
   //error函数
    protected function error($msg = ':(  出错了！',$url = '',$time = 1)
	{
		$url = $url ? "window.location.href='" . site_url($url) . "'" : "window.location.href='".DX_HISTORY."'";
	    $this->load->view('common/error',array('msg' => $msg, 'url' => $url, 'time' => $time));
		echo $this->output->get_output();
		exit();
	}
	//success函数
    protected function success($msg = ':)  操作成功',$url = '',$time = 1)
	{
		$url = $url ? "window.location.href='" . site_url($url) . "'" : "window.location.href='".DX_HISTORY."'";
	    $this->load->view('common/success',array('msg' => $msg, 'url' => $url, 'time' => $time));
		echo $this->output->get_output();
		exit();
	}
	// 	//上传图片
	protected function _upload_img($image,$path = "./uploads/common",$width = '2000',$height = '1000',$allowed_types = 'gif|jpg|png|jpeg')
    {
		 $config['upload_path'] = $path;
		 $config['allowed_types'] = $allowed_types;
		 $config['max_size'] = '10000';
		 $config['file_name'] = time().mt_rand(1000,9999);
		 $config['max_width'] = $width;
         $config['max_height'] = $height;
	     // 载入上传类
	     $this->load->library('upload',$config);
	     $this->upload->do_upload($image);
	     $wrong = $this->upload->display_errors();
		 if($wrong)
		 {
		    show_error($wrong);
		    return FALSE;
		 }
		//返回信息
		return $this->upload->data();
   }
}