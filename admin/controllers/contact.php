<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 联系我们
 */
class Contact extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	//获取栏目
	public function volunteer()
	{
		  //后台设置后缀为空
		$this->config->set_item('url_suffix', '');
		// 载入分页类
		$this->load->library('pagination');
		$perPage = 20;
		//配置项设置
		$config['base_url'] = site_url('contact/volunteer');
		$config['total_rows'] = $this->db->count_all_results('volunteer');
		$config['per_page'] = $perPage;
		$config['uri_segment'] = 3;
		// $config['enable_query_strings'] = TRUE;
		$config['first_link'] = '第一页';
		$config['prev_link'] = '上一页';
		$config['next_link'] = '下一页';
		$config['last_link'] = '最后一页';

		//初始化配置
		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
		$offset = $this->uri->segment(3);
		$this->db->limit($perPage, $offset);

		$data['volunteers'] = $this->db->select('id,name,email,message,tel,updatetime')->from('volunteer')->get()->result_array();
		$this->load->view('contact/volunteer',$data);
	}

    //删除信息
    public function volunteer_del()
    {
	      $id = intval($this->input->post('id'));
		  $flag = $this->db->delete('volunteer',array('id'=>$id));
		  if($flag)
		  {
		    $arr['status']  = 1;
		    $arr['message']  = "删除信息成功 :)";
		  }
		  else
		  {
		     $arr['status']  = 0;
		    $arr['message']  = "操作失败 :(";         
		  } 
		  echo json_encode($arr);
		  exit();
    }

	//留言信息管理
	public function leavemsg()
	{
		  //后台设置后缀为空
		$this->config->set_item('url_suffix', '');
		// 载入分页类
		$this->load->library('pagination');
		$perPage = 20;
		//配置项设置
		$config['base_url'] = site_url('contact/leavemsg');
		$config['total_rows'] = $this->db->count_all_results('message');
		$config['per_page'] = $perPage;
		$config['uri_segment'] = 3;
		// $config['enable_query_strings'] = TRUE;
		$config['first_link'] = '第一页';
		$config['prev_link'] = '上一页';
		$config['next_link'] = '下一页';
		$config['last_link'] = '最后一页';

		//初始化配置
		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
		$offset = $this->uri->segment(3);
		$this->db->limit($perPage, $offset);

		$data['messages'] = $this->db->select('id,lname,lemail,lmsg,ltime')->from('message')->get()->result_array();
		$this->load->view('contact/message',$data);
	}
    public function message_del()
    {
	      $id = intval($this->input->post('id'));
		  $flag = $this->db->delete('message',array('id'=>$id));
		  if($flag)
		  {
		    $arr['status']  = 1;
		    $arr['message']  = "删除信息成功 :)";
		  }
		  else
		  {
		     $arr['status']  = 0;
		    $arr['message']  = "操作失败 :(";         
		  } 
		  echo json_encode($arr);
		  exit();
    }

    //2015/3/17新增  联系人
    public function contacter()
    {
    	$this->load->model('dxdb_model','con','one_contacter');
    	// $this->load->model('dxdb_model','en_con','one_contacter_en');
    	$data['contacters'] = $this->con->all();
    	$this->load->view('contact/contacter',$data);
    }

    //状态
    public function contacter_status()
    {
    	$this->load->model('dxdb_model','con','one_contacter');
    	$this->load->model('dxdb_model','en_con','one_contacter_en');
    	$id = intval($this->input->post('id'));
    	$state = intval($this->input->post('state'));
    	if($state == 1)
    	{
    	     $flag = $this->en_con->dx_update(array('status'=>0),array('id'=>$id));
    	    $flag = $this->con->dx_update(array('status'=>0),array('id'=>$id));
    	    $msg = '操作成功：信息屏蔽!';
    	}
    	else
    	{
    	    $flag = $this->en_con->dx_update(array('status'=>1),array('id'=>$id));
    	    $flag = $this->con->dx_update(array('status'=>1),array('id'=>$id));
    	    $msg = '操作成功：信息状态正常 :)';
    	}
    	if($flag)
    	{
    	  $arr['status']  = 1;
    	  $arr['message']  = $msg;
    	}
    	else
    	{
    	   $arr['status']  = 0;
    	   $arr['message']  = "操作失败 :(";        
    	} 
    	echo json_encode($arr);
    	exit(); 
    }

    //删除
    public function contacter_del()
    {
    	$this->load->model('dxdb_model','con','one_contacter');
    	$this->load->model('dxdb_model','en_con','one_contacter_en');
    	  $id = intval($this->input->post('id'));
    	  $flag = $this->con->dx_delete(array('id'=>$id));
    	  $flag = $this->en_con->dx_delete(array('id'=>$id));
    	  if($flag)
    	  {
    	    $arr['status']  = 1;
    	    $arr['message']  = "删除信息成功 :)";
    	  }
    	  else
    	  {
    	     $arr['status']  = 0;
    	    $arr['message']  = "操作失败 :(";         
    	  } 
    	  echo json_encode($arr);
    	  exit();
    }

    //新曾
    public function contacter_add()
    {
    	$this->load->model('dxdb_model','con','one_contacter');
    	$this->load->model('dxdb_model','en_con','one_contacter_en');
    	if($this->input->is_ajax_request())
    	{
    	  $data = array();//表单内容
          $phone = str_replace('，', ',', $this->input->post('phone'));
    	  $data = array(
    	      // 'name'      => $this->input->post('name'),
    	      'team'      => $this->input->post('team'),
    	      'city'      => $this->input->post('city'),
    	      'phone'     => $phone,
    	      'remark'    => $this->input->post('remark'),
              'wechat'    => $this->input->post('wechat'),
    	      'sort'      => $this->input->post('sort')
    	   );
    	  $id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
    	  $lang = $this->uri->segment(3);
    	  if($id == '')
    	  {
    	      if($lang == 'ch')
    	      {
    	          $flag = $this->con->dx_insert($data);
    	          $flag = $this->en_con->dx_insert(array('sort'=>255));
    	      }
    	      else
    	      {
    	        $flag = $this->en_con->dx_insert($data); 
    	        $flag = $this->con->dx_insert(array('sort'=>255)); 
    	      }
    	      if($flag != false)
    	         $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
    	      else
    	         $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
    	         echo json_encode($arr);
    	      exit(); 
    	  }
    	  else
    	  {
    	        if($lang == 'ch')
    	        {
    	          $flag = $this->con->dx_update($data,array('id'=>$id));
    	        }
    	        else
    	        {
    	          $flag = $this->en_con->dx_update($data,array('id'=>$id));
    	        }
    	       if($flag != false)
    	         $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
    	       else
    	         $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
    	       echo json_encode($arr);
    	       exit();
    	  }
    	}
    	else
    	{
    	  $this->load->view('contact/contacter_add');
    	}
    }

    //编辑
    public function contacter_edit()
    {
        $this->load->model('dxdb_model','con','one_contacter');
        $this->load->model('dxdb_model','en_con','one_contacter_en');
        $id = $this->uri->segment(3);
        $data['cons'] = $this->con->one(array('id'=>$id));
        $data['en_cons'] = $this->en_con->one(array('id'=>$id));
        $this->load->view('contact/contacter_edit',$data);
    }
}