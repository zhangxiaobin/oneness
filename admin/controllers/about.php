<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 走进合一
 */
class About extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
    
    //关于合一
    public function un_desc()
    {
        $data['abouts'] = $this->db->select('id,desc')->from('about')->limit(1)->get()->result_array();
        $this->load->view('about/about',$data);
    }
    
    public function un_desc_edit()
    {
        if($this->input->is_ajax_request())
        {
            $insert_array = array();
            $insert_array = array(
                'title' => $this->input->post('title'),
                'desc'  => $this->input->post('desc')
            );
            $id = $this->input->post('id');//获取添加数据的id值   如果有id  就为编辑  否则为添加
            $lang = $this->uri->segment(3);
            if(trim($id) == '')
            {
                $arr = array('status'=>0,'tid'=>$flag,'message'=>'错误的参数 :(');  
                echo json_encode($arr);
                exit(); 
            }
            else
            {
                 if($lang == 'ch')
                 {
                   $flag = $this->db->update('about',$insert_array,array('id'=>$id));
                 }
                 else
                 {
                   $flag = $this->db->update('en_about',$insert_array,array('id'=>$id));
                 }
                if($flag != false)
                  $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 。:)');    
                else
                  $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 。:(');  
                echo json_encode($arr);
                exit();
            }
        }
        else
        {
           $id = $this->uri->segment(3);
           $data['abouts'] = $this->db->get_where('about',array('id'=>$id))->row_array();
           $data['en_abouts'] = $this->db->get_where('en_about',array('id'=>$id))->row_array();
           $this->load->view('about/about_edit',$data);         
        }

    }
     //校园图片管理
    public function un_pics()
    {
        $this->load->model('dxdb_model','pic','un_pics');
        //获取所有图片
        $data['pics'] = $this->pic->all();
        // p($data['pics']);
        $this->load->view('about/pic',$data);
    }   

    // 图片上传
    public function uni_img_upload() 
    {
        $this->load->model('dxdb_model','pic','un_pics');
        $result = $this->_upload_img("file",'../uploads/pics');
        if($result == FALSE)
        {
            $error = '上传 图片失败';
            $data['state'] = 'false';
            $data['message'] = $error;
            $data['origin_file_name'] = $_FILES["file"]["name"];
            echo json_encode($data);
            exit();
        }
        else
        {
            $crop_img = $result['full_path'];
            $thumb_img = $result['file_path'].$result['raw_name'].'_300_200'.$result['file_ext'];
            thumb($crop_img,$thumb_img, 285, 188, 5);//缩略图

            $pic_path = base_url()."../uploads/pics/".$result['file_name'];
            list($width, $height, $type, $attr) = getimagesize($pic_path);
            $insert_array = array();
            $insert_array['pic_name'] = $result['file_name'];
            $insert_array['pic_cover'] = $pic_path;
            $insert_array['pic_size'] = intval($_FILES['file']['size']);
            $insert_array['pic_spec'] = $width . 'x' . $height;
            $insert_array['upload_time'] = time();
            $result = $this->pic->dx_insert($insert_array); 

            $data = array ();
            $data ['file_path'] = $pic_path;
            $data ['file_name'] = $result['file_name'];
            $data['file_id'] = $result;
            $data['origin_file_name'] = $_FILES["file"]["name"];
            $data['state'] = 'true';
            // 整理为json格式
            echo json_encode($data);
            exit();
        }
    }

    //删除信息
    public function uni_img_del()
    {
        $this->load->model('dxdb_model','pic','un_pics');
        $str = $this->input->post('id');
        $tmp_id = explode('-', $str);
        foreach ($tmp_id as $v) 
        {
            $data = $this->pic->one(array('id'=>$v));
             $img_url = '../uploads/pics/'.$data['pic_name'];
             $thumb_url = '../uploads/pics/'.get_thumb($data['pic_name'],'_300_200');
             @unlink($img_url);@unlink($thumb_url);
            $flag = $this->pic->dx_delete(array('id'=>$v));
        }
        // $id = implode(',',$tmp_id);

        if($flag)
        {
          $arr['status']  = 1;
          $arr['message']  = "删除信息成功 :)";
        }
        else
        {
           $arr['status']  = 0;
          $arr['message']  = "操作失败 :(";         
        } 
        echo json_encode($arr);
        exit();
    }

    
    //走进校园部分
    public function sch_pics()
    {
        $this->load->model('dxdb_model','uni','university');
        $data['uni'] = $this->uni->all();
        $this->load->view('about/university',$data);
    }

    //添加新的校区
    public function sch_pics_add()
    {
        $this->load->model('dxdb_model','uni','university');
        if(IS_POST)
        {
            $pics = $this->input->post('data');
            $tuji = array();
            if(! empty($pics))
            {
                foreach ($pics['tupian'] as $k => $v) {
                    $tuji[] = array('title'=>$pics['title'][$k],'pic'=>$v,'picstat' => $pics['picstat'][$k] );
                }
            }
            $data = array(
                'uname' => $this->input->post('uname'),
                'enuname' => $this->input->post('enuname'),
                'upics' => serialize($tuji),
                'sort' => $this->input->post('sort') 
            );
            $flag = $this->uni->dx_insert($data);
            if($flag) $this->success('添加成功','about/sch_pics');
        }
        else
        {
             $this->load->view('about/university_add'); 
        } 
    }
    //编辑校区
    public function sch_pics_edit()
    {
        $this->load->model('dxdb_model','uni','university');
        $id = $this->uri->segment(3);
        if(IS_POST)
        {
            $pics = $this->input->post('data');
            $tuji = array();
            if(! empty($pics))
            {
                foreach ($pics['tupian'] as $k => $v) {
                    $tuji[] = array('title'=>$pics['title'][$k],'pic'=>$v,'picstat' => $pics['picstat'][$k] );
                }
            }
            $data = array(
                'uname' => $this->input->post('uname'),
                'enuname' => $this->input->post('enuname'),
                'upics' => serialize($tuji),
                'sort' => $this->input->post('sort') 
            );
            $flag = $this->uni->dx_update($data,array('id'=>$id));
            if($flag) $this->success('编辑成功','about/sch_pics');
        }
        else
        {
            $data = $this->uni->one(array('id'=>$id));
             $this->load->view('about/university_edit',$data); 
        }
    }
    //图片上传
    public function sch_pics_upload()
    {
        $result = $this->_upload_img("file",'../uploads/unipics');
        if($result == FALSE)
        {
            $error = '上传 图片失败';
            $data['state'] = 'false';
            $data['message'] = $error;
            $data['origin_file_name'] = $_FILES["file"]["name"];
            echo json_encode($data);
            exit();
        }
        else
        {
            // 缩略图开始
            $crop_img = $result['full_path'];
            $thumb_img = $result['file_path'].$result['raw_name'].'_300_200'.$result['file_ext'];
            $thumb_img2 = $result['file_path'].$result['raw_name'].'_200_200'.$result['file_ext'];
            thumb($crop_img,$thumb_img, 279, 159, 5);//缩略图
            thumb($crop_img,$thumb_img2, 255, 235, 5);//缩略图
            // ----end
            $pic_path = base_url()."../uploads/unipics/".$result['file_name'];
            $data = array ();
            $data ['file_path'] = $pic_path;
            $data ['file_name'] = $result['file_name'];
            $data['origin_file_name'] = $_FILES["file"]["name"];
            $data['state'] = 'true';
            // 整理为json格式
            echo json_encode($data);
            exit();
        }    
    }
    public function upload_img_remove()
    {
        $tmp_url = $this->input->post('file');
        $img_url = '../uploads/unipics/'.$tmp_url;
        $thumb1 = '../uploads/unipics/'.get_thumb($tmp_url,'_300_200');
        $thumb2 = '../uploads/unipics/'.get_thumb($tmp_url,'_200_200');
        @unlink($thumb1);@unlink($thumb2);
        if(! file_exists($img_url))
        {
          echo 1;exit();
        } 
        if(@unlink($img_url))
        {
            echo 1;exit();
        }
        else
        {
           echo 1;exit();
        }

    }
    //校区状态编辑
    public function sch_pics_status()
    {
         $this->load->model('dxdb_model','uni','university');
        $id = intval($this->input->post('id'));
        $state = intval($this->input->post('state'));
        if($state == 1)
        {
            $flag = $this->uni->dx_update(array('status'=>0),array('id'=>$id));$msg = '操作成功：信息屏蔽!';
        }
        else
        {
            $flag = $this->uni->dx_update(array('status'=>1),array('id'=>$id));$msg = '操作成功：信息状态正常 :)';
        }
        if($flag) {$arr['status']  = 1;$arr['message']  = $msg;}else { $arr['status']  = 0;$arr['message']  = "操作失败 :(";} 
        echo json_encode($arr);
        exit();  
    }
    //删除校区信息
    public function sch_pics_del()
    {
        $this->load->model('dxdb_model','uni','university');
        $id = intval($this->input->post('id'));
        //删除图片
        $flag = $this->uni->dx_delete(array('id'=>$id));
        if($flag)
        {
          $arr['status']  = 1;
          $arr['message']  = "删除信息成功 :)";
        }
        else
        {
           $arr['status']  = 0;
          $arr['message']  = "操作失败 :(";         
        } 
        echo json_encode($arr);
        exit();
    }
}