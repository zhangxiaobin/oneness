<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 目录部分
 */
class Course extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	//合一课程
	public function index()
	{
		$this->load->model('dxdb_model','cou','one_course');
    // $data['courses'] = $this->cou->all();
		$tdata = $this->cou->all(array(),array(0=>"id"));
    $data['courses'] = limitless($tdata);
		$this->load->view('courses/course',$data);
	}
    
    public function course_add()
    {
       $this->load->model('dxdb_model','cou','one_course');
       $this->load->model('dxdb_model','en_cou','one_course_en');	
       if($this->input->is_ajax_request())
       {
       	$data = array();//表单内容
       	$tmp_imgs = $this->input->post('image');
       	 //如果图片为空，那么返回错误
       	// if(empty($tmp_imgs))
       	// {
       	//    $arr = array('status'=>0,'message'=>'请上传图片, 图片为必须!  :(');  
       	//    echo json_encode($arr);
       	//    exit();    
       	// }
       	$data = array(
            'pid'       => $this->input->post('pid'),
       	    'title'     => $this->input->post('title'),
       	    'desc'      => $this->input->post('desc'),
       	    'day'       => $this->input->post('day'),
       	    'image'     => $tmp_imgs,
       	    'content'   => $this->input->post('content'),
       	    'sort'      => $this->input->post('sort')
       	 );
       	$id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
       	$lang = $this->uri->segment(3);
       	if($id == '')
       	{
       	    if($lang == 'ch')
       	    {
       	        $flag = $this->cou->dx_insert($data);
       	        $flag = $this->en_cou->dx_insert(array('sort'=>$this->input->post('sort'),'pid'=> $this->input->post('pid')));
       	    }
       	    else
       	    {
       	    	$flag = $this->en_cou->dx_insert($data); 
       	    	$flag = $this->cou->dx_insert(array('sort'=>$this->input->post('sort'),'pid'=> $this->input->post('pid')));
       	    }
       	    if($flag != false)
       	       $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
       	    else
       	       $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
       	       echo json_encode($arr);
       	    exit(); 
       	}
       	else
       	{
       	      if($lang == 'ch')
       	      {
       	        $flag = $this->cou->dx_update($data,array('id'=>$id));
       	      }
       	      else
       	      {
       	        $flag = $this->en_cou->dx_update($data,array('id'=>$id));
       	      }
       	     if($flag != false)
       	       $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
       	     else
       	       $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
       	     echo json_encode($arr);
       	     exit();
       	}
       }
       else
       {
        $data = array();
        $tmpdata = $this->cou->all(array(),array(),'id,pid,title');
        $data['pid'] = 0;
        $data['cours'] = limitless($tmpdata);
       	$this->load->view('courses/course_add',$data);
       }
    }
    public function course_edit()
    {
       $this->load->model('dxdb_model','cou','one_course');
       $this->load->model('dxdb_model','en_cou','one_course_en');	 
       $id = $this->uri->segment(3);
       $data = array();
       $tmpdata = $this->cou->all(array(),array(),'id,pid,title');
      
       $data['cours'] = limitless($tmpdata);
       $data['cou'] = $this->cou->one(array('id'=>$id));
        $data['pid'] =  $data['cou']['pid'];
       $data['en_cou'] = $this->en_cou->one(array('id'=>$id));
       $this->load->view('courses/course_edit',$data);	
    }
    //状态修改
    public function course_status()
    {
    	$this->load->model('dxdb_model','cou','one_course');
    	$this->load->model('dxdb_model','en_cou','one_course_en');	
		$id = intval($this->input->post('id'));
		$state = intval($this->input->post('state'));
		if($state == 1)
		{
		     $flag = $this->en_cou->dx_update(array('status'=>0),array('id'=>$id));
		    $flag = $this->cou->dx_update(array('status'=>0),array('id'=>$id));
		    $msg = '操作成功：信息屏蔽!';
		}
		else
		{
		    $flag = $this->en_cou->dx_update(array('status'=>1),array('id'=>$id));
		    $flag = $this->cou->dx_update(array('status'=>1),array('id'=>$id));
		    $msg = '操作成功：信息状态正常 :)';
		}
		if($flag)
		{
		  $arr['status']  = 1;
		  $arr['message']  = $msg;
		}
		else
		{
		   $arr['status']  = 0;
		   $arr['message']  = "操作失败 :(";        
		} 
		echo json_encode($arr);
		exit();   	
    }
    //ajax图片上传
    public function wis_img_upload()
    {
    	$image = $_POST['name'];//"goods_image"
    	$image_path = '../uploads/course';//图片路径
    	$info = $this->_upload_img($image,$image_path);

    	//缩略图设置   start
    	$crop_img = $info['full_path'];
    	$thumb_img = $info['file_path'].$info['raw_name'].'_1000_500'.$info['file_ext'];
    	thumb($crop_img,$thumb_img, 1034, 449, 5);//缩略图
    	//缩略图结束  end

    	$data = array ();
    	$data ['thumb_name'] = base_url()."../uploads/course/".$info['file_name'];
    	$data ['name']      = $info['file_name'];
    	 
    	// 整理为json格式
    	echo json_encode($data);
    	exit();
    }
    public function wis_img_del()
    {
    	$image = trim($_POST['name']);//"goods_image"
        $img_url = '../uploads/course/'.$image;
        $img = pathinfo($img_url);
        $thumb_url = '../uploads/course/'.$img['filename'].'_1000_500.'.$img['extension'];
    	@unlink($img_url); 
    	@unlink($thumb_url); 
    	// 整理为json格式
    	echo 1;
    	exit();
    }
	
  //合一课程安排  =============================================  start
    public function schedule()
    {
    	$this->load->model('dxdb_model','sch','one_schedule');
      $data['schs'] = $this->sch->all(array(),array(0=>"id"));
      $this->load->view('courses/schedule',$data);
    }

    //添加
    public function schdule_add()
    {
      $this->load->model('dxdb_model','sch','one_schedule');
      $this->load->model('dxdb_model','en_sch','one_schedule_en');

      if($this->input->is_ajax_request())
      {
        $data = array();//表单内容
        $year = @intval($this->input->post('year'));
        $schs = serialize($this->input->post('con'));
        $data = array(
          'year'=>$year,
          'con' =>$schs
          );
        $id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
        $lang = $this->uri->segment(3);
        if($id == '')
        {
            if($lang == 'ch')
            {
                $flag = $this->sch->dx_insert($data);
                $flag = $this->en_sch->dx_insert(array('year'=>$this->input->post('year')));
            }
            else
            {
              $flag = $this->en_sch->dx_insert($data); 
              $flag = $this->sch->dx_insert(array('year'=>$this->input->post('year'))); 
            }
            if($flag != false)
               $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
            else
               $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
               echo json_encode($arr);
            exit(); 
        }
        else
        {
              if($lang == 'ch')
              {
                $flag = $this->sch->dx_update($data,array('id'=>$id));
              }
              else
              {
                $flag = $this->en_sch->dx_update($data,array('id'=>$id));
              }
             if($flag != false)
               $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
             else
               $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
             echo json_encode($arr);
             exit();
        }
      }
      else
      {
        $this->load->model('dxdb_model','cou','one_course');
        $this->load->model('dxdb_model','en_cou','one_course_en');
        $data['courses'] = $this->cou->all(array(),array(0=>"id"));
        $data['en_courses'] = $this->en_cou->all(array(),array(0=>"id"));
        $this->load->view('courses/schdule_add',$data);
      }
    }
    
    //编辑
    public function schedule_edit()
    {
      $this->load->model('dxdb_model','sch','one_schedule');
      $this->load->model('dxdb_model','en_sch','one_schedule_en'); 
      //所有课程
        $this->load->model('dxdb_model','cou','one_course');
        $this->load->model('dxdb_model','en_cou','one_course_en');
        $data['courses'] = $this->cou->all(array(),array(0=>"id"));
        $data['en_courses'] = $this->en_cou->all(array(),array(0=>"id"));

      $id = $this->uri->segment(3);
      $data['cou'] = $this->sch->one(array('id'=>$id));
      $data['en_cou'] = $this->en_sch->one(array('id'=>$id));
      $this->load->view('courses/schdule_edit',$data);   
    }

    //课程表删除
    public function schedule_del()
    {
      $this->load->model('dxdb_model','sch','one_schedule');
      $this->load->model('dxdb_model','en_sch','one_schedule_en');
      $id = intval($this->input->post('id'));
      $flag = $this->sch->dx_delete(array('id'=>$id));
      $flag = $this->en_sch->dx_delete(array('id'=>$id));
      if($flag)
      {
        $arr['status']  = 1;
        $arr['message']  = "删除信息成功 :)";
      }
      else
      {
         $arr['status']  = 0;
        $arr['message']  = "操作失败 :(";         
      } 
      echo json_encode($arr);
      exit();
    }

    public function course_del()
    {
       $this->load->model('dxdb_model','cou','one_course');
       $this->load->model('dxdb_model','en_cou','one_course_en');  
      $id = intval($this->input->post('id'));
      $flag = $this->cou->dx_delete(array('id'=>$id));
      $flag = $this->en_cou->dx_delete(array('id'=>$id));
      if($flag)
      {
        $arr['status']  = 1;
        $arr['message']  = "删除信息成功 :)";
      }
      else
      {
         $arr['status']  = 0;
        $arr['message']  = "操作失败 :(";         
      } 
      echo json_encode($arr);
      exit();    
    }
}