<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 合一智慧
 */
class Wisdom extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
        // $this->load->model('dxdb_model','pic','un_pics');
	}

	//合一智慧(art)
	public function index()
	{
		$this->load->model('dxdb_model','art','one_wisdom_art');
          //后台设置后缀为空
        $this->config->set_item('url_suffix', '');
        // 载入分页类
        $this->load->library('pagination');
        $perPage = 20;
        //配置项设置
        $config['base_url'] = site_url('wisdom/wis_art');
        $config['total_rows'] = $this->art->dx_count();
        $config['per_page'] = $perPage;
        $config['uri_segment'] = 3;
        $config['first_link'] = '第一页';
        $config['prev_link'] = '上一页';
        $config['next_link'] = '下一页';
        $config['last_link'] = '最后一页';
        //初始化配置
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        $offset = $this->uri->segment(3);
        $this->db->limit($perPage, $offset);
		$data['weeks'] = $this->art->all();
		$this->load->view('wisdom/wis_art',$data);
	}
    
    //合一智慧添加
    public function wis_art_add()
    {
    	$this->load->model('dxdb_model','art','one_wisdom_art');
    	$this->load->model('dxdb_model','en_art','one_wisdom_art_en');
    	if($this->input->is_ajax_request())
    	{
    		$data = array();//表单内容
    		$tmp_imgs = $this->input->post('image');
    		 //如果图片为空，那么返回错误
    		if(empty($tmp_imgs))
    		{
    		   $arr = array('status'=>0,'message'=>'请上传图片, 图片为必须!  :(');  
    		   echo json_encode($arr);
    		   exit();    
    		}
    		$addtime = strtotime($this->input->post('updatetime'));
    		$data = array(
    		    'title'     => $this->input->post('title'),
    		    'desc'      => $this->input->post('desc'),
    		    'image'     => $tmp_imgs,
    		    'content'   => $this->input->post('content'),
    		    'vedio'     => serialize(htmlspecialchars($this->input->post('vedio'))),
    		    'updatetime'=> $addtime,
    		    'type'      => $this->input->post('type'),
    		    'sort'      => $this->input->post('sort')
    		 );
    		$id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
    		$lang = $this->uri->segment(3);
    		if($id == '')
    		{
    		    if($lang == 'ch')
    		    {
    		        $flag = $this->art->dx_insert($data);
    		        $flag = $this->en_art->dx_insert(array('type'=>$this->input->post('type')));
    		    }
    		    else
    		    {
    		    	$flag = $this->en_art->dx_insert($data); 
    		    	$flag = $this->art->dx_insert(array('type'=>$this->input->post('type'))); 
    		    }
    		    if($flag != false)
    		       $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
    		    else
    		       $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
    		       echo json_encode($arr);
    		    exit(); 
    		}
    		else
    		{
    		      if($lang == 'ch')
    		      {
    		        $flag = $this->art->dx_update($data,array('id'=>$id));
    		      }
    		      else
    		      {
    		        $flag = $this->en_art->dx_update($data,array('id'=>$id));
    		      }
    		     if($flag != false)
    		       $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
    		     else
    		       $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
    		     echo json_encode($arr);
    		     exit();
    		}
    	}
    	else
    	{
    		$this->load->view('wisdom/wis_art_add');
    	}
    }

    //合一智慧文章编辑
    public function wis_art_edit()
    {
        $this->load->model('dxdb_model','art','one_wisdom_art');
        $this->load->model('dxdb_model','en_art','one_wisdom_art_en');
        $id = $this->uri->segment(3);
        $data['arts'] = $this->art->one(array('id'=>$id));
        $data['en_arts'] = $this->en_art->one(array('id'=>$id));
        $this->load->view('wisdom/wis_art_edit',$data);
    }

    //删除
    public function wis_art_del()
    {
        $this->load->model('dxdb_model','art','one_wisdom_art');
        $this->load->model('dxdb_model','en_art','one_wisdom_art_en');
        $id = intval($this->input->post('id'));
        $flag = $this->art->dx_delete(array('id'=>$id));
        $flag = $this->en_art->dx_delete(array('id'=>$id));
        if($flag)
        {
          $arr['status']  = 1;
          $arr['message']  = "删除信息成功 :)";
        }
        else
        {
           $arr['status']  = 0;
          $arr['message']  = "操作失败 :(";         
        } 
        echo json_encode($arr);
        exit();
    } 

    public function wis_img_upload()
    {
    	$image = $_POST['name'];//"goods_image"
    	$image_path = '../uploads/wisdom';//图片路径
    	$info = $this->_upload_img($image,$image_path);

    	//缩略图设置   start
    	$crop_img = $info['full_path'];
    	$thumb_img = $info['file_path'].$info['raw_name'].'_350_244'.$info['file_ext'];
    	thumb($crop_img,$thumb_img, 350, 244, 5);//缩略图
    	//缩略图结束  end

    	$data = array ();
    	$data ['thumb_name'] = base_url()."../uploads/wisdom/".$info['file_name'];
    	$data ['name']      = $info['file_name'];
    	 
    	// 整理为json格式
    	echo json_encode($data);
    	exit();
    }
    public function wis_img_del()
    {
    	$image = trim($_POST['name']);//"goods_image"
        $img_url = '../uploads/wisdom/'.$image;
        $img = pathinfo($img_url);
        $thumb_url = '../uploads/wisdom/'.$img['filename'].'_350_244.'.$img['extension'];
    	@unlink($img_url); 
    	@unlink($thumb_url); 
    	// 整理为json格式
    	echo 1;
    	exit();
    }

    public function wis_art_status()
    {
    	$this->load->model('dxdb_model','art','one_wisdom_art');
    	$this->load->model('dxdb_model','en_art','one_wisdom_art_en');
		$id = intval($this->input->post('id'));
		$state = intval($this->input->post('state'));
		if($state == 1)
		{
		     $flag = $this->en_art->dx_update(array('status'=>0),array('id'=>$id));
		    $flag = $this->art->dx_update(array('status'=>0),array('id'=>$id));
		    $msg = '操作成功：信息屏蔽!';
		}
		else
		{
		    $flag = $this->en_art->dx_update(array('status'=>1),array('id'=>$id));
		    $flag = $this->art->dx_update(array('status'=>1),array('id'=>$id));
		    $msg = '操作成功：信息状态正常 :)';
		}
		if($flag)
		{
		  $arr['status']  = 1;
		  $arr['message']  = $msg;
		}
		else
		{
		   $arr['status']  = 0;
		   $arr['message']  = "操作失败 :(";        
		} 
		echo json_encode($arr);
		exit();   	
    }
	//温馨每周 =========================================================================
	public function week()
	{
		$this->load->model('dxdb_model','week','week_words');
          //后台设置后缀为空
        $this->config->set_item('url_suffix', '');
        // 载入分页类
        $this->load->library('pagination');
        $perPage = 20;
        //配置项设置
        $config['base_url'] = site_url('wisdom/week');
        $config['total_rows'] = $this->week->dx_count();
        $config['per_page'] = $perPage;
        $config['uri_segment'] = 3;
        $config['first_link'] = '第一页';
        $config['prev_link'] = '上一页';
        $config['next_link'] = '下一页';
        $config['last_link'] = '最后一页';
        //初始化配置
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        $offset = $this->uri->segment(3);
        $this->db->limit($perPage, $offset);
		$data['weeks'] = $this->week->all();
		$this->load->view('wisdom/week',$data);	
	}

	//添加
	public function week_add()
	{
		$this->load->model('dxdb_model','week','week_words');
		$this->load->model('dxdb_model','en_week','week_words_en');
		if($this->input->is_ajax_request())
		{
		    $data = array();//获取表单内容
		    $addtime = strtotime($this->input->post('addtime'));//获取当前时间
		    $data = array(
		        'content' => $this->input->post('content'),
		        'addtime'  => $addtime
		     );
		    $id = $this->input->post('id');//获取添加数据的id值   如果有id  就为编辑  否则为添加
		    $lang = $this->uri->segment(3);
		    if($id == '')
		    {
		        if($lang == 'ch')
		        {
		          $flag = $this->week->dx_insert($data);
		          $flag = $this->en_week->dx_insert(array('addtime'=>$addtime));
		        }
		        else
		        {
                  $flag = $this->en_week->dx_insert($data); 
                  $flag = $this->week->dx_insert(array('addtime'=>$addtime));      
		        } 
		        if($flag != false)
		           $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
		        else
		           $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
		           echo json_encode($arr);
		        exit();                
		    }
		    else
		    {
		      if($lang == 'ch')
		      {
		        $flag = $this->week->dx_update($data,array('id'=>$id));
		      }
		      else
		      {
		        $flag = $this->en_week->dx_update($data,array('id'=>$id));
		      }
		     if($flag != false)
		       $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
		    else
		       $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 :(');  
		      echo json_encode($arr);exit();
		    }


		}
		else
		{
			$this->load->view('wisdom/week_add');
		}	
	}

	//温馨每周编辑
	public function week_edit()
	{
		$this->load->model('dxdb_model','week','week_words');
		$this->load->model('dxdb_model','en_week','week_words_en');
		$id = $this->uri->segment(3);
		$data['weeks'] = $this->week->one(array('id'=>$id));
		$data['en_weeks'] = $this->en_week->one(array('id'=>$id));
		// p($data);
		$this->load->view('wisdom/week_edit',$data);
	}

	//温馨每周删除
	public function week_del()
	{
		$this->load->model('dxdb_model','week','week_words');
		$this->load->model('dxdb_model','en_week','week_words_en');
		$id = intval($this->input->post('id'));
		$flag = $this->week->dx_delete(array('id'=>$id));
		//删除英文信息
				$flag = $this->en_week->dx_delete(array('id'=>$id));
		if($flag)
		   $arr = array('status'=>1,'message'=>'成功删除信息 :)');    
		else
		   $arr = array('status'=>0,'message'=>'操作失败 :(');  
		   echo json_encode($arr);
		exit();
	}
}