<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 */

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$table = $this->db->dbprefix('node');
		$uid = $this->session->userdata('uid');	
		$sql = "SELECT n.nid,n.title FROM {$table} AS n WHERE n.state=1 AND n.pid=0";
		$data['node'] = $this->db->query($sql)->result_array();
		$this->load->view('admin/index',$data);
	}
   

    /**
     * 获取菜单
     */
    public function get_child_menu()
    {
    	$pid = $this->input->get('pid');
    	$table = $this->db->dbprefix('node');
        $showMenuData = $this->db->query("SELECT * FROM {$table} AS n WHERE n.state=1")->result_array();
        $childMenuData = channelLevel($showMenuData, $pid, '', 'nid');
        $html = "<div class='nid_$pid'>";
        foreach ($childMenuData as $menu) {
            $html .= "<dl><dt>" . $menu['title'] . "</dt>";
            foreach ($menu['data'] as $linkMenu) {
                $param = $linkMenu['param'] ? '/' . $linkMenu['param'] : '';
                    $url = base_url()."index.php/".$linkMenu['control']."/".$linkMenu['method'].$param;
                $html .= "<dd><a nid='" . $linkMenu["nid"] . "'
                    onclick='get_content(this," . $linkMenu["nid"] . ")' url='" . $url . "'>" . $linkMenu['title'] . "</a></dd>";
            }
            $html .= "</dl>";
        }
        $html .= "</div>";
       echo $html;

    	exit();
    }
    /**
     * 欢迎页面
     */
    public function xiao()
	{
		$this->load->view('admin/welcome');
	}

	/**
	 * 密码修改
	 */

	public function change_pass()
	{
		if(IS_POST)
		{
			$new_pwd = $this->input->post('newPwd');
			$data = array(
				'password' => md5($new_pwd)
			);
			$uid = $this->session->userdata('uid');
			$flag = $this->db->update('admin', $data, array('uid'=>$uid));
			if($flag)
			{
				$this->session->sess_destroy();
				// $this->success('修改密码成功','login');
				echo "<script>alert('修改密码成功');top.location.reload();</script>";				
			}
			else
			{
				$this->error('修改密码不成功');
			}
		}
		else
		{
			$this->load->view('admin/change_pwd');
		}

	}

	/**
	 * check this password
	 */
	public function check_password()
	{
		if ($this->input->is_ajax_request()) 
		{
			$old = trim($this->input->post('oldPwd'));
			$uid = $this->session->userdata('uid');
			$info = $this->db->select('password')->from('admin')->where(array('uid'=>$uid))->get()->row();
			if(md5($old) == $info->password)
			    echo 1;
			 else
			    echo 0;
			 exit();
		}
		else
		{
			echo 0;
			exit();
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */