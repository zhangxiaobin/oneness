<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 新闻、公告
 */
class News extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
	}

    //获取所有公告
    public function notice()
    {
       $this->load->model('dxdb_model','not','one_notice');
         //后台设置后缀为空
       $this->config->set_item('url_suffix', '');
       // 载入分页类
       $this->load->library('pagination');
       $perPage = 20;
       //配置项设置
       $config['base_url'] = site_url('wisdom/wis_not');
       $config['total_rows'] = $this->not->dx_count();
       $config['per_page'] = $perPage;
       $config['uri_segment'] = 3;
       $config['first_link'] = '第一页';
       $config['prev_link'] = '上一页';
       $config['next_link'] = '下一页';
       $config['last_link'] = '最后一页';
       //初始化配置
       $this->pagination->initialize($config);
       $data['links'] = $this->pagination->create_links();
       $offset = $this->uri->segment(3);
       $this->db->limit($perPage, $offset);
       $data['notices'] = $this->not->all();
       $this->load->view('news/notice',$data);
    }
    
    //添加
    public function notice_add()
    {
    	$this->load->model('dxdb_model','not','one_notice');
    	$this->load->model('dxdb_model','en_not','one_notice_en');
    	if($this->input->is_ajax_request())
    	{
    		$data = array();//表单内容
    		$addtime = strtotime($this->input->post('updatetime'));
    		$data = array(
    		    'title'     => $this->input->post('title'),
    		    'desc'      => $this->input->post('desc'),
    		    'content'   => $this->input->post('content'),
            'tag'       => $this->input->post('tag'),
    		    'vedio'     => serialize(htmlspecialchars($this->input->post('vedio'))),
            'updatetime'=> $addtime,
            'type'      => $this->input->post('type'),
    		    'sort'      => $this->input->post('sort')
    		 );
    		$id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
    		$lang = $this->uri->segment(3);
    		if($id == '')
    		{
    		    if($lang == 'ch')
    		    {
    		        $flag = $this->not->dx_insert($data);
    		        $flag = $this->en_not->dx_insert(array('type'=>$this->input->post('type')));
    		    }
    		    else
    		    {
    		    	$flag = $this->en_not->dx_insert($data); 
    		    	$flag = $this->not->dx_insert(array('type'=>$this->input->post('type'))); 
    		    }
    		    if($flag != false)
    		       $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
    		    else
    		       $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
    		       echo json_encode($arr);
    		    exit(); 
    		}
    		else
    		{
    		      if($lang == 'ch')
    		      {
    		        $flag = $this->not->dx_update($data,array('id'=>$id));
    		      }
    		      else
    		      {
    		        $flag = $this->en_not->dx_update($data,array('id'=>$id));
    		      }
    		     if($flag != false)
    		       $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
    		     else
    		       $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
    		     echo json_encode($arr);
    		     exit();
    		}
    	}
    	else
    	{
    		$this->load->view('news/notice_add');
    	}
    }

    public function notice_edit()
    {
      $this->load->model('dxdb_model','not','one_notice');
      $this->load->model('dxdb_model','en_not','one_notice_en');
      $id = $this->uri->segment(3);
      $data['nots'] = $this->not->one(array('id'=>$id));
      $data['en_nots'] = $this->en_not->one(array('id'=>$id));
      $this->load->view('news/notice_edit',$data);
    }

    //修改状态
    public function notice_status()
    {
    	$this->load->model('dxdb_model','not','one_notice');
    	$this->load->model('dxdb_model','en_not','one_notice_en');
  		$id = intval($this->input->post('id'));
  		$state = intval($this->input->post('state'));
  		if($state == 1)
  		{
  		     $flag = $this->en_not->dx_update(array('status'=>0),array('id'=>$id));
  		    $flag = $this->not->dx_update(array('status'=>0),array('id'=>$id));
  		    $msg = '操作成功：信息屏蔽!';
  		}
  		else
  		{
  		    $flag = $this->en_not->dx_update(array('status'=>1),array('id'=>$id));
  		    $flag = $this->not->dx_update(array('status'=>1),array('id'=>$id));
  		    $msg = '操作成功：信息状态正常 :)';
  		}
  		if($flag)
  		{
  		  $arr['status']  = 1;
  		  $arr['message']  = $msg;
  		}
  		else
  		{
  		   $arr['status']  = 0;
  		   $arr['message']  = "操作失败 :(";        
  		} 
  		echo json_encode($arr);
  		exit(); 
    }

    //删除
    public function notice_del()
    {
      $this->load->model('dxdb_model','not','one_notice');
      $this->load->model('dxdb_model','en_not','one_notice_en');
      $id = intval($this->input->post('id'));
      $flag = $this->not->dx_delete(array('id'=>$id));
      $flag = $this->en_not->dx_delete(array('id'=>$id));
      if($flag)
      {
        $arr['status']  = 1;
        $arr['message']  = "删除信息成功 :)";
      }
      else
      {
         $arr['status']  = 0;
        $arr['message']  = "操作失败 :(";         
      } 
      echo json_encode($arr);
      exit();
    }


//查看所有新闻 =================================================================================================
    public function onew()
    {
      $this->load->model('dxdb_model','news','one_news');
        //后台设置后缀为空
      $this->config->set_item('url_suffix', '');
      // 载入分页类
      $this->load->library('pagination');
      $perPage = 20;
      //配置项设置
      $config['base_url'] = site_url('wisdom/onew');
      $config['total_rows'] = $this->news->dx_count();
      $config['per_page'] = $perPage;
      $config['uri_segment'] = 3;
      $config['first_link'] = '第一页';
      $config['prev_link'] = '上一页';
      $config['next_link'] = '下一页';
      $config['last_link'] = '最后一页';
      //初始化配置
      $this->pagination->initialize($config);
      $data['links'] = $this->pagination->create_links();
      $offset = $this->uri->segment(3);
      $this->db->limit($perPage, $offset);
      $data['notices'] = $this->news->all();
      $this->load->view('news/news',$data);
    }
      //添加
    public function news_add()
      {
        $this->load->model('dxdb_model','news','one_news');
        $this->load->model('dxdb_model','en_news','one_news_en');
        if($this->input->is_ajax_request())
        {
          $data = array();//表单内容
          $addtime = strtotime($this->input->post('updatetime'));
          $data = array(
              'title'     => $this->input->post('title'),
              'desc'      => $this->input->post('desc'),
              'content'   => $this->input->post('content'),
              'tag'       => $this->input->post('tag'),
              'vedio'     => serialize(htmlspecialchars($this->input->post('vedio'))),
              'updatetime'=> $addtime,
              'type'      => $this->input->post('type'),
              'sort'      => $this->input->post('sort')
           );
          $id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
          $lang = $this->uri->segment(3);
          if($id == '')
          {
              if($lang == 'ch')
              {
                  $flag = $this->news->dx_insert($data);
                  $flag = $this->en_news->dx_insert(array('type'=>$this->input->post('type')));
              }
              else
              {
                $flag = $this->en_news->dx_insert($data); 
                $flag = $this->news->dx_insert(array('type'=>$this->input->post('type'))); 
              }
              if($flag != false)
                 $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
              else
                 $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
                 echo json_encode($arr);
              exit(); 
          }
          else
          {
                if($lang == 'ch')
                {
                  $flag = $this->news->dx_update($data,array('id'=>$id));
                }
                else
                {
                  $flag = $this->en_news->dx_update($data,array('id'=>$id));
                }
               if($flag != false)
                 $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
               else
                 $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
               echo json_encode($arr);
               exit();
          }
        }
        else
        {
          $this->load->view('news/news_add');
        }
      }

      public function news_edit()
      {
        $this->load->model('dxdb_model','news','one_news');
        $this->load->model('dxdb_model','en_news','one_news_en');
        $id = $this->uri->segment(3);
        $data['nots'] = $this->news->one(array('id'=>$id));
        $data['en_nots'] = $this->en_news->one(array('id'=>$id));
        $this->load->view('news/news_edit',$data);
      }

      //修改状态
      public function news_status()
      {
        $this->load->model('dxdb_model','news','one_news');
        $this->load->model('dxdb_model','en_news','one_news_en');
        $id = intval($this->input->post('id'));
        $state = intval($this->input->post('state'));
        if($state == 1)
        {
             $flag = $this->en_news->dx_update(array('status'=>0),array('id'=>$id));
            $flag = $this->news->dx_update(array('status'=>0),array('id'=>$id));
            $msg = '操作成功：信息屏蔽!';
        }
        else
        {
            $flag = $this->en_news->dx_update(array('status'=>1),array('id'=>$id));
            $flag = $this->news->dx_update(array('status'=>1),array('id'=>$id));
            $msg = '操作成功：信息状态正常 :)';
        }
        if($flag)
        {
          $arr['status']  = 1;
          $arr['message']  = $msg;
        }
        else
        {
           $arr['status']  = 0;
           $arr['message']  = "操作失败 :(";        
        } 
        echo json_encode($arr);
        exit(); 
      }

      //删除
      public function news_del()
      {
        $this->load->model('dxdb_model','news','one_news');
        $this->load->model('dxdb_model','en_news','one_news_en');
        $id = intval($this->input->post('id'));
        $flag = $this->news->dx_delete(array('id'=>$id));
        $flag = $this->en_news->dx_delete(array('id'=>$id));
        if($flag)
        {
          $arr['status']  = 1;
          $arr['message']  = "删除信息成功 :)";
        }
        else
        {
           $arr['status']  = 0;
          $arr['message']  = "操作失败 :(";         
        } 
        echo json_encode($arr);
        exit();
      }    
}