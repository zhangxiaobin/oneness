<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 目录部分
 */
class Share extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}
    
    //学员分享
    public function index()
    {
        $this->load->model('dxdb_model','stu','one_stu_share');
          //后台设置后缀为空
        $this->config->set_item('url_suffix', '');
        // 载入分页类
        $this->load->library('pagination');
        $perPage = 20;
        //配置项设置
        $config['base_url'] = site_url('share/index');
        $config['total_rows'] = $this->stu->dx_count();
        $config['per_page'] = $perPage;
        $config['uri_segment'] = 3;
        $config['first_link'] = '第一页';
        $config['prev_link'] = '上一页';
        $config['next_link'] = '下一页';
        $config['last_link'] = '最后一页';
        //初始化配置
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        $offset = $this->uri->segment(3);
        $this->db->limit($perPage, $offset);
        $data['weeks'] = $this->stu->all();
        $this->load->view('share/stu_share',$data);
    }
//添加
    public function share_add()
    {
      $this->load->model('dxdb_model','stu','one_stu_share');
      $this->load->model('dxdb_model','en_stu','one_stu_share_en');
      if($this->input->is_ajax_request())
      {
        $data = array();//表单内容
        $tmp_imgs = $this->input->post('image');
        $pimages = $this->input->post('pimage');
         //如果图片为空，那么返回错误
        if(empty($tmp_imgs))
        {
           $arr = array('status'=>0,'message'=>'请上传图片, 图片为必须!  :(');  
           echo json_encode($arr);
           exit();    
        }
        $addtime = strtotime($this->input->post('updatetime'));
        $lang = $this->uri->segment(3);
        if($lang == 'ch')
        {
          $blongs = $this->input->post('blongs');
        }
        else
        {
          $blongs = $this->input->post('blongs');
          $blongs = $blongs == '学员分享' ? 'shares' : 'wisdom';
        }
        $data = array(
            'title'     => $this->input->post('title'),
            'desc'      => $this->input->post('desc'),
            'image'     => $tmp_imgs,
            'content'   => $this->input->post('content'),
            'vedio'     => serialize(htmlspecialchars($this->input->post('vedio'))),
            'updatetime'=> $addtime,
            'type'      => $this->input->post('type'),
            'name'      => $this->input->post('name'),
            'blongs'    => $blongs,
            'pimage'    => $pimages,
            'sort'      => $this->input->post('sort')
         );
        $id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加

        if($id == '')
        {
            if($lang == 'ch')
            {
                $flag = $this->stu->dx_insert($data);
                $flag = $this->en_stu->dx_insert(array('sort'=>255));
            }
            else
            {
              $flag = $this->en_stu->dx_insert($data); 
              $flag = $this->stu->dx_insert(array('sort'=>255)); 
            }
            if($flag != false)
               $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
            else
               $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
               echo json_encode($arr);
            exit(); 
        }
        else
        {
              if($lang == 'ch')
              {
                $flag = $this->stu->dx_update($data,array('id'=>$id));
              }
              else
              {
                $flag = $this->en_stu->dx_update($data,array('id'=>$id));
              }
             if($flag != false)
               $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
             else
               $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
             echo json_encode($arr);
             exit();
        }
      }
      else
      {
        $this->load->view('share/stu_share_add');
      }
    }
//编辑
      public function share_edit()
      {
        $this->load->model('dxdb_model','stu','one_stu_share');
        $this->load->model('dxdb_model','en_stu','one_stu_share_en');
        $id = $this->uri->segment(3);
        $data['arts'] = $this->stu->one(array('id'=>$id));
        $data['en_arts'] = $this->en_stu->one(array('id'=>$id));
        $this->load->view('share/stu_share_edit',$data);
      }

//删除
      public function wis_art_del()
      {
          $this->load->model('dxdb_model','stu','one_stu_share');
          $this->load->model('dxdb_model','en_stu','one_stu_share_en');
          $id = intval($this->input->post('id'));
          $flag = $this->stu->dx_delete(array('id'=>$id));
          $flag = $this->en_stu->dx_delete(array('id'=>$id));
          if($flag)
          {
            $arr['status']  = 1;
            $arr['message']  = "删除信息成功 :)";
          }
          else
          {
             $arr['status']  = 0;
            $arr['message']  = "操作失败 :(";         
          } 
          echo json_encode($arr);
          exit();
      } 

      public function wis_img_upload()
      {
        $image = $_POST['name'];//"goods_image"
        $image_path = '../uploads/share';//图片路径
        $info = $this->_upload_img($image,$image_path);

        //缩略图设置   start
        if($image !== 'pimage')
        {
          $crop_img = $info['full_path'];
          $thumb_img = $info['file_path'].$info['raw_name'].'_261_208'.$info['file_ext'];
          thumb($crop_img,$thumb_img, 261, 208, 5);//缩略图        
        }
        else
        {
          $crop_img = $info['full_path'];
          $thumb_img = $info['file_path'].$info['raw_name'].'_100_100'.$info['file_ext'];
          thumb($crop_img,$thumb_img, 100, 100, 5);//缩略图    
        }

        //缩略图结束  end

        $data = array ();
        $data ['thumb_name'] = base_url()."../uploads/share/".$info['file_name'];
        $data ['name']      = $info['file_name'];
         
        // 整理为json格式
        echo json_encode($data);
        exit();
      }

      public function wis_img_del()
      {
        $image = trim($_POST['name']);//"goods_image"
          $img_url = '../uploads/share/'.$image;
          $img = pathinfo($img_url);
          $thumb_url = '../uploads/share/'.$img['filename'].'_261_208.'.$img['extension'];
        @unlink($img_url); 
        @unlink($thumb_url); 
        // 整理为json格式
        echo 1;
        exit();
      }

      public function wis_art_status()
      {
        $this->load->model('dxdb_model','stu','one_stu_share');
        $this->load->model('dxdb_model','en_stu','one_stu_share_en');
        $id = intval($this->input->post('id'));
        $state = intval($this->input->post('state'));
        if($state == 1)
        {
             $flag = $this->en_stu->dx_update(array('status'=>0),array('id'=>$id));
            $flag = $this->stu->dx_update(array('status'=>0),array('id'=>$id));
            $msg = '操作成功：信息屏蔽!';
        }
        else
        {
            $flag = $this->en_stu->dx_update(array('status'=>1),array('id'=>$id));
            $flag = $this->stu->dx_update(array('status'=>1),array('id'=>$id));
            $msg = '操作成功：信息状态正常 :)';
        }
        if($flag)
        {
          $arr['status']  = 1;
          $arr['message']  = $msg;
        }
        else
        {
           $arr['status']  = 0;
           $arr['message']  = "操作失败 :(";        
        } 
        echo json_encode($arr);
        exit();     
      }  
    







    //名人语录 =====================================
    public function saying()
    {
    	$this->load->model('dxdb_model','say','one_saying');
    	// $this->load->model('dxdb_model','en_say','one_saying_en');
    	$data['says'] = $this->say->all();
    	$this->load->view('share/say',$data);
    }

    //新增名人语录
    public function saying_add()
    {
       $this->load->model('dxdb_model','say','one_saying');
       $this->load->model('dxdb_model','en_say','one_saying_en');
       if($this->input->is_ajax_request())
       {
   			$data = array();//表单内容
        $tmp_imgs = $this->input->post('image');
         //如果图片为空，那么返回错误
        if(empty($tmp_imgs))
        {
           $arr = array('status'=>0,'message'=>'请上传图片, 图片为必须!  :(');  
           echo json_encode($arr);
           exit();    
        }
   			$data = array(
   			    'name'     => $this->input->post('name'),
            'image'     => $tmp_imgs,
   			    'desc'      => $this->input->post('desc'),
   			    'content'   => $this->input->post('content'),
   			    'sort'      => $this->input->post('sort')
   			 );
   			$id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
   			$lang = $this->uri->segment(3);
   			if($id == '')
   			{
   			    if($lang == 'ch')
   			    {
   			        $flag = $this->say->dx_insert($data);
   			        $flag = $this->en_say->dx_insert(array('sort'=>$this->input->post('sort')));
   			    }
   			    else
   			    {
   			    	$flag = $this->en_say->dx_insert($data); 
   			    	$flag = $this->say->dx_insert(array('sort'=>$this->input->post('sort'))); 
   			    }
   			    if($flag != false)
   			       $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
   			    else
   			       $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
   			       echo json_encode($arr);
   			    exit(); 
   			}
   			else
   			{
   			      if($lang == 'ch')
   			      {
   			        $flag = $this->say->dx_update($data,array('id'=>$id));
   			      }
   			      else
   			      {
   			        $flag = $this->en_say->dx_update($data,array('id'=>$id));
   			      }
   			     if($flag != false)
   			       $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
   			     else
   			       $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
   			     echo json_encode($arr);
   			     exit();
   			}
       }
       else
       {
       	   $this->load->view('share/say_add');
       }
    }
    //编辑
    public function saying_edit()
    {
    	$this->load->model('dxdb_model','say','one_saying');
    	$this->load->model('dxdb_model','en_say','one_saying_en');
	    $id = $this->uri->segment(3);
	    $data['says'] = $this->say->one(array('id'=>$id));
	    $data['en_says'] = $this->en_say->one(array('id'=>$id));
	    $this->load->view('share/say_edit',$data);
	}

	  //删除
	public function saying_del()
	{
		$this->load->model('dxdb_model','say','one_saying');
		$this->load->model('dxdb_model','en_say','one_saying_en');
	    $id = intval($this->input->post('id'));
	    $flag = $this->say->dx_delete(array('id'=>$id));
	    $flag = $this->en_say->dx_delete(array('id'=>$id));
	    if($flag)
	    {
	      $arr['status']  = 1;
	      $arr['message']  = "删除信息成功 :)";
	    }
	    else
	    {
	       $arr['status']  = $flag;
	      $arr['message']  = "操作失败 :(";         
	    } 
	    echo json_encode($arr);
	    exit();
	}

    // 编辑状态
    public function saying_status()
    {
    	$this->load->model('dxdb_model','say','one_saying');
    	$this->load->model('dxdb_model','en_say','one_saying_en');
		$id = intval($this->input->post('id'));
		$state = intval($this->input->post('state'));
		if($state == 1)
		{
		     $flag = $this->en_say->dx_update(array('status'=>0),array('id'=>$id));
		    $flag = $this->say->dx_update(array('status'=>0),array('id'=>$id));
		    $msg = '操作成功：信息屏蔽!';
		}
		else
		{
		    $flag = $this->en_say->dx_update(array('status'=>1),array('id'=>$id));
		    $flag = $this->say->dx_update(array('status'=>1),array('id'=>$id));
		    $msg = '操作成功：信息状态正常 :)';
		}
		if($flag)
		{
		  $arr['status']  = 1;
		  $arr['message']  = $msg;
		}
		else
		{
		   $arr['status']  = 0;
		   $arr['message']  = "操作失败 :(";        
		} 
		echo json_encode($arr);
		exit(); 
    }

    public function saying_img_upload()
    {
      $image = $_POST['name'];//"goods_image"
      $image_path = '../uploads/share';//图片路径
      $info = $this->_upload_img($image,$image_path);
      $data = array ();
      $data ['thumb_name'] = base_url()."../uploads/share/".$info['file_name'];
      $data ['name']      = $info['file_name']; 
      // 整理为json格式
      echo json_encode($data);
      exit();
    }
    public function saying_img_del()
    {
      $image = trim($_POST['name']);//"goods_image"
      $img_url = '../uploads/share/'.$image;
      @unlink($img_url); 
      echo 1;
      exit();
    }


    //课后辅导===========================================
    public function aftersch()
    {
      $this->load->model('dxdb_model','aft','one_afterschool');
        //后台设置后缀为空
      $this->config->set_item('url_suffix', '');
      // 载入分页类
      $this->load->library('pagination');
      $perPage = 20;
      //配置项设置
      $config['base_url'] = site_url('wisdom/onew');
      $config['total_rows'] = $this->aft->dx_count();
      $config['per_page'] = $perPage;
      $config['uri_segment'] = 3;
      $config['first_link'] = '第一页';
      $config['prev_link'] = '上一页';
      $config['next_link'] = '下一页';
      $config['last_link'] = '最后一页';
      //初始化配置
      $this->pagination->initialize($config);
      $data['links'] = $this->pagination->create_links();
      $offset = $this->uri->segment(3);
      $this->db->limit($perPage, $offset);
      $data['afts'] = $this->aft->all();
      $this->load->view('share/after',$data);
    }

    //添加
    public function aftersch_add()
    {
      $this->load->model('dxdb_model','aft','one_afterschool');
      $this->load->model('dxdb_model','en_aft','one_afterschool_en');
      if($this->input->is_ajax_request())
      {
        $data = array();//表单内容
        $addtime = strtotime($this->input->post('updatetime'));
        $data = array(
            'title'     => $this->input->post('title'),
            'desc'      => $this->input->post('desc'),
            'content'   => $this->input->post('content'),
            'name'      => $this->input->post('name'),
            'vedio'     => serialize(htmlspecialchars($this->input->post('vedio'))),
            'updatetime'=> $addtime,
            'type'      => $this->input->post('type'),
            'sort'      => $this->input->post('sort')
         );
        $id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
        $lang = $this->uri->segment(3);
        if($id == '')
        {
            if($lang == 'ch')
            {
                $flag = $this->aft->dx_insert($data);
                $flag = $this->en_aft->dx_insert(array('type'=>$this->input->post('type')));
            }
            else
            {
              $flag = $this->en_aft->dx_insert($data); 
              $flag = $this->aft->dx_insert(array('type'=>$this->input->post('type'))); 
            }
            if($flag != false)
               $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
            else
               $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
               echo json_encode($arr);
            exit(); 
        }
        else
        {
              if($lang == 'ch')
              {
                $flag = $this->aft->dx_update($data,array('id'=>$id));
              }
              else
              {
                $flag = $this->en_aft->dx_update($data,array('id'=>$id));
              }
             if($flag != false)
               $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
             else
               $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
             echo json_encode($arr);
             exit();
        }
      }
      else
      {
        $this->load->view('share/after_add');
      }
    }

    public function aftersch_edit()
    {
      $this->load->model('dxdb_model','aft','one_afterschool');
      $this->load->model('dxdb_model','en_aft','one_afterschool_en');
      $id = $this->uri->segment(3);
      $data['nots'] = $this->aft->one(array('id'=>$id));
      $data['en_nots'] = $this->en_aft->one(array('id'=>$id));
      $this->load->view('share/after_edit',$data);
    }
    //修改状态
    public function aftersch_status()
    {
      $this->load->model('dxdb_model','aft','one_afterschool');
      $this->load->model('dxdb_model','en_aft','one_afterschool_en');
      $id = intval($this->input->post('id'));
      $state = intval($this->input->post('state'));
      if($state == 1)
      {
           $flag = $this->en_aft->dx_update(array('status'=>0),array('id'=>$id));
          $flag = $this->aft->dx_update(array('status'=>0),array('id'=>$id));
          $msg = '操作成功：信息屏蔽!';
      }
      else
      {
          $flag = $this->en_aft->dx_update(array('status'=>1),array('id'=>$id));
          $flag = $this->aft->dx_update(array('status'=>1),array('id'=>$id));
          $msg = '操作成功：信息状态正常 :)';
      }
      if($flag)
      {
        $arr['status']  = 1;
        $arr['message']  = $msg;
      }
      else
      {
         $arr['status']  = 0;
         $arr['message']  = "操作失败 :(";        
      } 
      echo json_encode($arr);
      exit(); 
    }

    //删除
    public function aftersch_del()
    {
      $this->load->model('dxdb_model','aft','one_afterschool');
      $this->load->model('dxdb_model','en_aft','one_afterschool_en');
      $id = intval($this->input->post('id'));
      $flag = $this->aft->dx_delete(array('id'=>$id));
      $flag = $this->en_aft->dx_delete(array('id'=>$id));
      if($flag)
      {
        $arr['status']  = 1;
        $arr['message']  = "删除信息成功 :)";
      }
      else
      {
         $arr['status']  = 0;
        $arr['message']  = "操作失败 :(";         
      } 
      echo json_encode($arr);
      exit();
    }



    /**
     * 2105.2.19修改    ad
     */


    public function interprete()
    {
      $this->load->model('dxdb_model','inter','one_interprete');
        //后台设置后缀为空
      $this->config->set_item('url_suffix', '');
      // 载入分页类
      $this->load->library('pagination');
      $perPage = 20;
      //配置项设置
      $config['base_url'] = site_url('share/index');
      $config['total_rows'] = $this->inter->dx_count();
      $config['per_page'] = $perPage;
      $config['uri_segment'] = 3;
      $config['first_link'] = '第一页';
      $config['prev_link'] = '上一页';
      $config['next_link'] = '下一页';
      $config['last_link'] = '最后一页';
      //初始化配置
      $this->pagination->initialize($config);
      $data['links'] = $this->pagination->create_links();
      $offset = $this->uri->segment(3);
      $this->db->limit($perPage, $offset);
      $data['weeks'] = $this->inter->all();
      $this->load->view('share/interprete',$data);
    }

    //增加
    public function interprete_add()
    {
      $this->load->model('dxdb_model','inter','one_interprete');
      $this->load->model('dxdb_model','en_inter','one_interprete_en');
      if($this->input->is_ajax_request())
      {
        $data = array();//表单内容
        $tmp_imgs = $this->input->post('image');
        $pimages = $this->input->post('pimage');
         //如果图片为空，那么返回错误
        if(empty($tmp_imgs))
        {
           $arr = array('status'=>0,'message'=>'请上传图片, 图片为必须!  :(');  
           echo json_encode($arr);
           exit();    
        }
        $addtime = strtotime($this->input->post('updatetime'));
        $data = array(
            'title'     => $this->input->post('title'),
            'desc'      => $this->input->post('desc'),
            'image'     => $tmp_imgs,
            'pimage'    => $pimages,
            'content'   => $this->input->post('content'),
            'vedio'     => serialize(htmlspecialchars($this->input->post('vedio'))),
            'updatetime'=> $addtime,
            'type'      => $this->input->post('type'),
            'name'      => $this->input->post('name'),
            'sort'      => $this->input->post('sort')
         );
        $id = trim($this->input->post('id'));//获取添加数据的id值   如果有id  就为编辑  否则为添加
        $lang = $this->uri->segment(3);
        if($id == '')
        {
            if($lang == 'ch')
            {
                $flag = $this->inter->dx_insert($data);
                $flag = $this->en_inter->dx_insert(array('sort'=>255));
            }
            else
            {
              $flag = $this->en_inter->dx_insert($data); 
              $flag = $this->inter->dx_insert(array('sort'=>255)); 
            }
            if($flag != false)
               $arr = array('status'=>1,'tid'=>$flag,'message'=>'保存成功 :)');    
            else
               $arr = array('status'=>0,'tid'=>$flag,'message'=>'操作失败 :(');  
               echo json_encode($arr);
            exit(); 
        }
        else
        {
              if($lang == 'ch')
              {
                $flag = $this->inter->dx_update($data,array('id'=>$id));
              }
              else
              {
                $flag = $this->en_inter->dx_update($data,array('id'=>$id));
              }
             if($flag != false)
               $arr = array('status'=>1,'tid'=>$id,'message'=>'保存成功 1:)');    
             else
               $arr = array('status'=>0,'tid'=>$id,'message'=>'操作失败 1:(');  
             echo json_encode($arr);
             exit();
        }
      }
      else
      {
        $this->load->view('share/interprete_add');
      }
    }


    //修改状态
    public function interprete_status()
    {
      $this->load->model('dxdb_model','inter','one_interprete');
      $this->load->model('dxdb_model','en_inter','one_interprete_en');
      $id = intval($this->input->post('id'));
      $state = intval($this->input->post('state'));
      if($state == 1)
      {
           $flag = $this->en_inter->dx_update(array('status'=>0),array('id'=>$id));
          $flag = $this->inter->dx_update(array('status'=>0),array('id'=>$id));
          $msg = '操作成功：信息屏蔽!';
      }
      else
      {
          $flag = $this->en_inter->dx_update(array('status'=>1),array('id'=>$id));
          $flag = $this->inter->dx_update(array('status'=>1),array('id'=>$id));
          $msg = '操作成功：信息状态正常 :)';
      }
      if($flag)
      {
        $arr['status']  = 1;
        $arr['message']  = $msg;
      }
      else
      {
         $arr['status']  = 0;
         $arr['message']  = "操作失败 :(";        
      } 
      echo json_encode($arr);
      exit(); 
    }

    public function interprete_del()
    {
      $this->load->model('dxdb_model','inter','one_interprete');
      $this->load->model('dxdb_model','en_inter','one_interprete_en');
      $id = intval($this->input->post('id'));
      $flag = $this->inter->dx_delete(array('id'=>$id));
      $flag = $this->en_inter->dx_delete(array('id'=>$id));
      if($flag)
      {
        $arr['status']  = 1;
        $arr['message']  = "删除信息成功 :)";
      }
      else
      {
         $arr['status']  = 0;
        $arr['message']  = "操作失败 :(";         
      } 
      echo json_encode($arr);
      exit();  
    }

    public function interprete_edit()
    {
      $this->load->model('dxdb_model','inter','one_interprete');
      $this->load->model('dxdb_model','en_inter','one_interprete_en');
      $id = $this->uri->segment(3);
      $data['arts'] = $this->inter->one(array('id'=>$id));
      $data['en_arts'] = $this->en_inter->one(array('id'=>$id));
      $this->load->view('share/interprete_edit',$data);
    }
}

