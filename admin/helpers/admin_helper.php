<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  辅助函数库
 *
 */

// ------------------------------------------------------------------------

 /**
  * 视图url
  * @access	public
  */
if ( ! function_exists('back_url'))
{
	function back_url()
	{
		return base_url().'../';
	}
}
 /**
     * 图片裁切处理
     * @param $img 原图
     * @param string $outFile 另存文件名
     * @param string $thumbWidth 缩略图宽度
     * @param string $thumbHeight 缩略图高度
     * @param string $thumbType 裁切图片的方式
     * 1 固定宽度  高度自增 2固定高度  宽度自增 3固定宽度  高度裁切
     * 4 固定高度 宽度裁切 5缩放最大边 原图不裁切 6缩略图尺寸不变，自动裁切最大边
     * @return bool|string
     */
//  if ( ! function_exists('thumb'))
//  {
// 	function thumb($img, $outFile = '', $thumbWidth = '', $thumbHeight = '', $thumbType = '')
// 	{
// 	    $type = array(".jpg", ".jpeg", ".png", ".gif");
// 	    $imgType = strtolower(strrchr($img, '.'));

// 	    if (! (extension_loaded('gd') && file_exists($img) && in_array($imgType, $type))) {
// 	        return false;
// 	    }
// 	    //基础配置
// 	    $thumbType = $thumbType ;
// 	    $thumbWidth = $thumbWidth;
// 	    $thumbHeight = $thumbHeight;
// 	    //获得图像信息
// 	    $imgInfo = getimagesize($img);
// 	    $imgWidth = $imgInfo [0];
// 	    $imgHeight = $imgInfo [1];
// 	    $imgType = image_type_to_extension($imgInfo [2]);
// 	    //获得相关尺寸
// 	    $thumb_size = thumbSize($imgWidth, $imgHeight, $thumbWidth, $thumbHeight, $thumbType);
// 	    //原始图像资源
// 	    $func = "imagecreatefrom" . substr($imgType, 1);
// 	    $resImg = $func($img);
// 	    //缩略图的资源
// 	    if ($imgType == '.gif') {
// 	        $res_thumb = imagecreate($thumb_size [0], $thumb_size [1]);
// 	        $color = imagecolorallocate($res_thumb, 255, 0, 0);
// 	    } else {
// 	        $res_thumb = imagecreatetruecolor($thumb_size [0], $thumb_size [1]);
// 	        imagealphablending($res_thumb, false); //关闭混色
// 	        imagesavealpha($res_thumb, true); //储存透明通道
// 	    }
// 	    //绘制缩略图X
// 	    if (function_exists("imagecopyresampled")) {
// 	        imagecopyresampled($res_thumb, $resImg, 0, 0, 0, 0, $thumb_size [0], $thumb_size [1], $thumb_size [2], $thumb_size [3]);
// 	    } else {
// 	        imagecopyresized($res_thumb, $resImg, 0, 0, 0, 0, $thumb_size [0], $thumb_size [1], $thumb_size [2], $thumb_size [3]);
// 	    }
// 	    //处理透明色
// 	    if ($imgType == '.gif') {
// 	        imagecolortransparent($res_thumb, $color);
// 	    }
// 	    //配置输出文件名
// 	    $imgInfo = pathinfo($img);
// 	    $outFile = $outFile;

// 	    $func = "image" . substr($imgType, 1);
// 	    $func($res_thumb, $outFile);
// 	    if (isset($resImg))
// 	        imagedestroy($resImg);
// 	    if (isset($res_thumb))
// 	        imagedestroy($res_thumb);
// 	    return $outFile;
// 	}	
//  }

// if ( ! function_exists('thumbSize'))
// {
// 	function thumbSize($imgWidth, $imgHeight, $thumbWidth, $thumbHeight, $thumbType)
// 	{
// 	    //初始化缩略图尺寸
// 	    $w = $thumbWidth;
// 	    $h = $thumbHeight;
// 	    //初始化原图尺寸
// 	    $cuthumbWidth = $imgWidth;
// 	    $cuthumbHeight = $imgHeight;
// 	    switch ($thumbType) 
// 	    {
// 	        case 1 :
// 	            //固定宽度  高度自增
// 	            $h = $thumbWidth / $imgWidth * $imgHeight;
// 	            break;
// 	        case 2 :
// 	            //固定高度  宽度自增
// 	            $w = $thumbHeight / $imgHeight * $imgWidth;
// 	            break;
// 	        case 3 :
// 	            //固定宽度  高度裁切
// 	            $cuthumbHeight = $imgWidth / $thumbWidth * $thumbHeight;
// 	            break;
// 	        case 4 :
// 	            //固定高度  宽度裁切
// 	            $cuthumbWidth = $imgHeight / $thumbHeight * $thumbWidth;
// 	            break;
// 	        case 5 :
// 	            //缩放最大边 原图不裁切
// 	            if (($imgWidth / $thumbWidth) > ($imgHeight / $thumbHeight)) {
// 	                $h = $thumbWidth / $imgWidth * $imgHeight;
// 	            } elseif (($imgWidth / $thumbWidth) < ($imgHeight / $thumbHeight)) {
// 	                $w = $thumbHeight / $imgHeight * $imgWidth;
// 	            } else {
// 	                $w = $thumbWidth;
// 	                $h = $thumbHeight;
// 	            }
// 	            break;
// 	        default:
// 	            //缩略图尺寸不变，自动裁切图片
// 	            if (($imgHeight / $thumbHeight) < ($imgWidth / $thumbWidth)) {
// 	                $cuthumbWidth = $imgHeight / $thumbHeight * $thumbWidth;
// 	            } elseif (($imgHeight / $thumbHeight) > ($imgWidth / $thumbWidth)) {
// 	                $cuthumbHeight = $imgWidth / $thumbWidth * $thumbHeight;
// 	            }
// 	//            }
// 	    }
// 	    $arr [0] = $w;
// 	    $arr [1] = $h;
// 	    $arr [2] = $cuthumbWidth;
// 	    $arr [3] = $cuthumbHeight;
// 	    return $arr; 
// 	}	
// }

if ( ! function_exists('dx_array_filter'))
{
	function dx_array_filter($arr)
	{
		$tmp = array();
		if(is_array($arr))
		{
           foreach ($arr as $v) {
           	if(! empty($v))
           	{
           		$tmp[] = $v;
           	}
           }
           return $tmp;
		}
		return false;
	}
}


 function thumb($srcfile, $thumbfile, $new_width=0, $new_height=0,$sty=TRUE, $rate=95, $markwords='',$markimage='') {
 	//global $cfg_root;
 	//$thumbfile = $srcfile ."_{$new_width}X{$new_height}.".substr($srcfile,-3);
 	$v_srcfile = $srcfile;
 	$v_thumbfile = $thumbfile;
 	//$srcfile = $cfg_root.$srcfile;
 	//$thumbfile = $cfg_root.$thumbfile;
 	
 	//echo $srcfile .',,,,,,,,'.$thumbfile;
 	if (file_exists($thumbfile)) { return $v_thumbfile; }
 	if ($new_width ==0 && $new_height == 0) { return ; }
 	if (!file_exists($srcfile)) { return; }
 	
 	list($width, $height) = getimagesize($srcfile);
 	if ($new_width > $width) { $new_width = $width; }
 	
 	// 图像类型
 	//$type = exif_imagetype($srcfile);
 	
 	$img_info = getimagesize($srcfile); 
 	
 	switch ($img_info[2]) {
 	case 1:
 		$type = IMAGETYPE_GIF;
 		break;
 	case 2:
 		$type = IMAGETYPE_JPEG;
 		break;
 	case 3:
 		$type = IMAGETYPE_PNG;
 		break;
 	} 
 	//echo $type;
 	
 	$support_type = array(IMAGETYPE_JPEG , IMAGETYPE_PNG , IMAGETYPE_GIF);
 	if(!in_array($type, $support_type,true)) { return; }
 	//Load image
 	switch($type) {
 		case IMAGETYPE_JPEG :
 			$src_img = imagecreatefromjpeg($srcfile);
 			break;
 		case IMAGETYPE_PNG :
 			$src_img = imagecreatefrompng($srcfile);
 			break;
 		case IMAGETYPE_GIF :
 			$src_img = imagecreatefromgif($srcfile);
 			break;
 		default:
 			return;
 	}
 	$w = imagesx($src_img);
 	$h = imagesy($src_img);
 	if ($new_width == 0 ) { $new_width = $w * ($new_height/$h); }
 	if ($new_height == 0 ) { $new_height = $h * ($new_width/$w); }
 	$ratio_w = 1.0 * $new_width / $w;
 	$ratio_h = 1.0 * $new_height / $h;
 	$ratio = 1.0;
 	// 生成的图像的高宽比原来的都小，或都大 ，原则是 取大比例放大，取大比例缩小（缩小的比例就比较小了）
 	if ( ($ratio_w < 1 && $ratio_h < 1) || ($ratio_w > 1 && $ratio_h > 1)) {
 		if ($ratio_w < $ratio_h) {
 			$ratio = $ratio_h ; // 情况一，宽度的比例比高度方向的小，按照高度的比例标准来裁剪或放大
 		} else {
 			$ratio = $ratio_w ;
 		}
 		// 定义一个中间的临时图像，该图像的宽高比 正好满足目标要求
 		$inter_w=(int)($new_width / $ratio);
 		$inter_h=(int) ($new_height / $ratio);
 		$inter_img=imagecreatetruecolor($inter_w , $inter_h);
 		$srcx = (int)(($w - $inter_w)/2);
 		$srcy = (int)(($h - $inter_h)/2);
 		imagecopy($inter_img, $src_img, 0,0,$srcx,$srcy,$inter_w,$inter_h);
 		// 生成一个以最大边长度为大小的是目标图像$ratio比例的临时图像
 		// 定义一个新的图像
 		$new_img = imagecreatetruecolor($new_width,$new_height);
 		imagecopyresampled($new_img,$inter_img,0,0,0,0,$new_width,$new_height,$inter_w,$inter_h);
 	} // end if 1
 	// 2 目标图像 的一个边大于原图，一个边小于原图 ，先放大平普图像，然后裁剪
 	// =if( ($ratio_w < 1 && $ratio_h > 1) || ($ratio_w >1 && $ratio_h <1) )
 	else{
 		$ratio = $ratio_h>$ratio_w? $ratio_h : $ratio_w; //取比例大的那个值
 		// 定义一个中间的大图像，该图像的高或宽和目标图像相等，然后对原图放大
 		$inter_w = (int)($w * $ratio);
 		$inter_h = (int)($h * $ratio);
 		if($ratio_h>$ratio_w) {
 			$srcx = (int)(($inter_w - $w)/2);
 			$srcy = 0;
 		} else {
 			$srcx = 0;
 			$srcy = (int)(($inter_h - $h)/2);
 		}
 		$inter_img = imagecreatetruecolor($inter_w , $inter_h);
 		//将原图缩放比例后裁剪
 		imagecopyresampled($inter_img,$src_img,0,0,$srcx,$srcy,$inter_w,$inter_h,$w,$h);
 		// 定义一个新的图像
 		$new_img = imagecreatetruecolor($new_width,$new_height);
 		imagecopy($new_img, $inter_img, 0,0,0,0,$new_width,$new_height); 
 	}// if3
 	switch($type) {
 		case IMAGETYPE_JPEG :
 			imagejpeg($new_img, $thumbfile, $rate); // 存储图像
 			break;
 		case IMAGETYPE_PNG :
 			imagepng($new_img,$thumbfile);
 			break;
 		case IMAGETYPE_GIF :
 			imagegif($new_img,$thumbfile, $rate);
 			break;
 		default:
 			break;
 	}
 	return $v_thumbfile; 
 }