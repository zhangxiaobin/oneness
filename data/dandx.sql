/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : dandx

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2014-12-26 17:20:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dx_admin`
-- ----------------------------
DROP TABLE IF EXISTS `dx_admin`;
CREATE TABLE `dx_admin` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL COMMENT '管理员名称',
  `password` char(40) NOT NULL COMMENT '密码',
  `logintime` int(10) NOT NULL DEFAULT '0' COMMENT '登录时间',
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 0 屏蔽',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常，0修改',
  `lastip` char(15) NOT NULL COMMENT '最后登录ip',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_admin
-- ----------------------------
INSERT INTO `dx_admin` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2', '1', '1', '');

-- ----------------------------
-- Table structure for `dx_category`
-- ----------------------------
DROP TABLE IF EXISTS `dx_category`;
CREATE TABLE `dx_category` (
  `cid` mediumint(5) NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `pid` mediumint(5) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `catname` char(30) NOT NULL DEFAULT '' COMMENT '栏目名字',
  `cat_url` char(30) DEFAULT NULL COMMENT '跳转的url',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 0 不正常',
  `sort` smallint(5) NOT NULL DEFAULT '255' COMMENT '排序',
  `seo_title` char(100) DEFAULT NULL COMMENT 'seo_标题',
  `seo_description` varchar(255) DEFAULT NULL COMMENT 'seo_描述',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_category
-- ----------------------------
INSERT INTO `dx_category` VALUES ('1', '0', '首页', '', '1', '255', null, null);
INSERT INTO `dx_category` VALUES ('2', '0', '联系我们', null, '1', '255', null, null);
INSERT INTO `dx_category` VALUES ('3', '0', '新闻速递', null, '1', '255', null, null);
INSERT INTO `dx_category` VALUES ('4', '3', '国内新闻', null, '1', '255', null, null);
INSERT INTO `dx_category` VALUES ('5', '3', '国外新闻', null, '1', '255', null, null);

-- ----------------------------
-- Table structure for `dx_click`
-- ----------------------------
DROP TABLE IF EXISTS `dx_click`;
CREATE TABLE `dx_click` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hits` int(10) NOT NULL COMMENT '点击数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_click
-- ----------------------------
INSERT INTO `dx_click` VALUES ('1', '47');

-- ----------------------------
-- Table structure for `dx_en_events_tag`
-- ----------------------------
DROP TABLE IF EXISTS `dx_en_events_tag`;
CREATE TABLE `dx_en_events_tag` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '标签ID',
  `value` char(60) NOT NULL COMMENT '标签值',
  `sort` smallint(6) NOT NULL DEFAULT '255' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_en_events_tag
-- ----------------------------
INSERT INTO `dx_en_events_tag` VALUES ('1', '开业', '255');
INSERT INTO `dx_en_events_tag` VALUES ('4', '元旦', '255');
INSERT INTO `dx_en_events_tag` VALUES ('5', '圣诞节', '255');

-- ----------------------------
-- Table structure for `dx_events`
-- ----------------------------
DROP TABLE IF EXISTS `dx_events`;
CREATE TABLE `dx_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'eventid',
  `title` char(60) NOT NULL COMMENT '标题',
  `desc` varchar(255) DEFAULT NULL COMMENT '简述',
  `content` text COMMENT '详细内容',
  `sort` smallint(6) NOT NULL COMMENT '排序',
  `addtime` int(10) NOT NULL COMMENT '添加时间',
  `tag` char(30) NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_events
-- ----------------------------

-- ----------------------------
-- Table structure for `dx_events_tag`
-- ----------------------------
DROP TABLE IF EXISTS `dx_events_tag`;
CREATE TABLE `dx_events_tag` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '标签ID',
  `value` char(60) NOT NULL COMMENT '标签值',
  `sort` smallint(6) NOT NULL DEFAULT '255' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_events_tag
-- ----------------------------
INSERT INTO `dx_events_tag` VALUES ('1', '开业', '255');
INSERT INTO `dx_events_tag` VALUES ('4', '元旦', '255');

-- ----------------------------
-- Table structure for `dx_goods`
-- ----------------------------
DROP TABLE IF EXISTS `dx_goods`;
CREATE TABLE `dx_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `title` varchar(60) NOT NULL COMMENT '标题、名字',
  `index_img` varchar(100) NOT NULL COMMENT '首页显示图 ',
  `list_img` varchar(100) NOT NULL COMMENT '列表图片',
  `addtime` int(10) NOT NULL COMMENT '添加时间',
  `sort` smallint(6) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `dx_node`
-- ----------------------------
DROP TABLE IF EXISTS `dx_node`;
CREATE TABLE `dx_node` (
  `nid` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '节点id',
  `title` char(30) NOT NULL COMMENT '中文标题',
  `control` char(30) NOT NULL COMMENT '控制器',
  `method` char(30) NOT NULL COMMENT '方法',
  `param` char(100) NOT NULL COMMENT '参数',
  `comment` varchar(255) NOT NULL COMMENT '备注',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1 显示 0 不显示',
  `type` tinyint(1) NOT NULL DEFAULT '2' COMMENT '类型 1 权限控制菜单 2 普通菜单',
  `pid` smallint(6) NOT NULL DEFAULT '0' COMMENT '父id',
  `order` smallint(6) NOT NULL DEFAULT '255' COMMENT '排序',
  PRIMARY KEY (`nid`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_node
-- ----------------------------
INSERT INTO `dx_node` VALUES ('1', '系统概要', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `dx_node` VALUES ('2', '系统配置', '', '', '', '', '1', '2', '1', '255');
INSERT INTO `dx_node` VALUES ('3', '网站配置', 'welcome', 'xiao', '', '', '1', '2', '2', '255');
INSERT INTO `dx_node` VALUES ('4', '密码修改', 'welcome', 'change_pass', '', '', '1', '2', '2', '255');
INSERT INTO `dx_node` VALUES ('6', '信息管理', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `dx_node` VALUES ('7', '店铺信息', '', '', '', '', '1', '2', '6', '255');
INSERT INTO `dx_node` VALUES ('8', '店铺管理', 'store', 'index', '', '', '1', '2', '7', '255');
INSERT INTO `dx_node` VALUES ('9', '商品信息', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `dx_node` VALUES ('10', '商品信息', '', '', '', '', '1', '2', '9', '255');
INSERT INTO `dx_node` VALUES ('11', '商品管理', 'store_goods', 'index', '', '', '1', '2', '10', '255');
INSERT INTO `dx_node` VALUES ('12', '新闻信息', '', '', '', '', '1', '2', '6', '255');
INSERT INTO `dx_node` VALUES ('13', '标签管理', 'event', 'tag', '', '', '1', '2', '12', '255');
INSERT INTO `dx_node` VALUES ('14', '新闻管理', 'event', 'index', '', '', '1', '2', '12', '255');

-- ----------------------------
-- Table structure for `dx_store`
-- ----------------------------
DROP TABLE IF EXISTS `dx_store`;
CREATE TABLE `dx_store` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '店铺id',
  `store_name` char(60) NOT NULL COMMENT '店铺名字',
  `list_img` varchar(255) NOT NULL COMMENT '店铺多图展示',
  `province` char(30) NOT NULL COMMENT '省份',
  `city` char(30) NOT NULL COMMENT '城市',
  `address_img` varchar(100) NOT NULL COMMENT '地址图片',
  `address` varchar(255) DEFAULT NULL COMMENT '地址信息',
  `contract` varchar(60) DEFAULT NULL COMMENT '联系方式',
  `shop_hours` varchar(100) DEFAULT NULL COMMENT '营业时间',
  `addtime` int(10) NOT NULL COMMENT '店铺开店时间',
  `sort` smallint(6) NOT NULL DEFAULT '255' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 0 屏蔽',
  `desc` varchar(255) NOT NULL COMMENT '店铺描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_store
-- ----------------------------
INSERT INTO `dx_store` VALUES ('1', '店铺1', '', '', '', '', null, null, null, '0', '255', '1', '');

-- ----------------------------
-- Table structure for `dx_throttles`
-- ----------------------------
DROP TABLE IF EXISTS `dx_throttles`;
CREATE TABLE `dx_throttles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dx_throttles
-- ----------------------------
INSERT INTO `dx_throttles` VALUES ('1', '1', 'attempt_login', '127.0.0.1', '2014-12-22 12:06:51', '2014-12-22 12:06:51');
