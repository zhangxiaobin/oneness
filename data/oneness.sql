/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : oneness

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2015-02-12 19:53:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `one_about`
-- ----------------------------
DROP TABLE IF EXISTS `one_about`;
CREATE TABLE `one_about` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` char(30) NOT NULL COMMENT '标题',
  `desc` varchar(1500) DEFAULT NULL COMMENT '学校简介',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_about
-- ----------------------------
INSERT INTO `one_about` VALUES ('1', '校园简介e', '这是测试内容');

-- ----------------------------
-- Table structure for `one_admin`
-- ----------------------------
DROP TABLE IF EXISTS `one_admin`;
CREATE TABLE `one_admin` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL COMMENT '管理员名称',
  `password` char(40) NOT NULL COMMENT '密码',
  `logintime` int(10) NOT NULL DEFAULT '0' COMMENT '登录时间',
  `role` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 0 屏蔽',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常，0修改',
  `lastip` char(15) NOT NULL COMMENT '最后登录ip',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_admin
-- ----------------------------
INSERT INTO `one_admin` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2', '1', '1', '');

-- ----------------------------
-- Table structure for `one_category`
-- ----------------------------
DROP TABLE IF EXISTS `one_category`;
CREATE TABLE `one_category` (
  `cid` mediumint(5) NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `pid` mediumint(5) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `catname` char(30) NOT NULL DEFAULT '' COMMENT '栏目名字',
  `cat_url` char(30) DEFAULT NULL COMMENT '跳转的url',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1正常 0 不正常',
  `sort` smallint(5) NOT NULL DEFAULT '255' COMMENT '排序',
  `seo_title` char(100) DEFAULT NULL COMMENT 'seo_标题',
  `seo_description` varchar(255) DEFAULT NULL COMMENT 'seo_描述',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_category
-- ----------------------------
INSERT INTO `one_category` VALUES ('1', '0', '首页', '', '1', '255', null, null);
INSERT INTO `one_category` VALUES ('2', '0', '联系我们', null, '1', '255', null, null);
INSERT INTO `one_category` VALUES ('3', '0', '新闻速递', null, '1', '255', null, null);
INSERT INTO `one_category` VALUES ('4', '3', '国内新闻', null, '1', '255', null, null);
INSERT INTO `one_category` VALUES ('5', '3', '国外新闻', null, '1', '255', null, null);

-- ----------------------------
-- Table structure for `one_copy`
-- ----------------------------
DROP TABLE IF EXISTS `one_copy`;
CREATE TABLE `one_copy` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `one_en_about`
-- ----------------------------
DROP TABLE IF EXISTS `one_en_about`;
CREATE TABLE `one_en_about` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` char(30) NOT NULL COMMENT '标题',
  `desc` varchar(1500) DEFAULT NULL COMMENT '学校简介',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_en_about
-- ----------------------------
INSERT INTO `one_en_about` VALUES ('1', 'school desce', 'this is test ceshi');

-- ----------------------------
-- Table structure for `one_node`
-- ----------------------------
DROP TABLE IF EXISTS `one_node`;
CREATE TABLE `one_node` (
  `nid` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '节点id',
  `title` char(30) NOT NULL COMMENT '中文标题',
  `control` char(30) NOT NULL COMMENT '控制器',
  `method` char(30) NOT NULL COMMENT '方法',
  `param` char(100) NOT NULL COMMENT '参数',
  `comment` varchar(255) NOT NULL COMMENT '备注',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1 显示 0 不显示',
  `type` tinyint(1) NOT NULL DEFAULT '2' COMMENT '类型 1 权限控制菜单 2 普通菜单',
  `pid` smallint(6) NOT NULL DEFAULT '0' COMMENT '父id',
  `order` smallint(6) NOT NULL DEFAULT '255' COMMENT '排序',
  PRIMARY KEY (`nid`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_node
-- ----------------------------
INSERT INTO `one_node` VALUES ('1', '系统概要', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `one_node` VALUES ('2', '系统配置', '', '', '', '', '1', '2', '1', '255');
INSERT INTO `one_node` VALUES ('3', '网站配置', 'welcome', 'xiao', '', '', '1', '2', '2', '255');
INSERT INTO `one_node` VALUES ('4', '密码修改', 'welcome', 'change_pass', '', '', '1', '2', '2', '255');
INSERT INTO `one_node` VALUES ('6', '走进合一', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `one_node` VALUES ('7', '走进合一', '', '', '', '', '1', '2', '6', '255');
INSERT INTO `one_node` VALUES ('8', '校园简介', 'about', 'un_desc', '', '', '1', '2', '7', '255');
INSERT INTO `one_node` VALUES ('9', '合一智慧', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `one_node` VALUES ('10', '合一智慧', '', '', '', '', '1', '2', '9', '255');
INSERT INTO `one_node` VALUES ('11', '合一智慧', 'wisdom', 'index', '', '', '1', '2', '10', '255');
INSERT INTO `one_node` VALUES ('12', '温馨每周', 'wisdom', 'week', '', '', '1', '2', '10', '255');
INSERT INTO `one_node` VALUES ('13', '印度印象', 'about', 'un_pics', '', '', '1', '2', '7', '254');
INSERT INTO `one_node` VALUES ('14', '校园资讯', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `one_node` VALUES ('15', '校园资讯', '', '', '', '', '1', '2', '14', '255');
INSERT INTO `one_node` VALUES ('16', '合一公告', 'news', 'notice', '', '', '1', '2', '15', '255');
INSERT INTO `one_node` VALUES ('17', '合一新闻', 'news', 'new', '', '', '1', '2', '15', '255');
INSERT INTO `one_node` VALUES ('18', '合一教程', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `one_node` VALUES ('19', '合一教程', '', '', '', '', '1', '2', '18', '255');
INSERT INTO `one_node` VALUES ('20', '互动天地', '', '', '', '', '1', '2', '0', '255');
INSERT INTO `one_node` VALUES ('21', '互动天地', '', '', '', '', '1', '2', '20', '255');
INSERT INTO `one_node` VALUES ('22', '学员分享', 'share', 'index', '', '', '1', '2', '21', '255');
INSERT INTO `one_node` VALUES ('23', '学员语录', 'share', 'saying', '', '', '1', '2', '21', '255');
INSERT INTO `one_node` VALUES ('24', '走进校园', 'about', 'sch_pics', '', '', '1', '2', '7', '255');

-- ----------------------------
-- Table structure for `one_throttles`
-- ----------------------------
DROP TABLE IF EXISTS `one_throttles`;
CREATE TABLE `one_throttles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_throttles
-- ----------------------------
INSERT INTO `one_throttles` VALUES ('1', '1', 'attempt_login', '127.0.0.1', '2014-12-22 12:06:51', '2014-12-22 12:06:51');

-- ----------------------------
-- Table structure for `one_university`
-- ----------------------------
DROP TABLE IF EXISTS `one_university`;
CREATE TABLE `one_university` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT COMMENT 'Uni  id',
  `uname` char(30) DEFAULT NULL COMMENT 'chinese name',
  `enuname` varchar(100) DEFAULT NULL COMMENT 'en name',
  `upics` varchar(2000) DEFAULT NULL COMMENT '图片',
  `status` tinyint(1) DEFAULT '1' COMMENT '1,正常0,隐藏',
  `sort` smallint(6) DEFAULT '255' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_university
-- ----------------------------
INSERT INTO `one_university` VALUES ('6', '玩儿玩儿', 'werwer', 'a:5:{i:0;a:3:{s:5:\"title\";s:12:\"玩儿玩儿\";s:3:\"pic\";s:18:\"14237369716665.jpg\";s:7:\"picstat\";s:1:\"0\";}i:1;a:3:{s:5:\"title\";s:6:\"玩儿\";s:3:\"pic\";s:18:\"14237369716401.jpg\";s:7:\"picstat\";s:1:\"0\";}i:2;a:3:{s:5:\"title\";s:6:\"捂热\";s:3:\"pic\";s:18:\"14237369717253.jpg\";s:7:\"picstat\";s:1:\"0\";}i:3;a:3:{s:5:\"title\";s:6:\"玩儿\";s:3:\"pic\";s:18:\"14237369716787.jpg\";s:7:\"picstat\";s:1:\"1\";}i:4;a:3:{s:5:\"title\";s:6:\"玩儿\";s:3:\"pic\";s:18:\"14237369712450.jpg\";s:7:\"picstat\";s:1:\"1\";}}', '1', '255');
INSERT INTO `one_university` VALUES ('4', '测试', 'ceshi', 'a:3:{i:0;a:3:{s:5:\"title\";s:13:\"1111111111到\";s:3:\"pic\";s:18:\"14237365303822.png\";s:7:\"picstat\";s:1:\"0\";}i:1;a:3:{s:5:\"title\";s:14:\"12222222222到\";s:3:\"pic\";s:18:\"14237365302394.png\";s:7:\"picstat\";s:1:\"0\";}i:2;a:3:{s:5:\"title\";s:14:\"22233333333到\";s:3:\"pic\";s:18:\"14237365303397.png\";s:7:\"picstat\";s:1:\"1\";}}', '1', '255');
INSERT INTO `one_university` VALUES ('5', '玩儿玩儿', 'werwer', 'a:5:{i:0;a:3:{s:5:\"title\";s:12:\"玩儿玩儿\";s:3:\"pic\";s:18:\"14237369716665.jpg\";s:7:\"picstat\";s:1:\"0\";}i:1;a:3:{s:5:\"title\";s:6:\"玩儿\";s:3:\"pic\";s:18:\"14237369716401.jpg\";s:7:\"picstat\";s:1:\"0\";}i:2;a:3:{s:5:\"title\";s:6:\"捂热\";s:3:\"pic\";s:18:\"14237369717253.jpg\";s:7:\"picstat\";s:1:\"0\";}i:3;a:3:{s:5:\"title\";s:6:\"玩儿\";s:3:\"pic\";s:18:\"14237369716787.jpg\";s:7:\"picstat\";s:1:\"1\";}i:4;a:3:{s:5:\"title\";s:6:\"玩儿\";s:3:\"pic\";s:18:\"14237369712450.jpg\";s:7:\"picstat\";s:1:\"1\";}}', '1', '255');

-- ----------------------------
-- Table structure for `one_un_pics`
-- ----------------------------
DROP TABLE IF EXISTS `one_un_pics`;
CREATE TABLE `one_un_pics` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pic_name` varchar(100) NOT NULL COMMENT '图片名称',
  `pic_cover` varchar(255) NOT NULL COMMENT '图片路径',
  `pic_spec` varchar(100) NOT NULL COMMENT '规格',
  `pic_size` int(10) NOT NULL COMMENT '图片大小',
  `upload_time` int(10) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of one_un_pics
-- ----------------------------
INSERT INTO `one_un_pics` VALUES ('105', '14237278336643.png', 'http://localhost/oneness/admin/../uploads/pics/14237278336643.png', '235x235', '57393', '1423727833');
INSERT INTO `one_un_pics` VALUES ('107', '14237332781613.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237332781613.jpg', '667x1000', '93270', '1423733278');
INSERT INTO `one_un_pics` VALUES ('106', '14237332783801.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237332783801.jpg', '667x1000', '102605', '1423733278');
INSERT INTO `one_un_pics` VALUES ('104', '14237278033668.png', 'http://localhost/oneness/admin/../uploads/pics/14237278033668.png', '235x235', '42171', '1423727803');
INSERT INTO `one_un_pics` VALUES ('97', '14237165114114.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165114114.jpg', '538x792', '73029', '1423716511');
INSERT INTO `one_un_pics` VALUES ('96', '14237165113816.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165113816.jpg', '538x792', '68653', '1423716511');
INSERT INTO `one_un_pics` VALUES ('95', '14237165119045.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165119045.jpg', '538x792', '70401', '1423716511');
INSERT INTO `one_un_pics` VALUES ('94', '14237165119809.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165119809.jpg', '538x792', '71710', '1423716511');
INSERT INTO `one_un_pics` VALUES ('93', '14237165116678.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165116678.jpg', '543x798', '75149', '1423716511');
INSERT INTO `one_un_pics` VALUES ('92', '14237165117675.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165117675.jpg', '543x798', '88853', '1423716511');
INSERT INTO `one_un_pics` VALUES ('91', '14237165115320.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165115320.jpg', '543x798', '89894', '1423716511');
INSERT INTO `one_un_pics` VALUES ('90', '14237165117789.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165117789.jpg', '543x798', '28832', '1423716511');
INSERT INTO `one_un_pics` VALUES ('89', '14237165116961.jpg', 'http://localhost/oneness/admin/../uploads/pics/14237165116961.jpg', '543x798', '129357', '1423716511');
